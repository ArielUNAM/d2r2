#!/usr/bin/python3.4
# coding=utf-8

import sys
import bmgmd as pr
import os
import time

# initialice flag2parameter dictionary
changedFlags=[]
flag2parameter={\
			"project_name":"project",\
			"project_path":"./",\
			"secuences_path":"./",\
			"fasta_labels":None,\
			"fastas_format":None,\
			"secuences_kind":"genes",\
			"identifier_kind":"genes",\
			"species_tree":None,\
			"proteinortho":"proteinortho5.pl",\
			"reconciliation_filter":None,\
			"recon_filter_object":None,\
			"steeps":"0",\
			"prt_steeps":"0",\
			"gff_path":None,\
			"gff_labels":None,\
			"overwrite":False}

# Read flags and it's parameters

for string in sys.argv[1:]:
	if(string[0]=="-"):
		# La entrada es una bandera
		flag=string[1:]
		changedFlags+=[flag]
	else:
		# La entrada es parámetro de la bandera
		flag2parameter[flag]=string

# determine if create the project or load

if(flag2parameter["project_name"] in os.listdir(flag2parameter["project_path"])):
	if(flag2parameter["overwrite"]):
		# Re-crear el proyecto
		print("--------------------------------------------------------------------")
		print("PROJECT WILL BE OVERWRITEN in 15 seconds  !!!!!!!!!!.\n\nctrl + c to cancel\n")
		print("If you don't want to overwrite, set the 'overwrite' flag to 'False'")
		print("-------------------------------------------------------------------\n")
		action="create"
		time.sleep(15)
	else:
		# Cargar el proyecto 
		action="load"
else:
	# Crear proyecto
	action="create"

# start the project
if(action=="create"):
	p=pr.project(flag2parameter["project_name"],\
				flag2parameter["project_path"],\
				sequencesPath=flag2parameter["secuences_path"],\
				fastasLabels=(None if flag2parameter["fasta_labels"]==None else pr.getFilesLabels(flag2parameter["fasta_labels"])),\
				fastasFormat=flag2parameter["fastas_format"],\
				speciesTree=flag2parameter["species_tree"],\
				proteinorthoPath=flag2parameter["proteinortho"],\
				overWrite=flag2parameter["overwrite"],\
				GTFsPath=flag2parameter["gff_path"],\
				GTFsLabels=(None if flag2parameter["gff_labels"]==None else pr.getFilesLabels(flag2parameter["gff_labels"])),\
				secuencesKind=flag2parameter["secuences_kind"],\
				identifierKind=flag2parameter["identifier_kind"],\
				proteinorthoSteeps=[int(steep) for steep in flag2parameter["prt_steeps"].split(",")],\
				reconFilterType=flag2parameter["reconciliation_filter"],\
				reconListFilter=flag2parameter["recon_filter_object"])
else:
	p=pr.load(flag2parameter["project_name"],flag2parameter["project_path"])
	for flag in changedFlags:
		p.status.setAtribute(flag,flag2parameter[flag])

# run the steeps of analisys

with open("{}Summary/command_line_history".format(p.path),"a") as F:
	F.write(">command\n{}\n".format(sys.argv))
	F.write(">flags\n")
	for flag in flag2parameter: F.write("{}\t{}\n".format(flag,flag2parameter[flag]))
	steepsList=[int(steep) for steep in flag2parameter["steeps"].split(",")]
	p.status.runSteps(steps=steepsList)
#	p.analyze(steeps=steepsList)
	F.write(">done\n")
