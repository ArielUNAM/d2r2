# coding=utf-8
import order
import os
from itertools import combinations
import matplotlib.pyplot as plt
import numpy as np
import nets as nt
from copy import deepcopy

def areCongruent(t,T,returnIncongrunts=False):
	# returns True if t is a sub-tree of T else
	incongruents=incongruentTriplets(t,T)
	congruent=len(incongruents)==0
	if(returnIncongrunts): return congruent,incongruents
	else: return congruent

def incongruentTriplets(t,T):
	# Returns the triplets in t
	#	that makes t not to be a sub tree of T
	# Buscar Toda tripleta de t en T:
	#	primero buscar outgrou: por cada elemento
	#	luego ingroupp: por cada par
	incongruets=[]
	TtripletList=T.tripletList()
	for triplet in t.tripletList():
		if(not triplet in TtripletList): incongruets+=[triplet]
	return incongruets

def wasCorrectMap(tree,returnIncongruents=False):
	incongruents=[]
	bfs=order.Tour(tree.root)
	bfs.actions=[lambda node: [None if node.weight+node.edgesGain[soon]-node.edgesLose[soon]==soon.weight\
					else incongruents.append(soon)\
					for soon in node.soons]]
	bfs.go()
	correct=len(incongruents)==0
	if(returnIncongruents): return correct,incongruents
	else: return correct

def getLeavOrthologs(leav):
	orthologs=[]
	bfs=order.Tour(leav.tree.root,kind="BFS")
	bfs.actions=[lambda node: [None if leav.isNodeInPath(soon)\
					else [orthologs.append(indLeave) for indLeave in soon.inducedLeaves]\
					for soon in node.soons]]
	bfs.condition=lambda node: node.type=="S"
	bfs.restriction=lambda node: not leav.isNodeInPath(node)
	bfs.go()
	return orthologs

class proteinsTree2genesTree:

	def __init__(self,tree,genesdict):
		self.tree=tree
		self.genesdict=genesdict
		self.getGen2Leavs()
		self.getSuperNodes()
		# init genes tree
		self.tc=treeCopier(self.tree,restrictors=list(self.colapsedEvents))
		# complete genestree
		self.addSuperNodes2gtree()
		self.genestree=self.tc.ctree


	def getGen2Leavs(self):
		# obtain hash { gen : [ list of protein expretions] }
		self.gen2leavs={}
		for leav in self.tree.leavs:
			gen=self.genesdict.label2gen[leav.label]
			if(gen in self.gen2leavs): self.gen2leavs[gen]+=[leav]
			else:  self.gen2leavs[gen]=[leav]

	def add2SuperNode(self,node,gen):
		self.genSupernode[gen]+=[node]
		self.colapsedEvents[node]=gen

	def getSuperNodes(self):
		# for each gen obtain the LCA of it's proteins in tree (LCA should be type P)
		# then for each gen obtain a superNode cointaing all the nodes in the sub tree induced by duplicationForProteinsOfGen[gen]
		# also create a hash of colapsed nodes to it's gen
		self.genSupernode={}
		self.colapsedEvents={}
		self.gen2LCA={}
		for gen in self.gen2leavs:
			leavs=self.gen2leavs[gen]
			genLCA=self.tree.nodesLastCommonAncestor(leavs)
			self.gen2LCA[gen]=genLCA
			self.genSupernode[gen]=[]
			bfs=order.Tour(genLCA,kind="BFS")
			bfs.actions=[lambda node:self.add2SuperNode(node,gen)]
			bfs.restriction=lambda node: sum([leav.isNodeInPath(node) for leav in leavs])==0
			bfs.go()

	def getGenInducedBrother(self,node):
		gen=self.colapsedEvents[node]
		if(gen not in self.genInducedBrother):
			self.genInducedBrother[gen]=Node(type="P")
			genLCA=self.gen2LCA[gen]
			genSuperNodeDad=genLCA.dad
			if(genSuperNodeDad not in self.tc.tree2ctree):
				self.addNodeCopy(genSuperNodeDad)
			self.tc.addNodeCopy(node,nodeCopy=self.genInducedBrother[gen],dad=self.tc.tree2ctree[genSuperNodeDad])

	def addSuperNodes2gtree(self):
		# for each gen obtain it's supernode's dad and add the gen
		self.genInducedBrother={}
		for gen in self.gen2LCA: self.addGen2gtree(gen)

	def addGen2gtree(self,gen):
		leav=Node(label=gen,type=self.gen2leavs[gen][0].type)
		genLCA=self.gen2LCA[gen]
		genSuperNodeDad=genLCA.dad
		if(genSuperNodeDad not in self.tc.tree2ctree): self.addNodeCopy(genSuperNodeDad)
		self.tc.addNodeCopy(genLCA,nodeCopy=leav,dad=self.tc.tree2ctree[genSuperNodeDad])

	def addNodeCopy(self,node,cnode=None):
		# obtener padre
		dad=node.dad
		# si el padre es colapsado
		if(dad in self.colapsedEvents):
			self.getGenInducedBrother(dad)
		elif(dad not in self.tc.tree2ctree):
			self.addNodeCopy(dad)
		copyDad=self.tc.tree2ctree[dad]
		self.tc.addNodeCopy(node,nodeCopy=cnode,dad=copyDad)

class treeCopier:

	def __init__(self,tree,restrictors=[]):
		self.tree=tree
		self.tree2ctree={}
		self.ctree=Tree(type=tree.type,label2spice=tree.label2spice,weighted=tree.weighted)
		posOrder=order.Tour(self.tree.root,kind="DFS",DFSOrder="pos",actions=[self.getNodeCopy])
		posOrder.go()
		self.ctree.addNode(node=self.tree2ctree[tree.root])
		self.ctree.triplets=deepcopy(tree.triplets)
		self.copyLeavsRelations()

	def copyLeavsRelations(self):
		for leaveType in self.tree.type2leaves:
			self.ctree.type2leaves[leaveType]=[]
			for leave in self.tree.type2leaves[leaveType]:
				self.ctree.type2leaves[leaveType]+=[self.tree2ctree[leave]]

	def getNodeCopy(self,node):
		nodeCopy=Node(label=node.label,tree=self.ctree,path=node.path[:],type=node.type,weight=node.weight,edgesGain={},edgesLose={})
		for soon in node.soons:
			copySoon=self.tree2ctree[soon]
			nodeCopy.addSoon(node=copySoon)
			nodeCopy.edgesGain[copySoon]=node.edgesGain[soon]
			nodeCopy.edgesLose[copySoon]=node.edgesLose[soon]
		# agregar copia de tripletas de nodo, path y hojas
		nodeCopy.triplets=deepcopy(node.triplets)
		nodeCopy.inducedLeavesType=node.inducedLeavesType[:]
		self.tree2ctree[node]=nodeCopy


class Tree:
	treeTypes=["spices","genes","proteins"]

	def __init__(self,newick=None,label=None,root=None,createRoot=False,type=None,\
		label2spice=None,type2leaves=None,weighted=False):
		self.initAtributes(label,root,createRoot,type,type2leaves,weighted,label2spice)
		if(newick!=None):
			NewickReader(newick,tree=self,label2spice=label2spice,weighted=weighted)

	def __repr__(self):
		return self.root.__repr__()

	def nodesLastCommonAncestor(self,L):
		if(len(L)>1):
			maxPathLen=max([len(node.path) for node in L])
			f=False
			for i in range(maxPathLen):
				pathElements=[node.path[i] if len(node.path)>i else None for node in L]
				if(None in pathElements or False in [pathElement==pathElements[0] for pathElement in pathElements]):
					f=True
					break
			if(f):
				return self.nodeOfPath(L[0].path[:i])
			else: return L[0]
		else:
			if(len(L)==1):
				return L[0]

	def typesLastCommonAncestor(self,L):
		leaves=[]
		for Leavetype in L:
			leaves+=self.type2leaves[Leavetype]
		return self.nodesLastCommonAncestor(leaves)

	def nodeOfPath(self,path):
		node=self.root
		for i in path:
			node=node.soons[i]
		return node

	def initAtributes(self,label,root,createRoot,type,type2leaves,weighted,label2spice):
		self.type=type
		self.label=label
		self.leavs=[]
		self.superTrees=[]
		self.root=root
		self.weighted=weighted
		self.label2spice=label2spice
		if(root==None):
			if(createRoot):
				self.addNode()
				self.root.setAge()
			else: self.root=None
		else: self.addNode(node=root)
		if(type2leaves==None): self.type2leaves={}
		else: self.type2leaves=type2leaves


	def addNode(self,node=None,dad=None,label=None,soons=[]):
		if(node==None):
			node=Node(label=label,dad=dad)
		node.tree=self
		# Adds node soon of dad
		# if no dad --> soon of root
		# 	if no root --> creates it is the new root
		if(dad==None):
			if(self.root==None):
				self.root=node
				node.path=[]
			else:
				self.root.addSoon(node=node)
				dad=self.root
		else:
			dad.addSoon(node=node)
		self.fixLeaves(dad)

	def fixLeaves(self,node):
		bfs=order.Tour(node,kind="BFS",actions=[lambda node:self.concatenateLeaf(node)],\
			condition=lambda node: node.isLeaf(),\
			elseActions=[lambda node:self.deConcatenateLeaf(node)])
		bfs.go()

	def concatenateLeaf(self,node):
		if(node not in self.leavs): self.leavs+=[node]

	def deConcatenateLeaf(self,node):
		f=False
		for i in range(len(self.leavs)):
			if(node==self.leavs[i]):
				f=True
				break
		if(f): del self.leavs[i]

	def tripletList(self):
		L=[]
		for A in self.triplets:
			for a in A:
				for B in self.triplets[A]:
					for b1,b2 in combinations(B,2): L+=[(a,b1,b2)]
		return L

	def load(getFileName,name=None,dif=None,weight=False,type=None,label2spice=None):
		filePath,fileName=getFileName("Trees/","newick",name=name,dif=dif)
		newickFile=filePath+fileName
		with open(newickFile) as F: newick=F.readline().split(";")[0]
		return Tree(newick=newick,type=type,label2spice=label2spice)

	def save(self,getFileName,name=None,dif=None,weight=False,autoCorrectDif=False):
		filePath,fileName=getFileName("Trees/","newick",name="{}.{}".format(name,self.type),dif=dif,autoCorrectDif=autoCorrectDif)
		if(fileName in os.listdir(filePath)):
			print("error: {} alrredy exists".format(filePath+fileName))
		else:
			with open(filePath+fileName,"w") as F:
				F.write(self.root.labelAsSuperNode(type=type,weight=weight)+";")

	def getFileName(self,project,path,name=None,dif=None,weight=False,autoCorrectDif=False):
		filePath="{}{}/Summary/".format(path,project)
		if(dif==None):
			dif=""
			difInt=1
		else: dif="-{}".format(dif)
		if(name==None): name=project
		fileName="{}.{}{}.newick".format(name,self.type,dif)
		if(autoCorrectDif):
			files=os.listdir(filePath)
			while(fileName in files):
				dif="-{}".format(difInt)
				difInt+=1
				fileName="{}.{}{}.newick".format(name,self.type,dif)
		return filePath,fileName

class Node:

	idCounter=0

	def __init__(self,label=None,soons=None,dad=None,tree=None,type=None,path=None,\
			weight=None,edgesGain=None,edgesLose=None,age=None):
		# soons is a list of nodes
		if(soons==None): self.soons=[]
		else: self.soons=soons
		# dad is a node
		if(label==None):
			self.label=Node.idCounter
			Node.idCounter+=1
		else: self.label=label
		self.type=type
		self.dad=dad
		self.tree=tree
		self.path=path
		self.weight=weight	# int
		self.edgesGain=edgesGain	# { soon : int}
		self.edgesLose=edgesLose	# { soon : int}
		self.age=age

	def __repr__(self):
		return "{} {}".format(self.label,self.soons if len(self.soons)>0 else "")

	def addSoon(self,node=None):
		if(node==None): node=Node()
		node.dad=self
		self.soons+=[node]
		if(self.path!=None):
			node.path=self.path+[len(self.soons)-1]
			node.setAge()
		else: node.path=None
		if(self.weight!=None):
			self.edgesLose[node]=0
			self.edgesGain[node]=0

	def addTriplets(self):
		self.triplets=Tripletes(self).outGroups
		for A in self.triplets:
			for B in self.triplets[A]:
				if(A in self.tree.triplets): self.tree.triplets[A]+=[B]
				else: self.tree.triplets[A]=[B]

	def isLeaf(self):
		return len(self.soons)==0

	def labelAsSuperNode(self,type=False,weight=False):
		if(self.isLeaf()):
			if(weight): return "{}:{}_{}_{}".format(self.label,self.weight,self.dad.edgesGain[self],self.dad.edgesLose[self])
			else: return self.label
		else:
			ret="("
			for soon in self.soons[:-1]: ret+=soon.labelAsSuperNode(type=type,weight=weight)+","
			ret+=self.soons[-1].labelAsSuperNode(type=type,weight=weight)+")"
			if(type): ret+=self.type
			if(weight):
				if(self.dad==None): ret+=":"+str(self.weight)
				else:
					ret+=":{}_{}_{}".format(self.weight,self.dad.edgesGain[self],self.dad.edgesLose[self])
			return ret

	def nodesList2labelsList(self,L):
		return [node.label for node in L]

	def nodesList2typesList(self,L):
		return [node.type for node in L]

	def getInducedLeaves(self):
		leaves=[]
		def addLeave(node,leaves):
			leaves+=[node]
		bfs=order.Tour(self,kind="BFS",actions=[lambda node:addLeave(node,leaves)],\
			condition=lambda node: node.isLeaf())
		bfs.go()
		return leaves

	def setInducedLeavesType(self):
		if(self.isLeaf()):
			self.inducedLeavesType=tuple([self.type])
			self.inducedLeaves=tuple([self])
			if(self.type in self.tree.type2leaves): self.tree.type2leaves[self.type]+=[self]
			else: self.tree.type2leaves[self.type]=[self]
		else:
			soonInducedLeavesType=[]
			soonInducedLeaves=[]
			for soon in self.soons:
				soon.setInducedLeavesType()
				soonInducedLeavesType+=soon.inducedLeavesType
				soonInducedLeaves+=soon.inducedLeaves
			soonInducedLeaves=list(set(soonInducedLeaves))
			soonInducedLeavesStr=[leave.label for leave in soonInducedLeaves]
			ordered=np.argsort(soonInducedLeavesStr)
			self.inducedLeavesType=tuple(sorted(set(soonInducedLeavesType)))
			self.inducedLeaves=tuple([soonInducedLeaves[i] for i in ordered])


	def sumGene(self):
		self.weight+=1

	def loseGene(self):
		self.dad.edgesLose[self]+=1

	def duplicateGene(self,duplications):
		self.dad.edgesGain[self]+=duplications

	def isNodeInPath(self,node):
		if(len(self.path)>=len(node.path)):
			l=len(node.path)
			return node.path==self.path[:l]
		else: return False

	def setAge(self):
		self.age=len(self.path)

class Tripletes:
	def __init__(self,node):
	# creates triplets sets of a node od the kind (A|B)
	# where len(A)>=1, len(B)>=2
	# Each set (A|B) represntes the posible triplets:
	#	( a_i | b_j , b_k )
	#	for:
	#		a_i in A
	#		b_j,k in B
	#		j!=k
	#	note that A.intersection(B)=Null_set
		self.node=node
		self.outGroups={} # Dict of outgroup : ingroup = { A : [B_0,b_1,...] }
		self.inGroups={} # { B : A_0, idx of B in A }
		if(node.type=="S"):
			for soon in node.soons:
				self.A=tuple(sorted(set(node.nodesList2typesList(soon.getInducedLeaves()))))
				for soon1 in node.soons:
					if(soon!=soon1):
						self.addInGroups(soon,soon1)

	def addInGroups(self,soon,soon1):
		self.B=set()	#(self.B)
		if(soon1.type in ["S","P"]):
			for soon11 in soon1.soons: self.addNodeLeaves(soon11)
			self.fixDictionarys()
		""" 
		if(soon1.type=="S"):
			for soon11 in soon1.soons: self.addNodeLeaves(soon11)
			self.fixDictionarys()
		elif(soon1.type=="P"):
			for soon11 in soon1.soons:
				self.B=set()
				for soon111 in soon11.soons: self.addNodeLeaves(soon111)
				self.fixDictionarys()
		"""

	def addNodeLeaves(self,node):
		for type in node.nodesList2typesList(node.getInducedLeaves()): self.B.add(type)

	def fixDictionarys(self):
		if(len(self.B)>1): self.addOutGroup()

	def addOutGroup(self):
		self.B=tuple(sorted(self.B))
		if(self.A in self.outGroups):
			if(self.B not in self.outGroups[self.A]): self.outGroups[self.A]+=[self.B]
		else: self.outGroups[self.A]=[self.B]

class NewickReader:

	indicators={")":None,"(":None,",":None}

	def __init__(self,newick,tree=None,root=None,treeType=None,label2spice=None,weighted=True):
		self.initAtributes(newick,tree,root,treeType,label2spice,weighted)
		# La string se va leyendo de derecha a izquierda
		# Con forme va leyendo genera árbol
		# Se construye como un dfs
		self.readItem()
		self.createItem()
		while(self.pointer<self.newickLen):
			self.readItem()
			self.createItem()
		self.tree.root.setInducedLeavesType()
		self.tree.root.setAge()


	def createItem(self):
		if(self.pointerElement==")"):	# Create a node, go to node
			node=self.createNode(self)
			self.tree.addNode(node=node,dad=self.actualNode)
			self.setDad2nodeWeights(node)
			self.actualNode=node
		elif(self.pointerElement=="("):	# create leaf if avalibre, set leaf and actual node induced leavs ,set node triplets ,retuen to dad
			if(self.item!=""):
				node=self.createNode(self)
				node.type=self.label2spice(node.label)
				self.tree.addNode(node=node,dad=self.actualNode)
				self.setDad2nodeWeights(node)
				node.addTriplets()		
			self.actualNode.addTriplets()
			self.actualNode=self.actualNode.dad
		elif(self.pointerElement==","):	# create leaf if avalibre, set leaf induced leavs
			if(self.item!=""):
				node=self.createNode(self)
				node.type=self.label2spice(node.label)
				self.tree.addNode(node=node,dad=self.actualNode)
				self.setDad2nodeWeights(node)
				node.addTriplets()		

	def setDad2nodeWeights(self,node):
		if(len(self.dad2nodeWeights)==2):
			node.dad.edgesGain[node]=self.dad2nodeWeights[0]
			node.dad.edgesLose[node]=self.dad2nodeWeights[1]

	def typeNoWehgtDecomposeItem(self,node):
		if(self.item!=""):
			node.type=self.item
			node.label=self.item
		self.dad2nodeWeights=[]

	def typeWehgtDecomposeItem(self,node):
		if(self.item==""):
			node.type=None
#			node.weight=None
			self.dad2nodeWeights=[]
		else:
			type,weights=self.item.split(":")
			if(len(type)>0): #node.type=None
				node.type=type
				node.label=type
			self.getNewikcWeight(weights,node)

	def labelNoWehgtDecomposeItem(self,node):
		if(self.item!=""): node.label=self.item
		self.dad2nodeWeights=[]

	def labelWehgtDecomposeItem(self,node):
		if(self.item==""):
			node.label=None
			node.weight=None
			self.dad2nodeWeights=[]
		else:
			label,weights=self.item.split(":")
			if(len(label)>0): node.label=label
			self.getNewikcWeight(weights,node)

	def getNewikcWeight(self,weights,node):
		weights=weights.split("_")
		node.weight=int(weights[0])
		if(len(weights)>1): self.dad2nodeWeights=[int(weights[1]),int(weights[2])]
		else: self.dad2nodeWeights=[]


	def item2event(self):
		node=Node()
		if(self.weighted): self.typeWehgtDecomposeItem(node)
		else: self.typeNoWehgtDecomposeItem(node)
		return node

	def item2label(self):
		node=Node(type="S",weight=0,edgesGain={},edgesLose={})
		if(self.weighted): self.labelWehgtDecomposeItem(node)
		else: self.labelNoWehgtDecomposeItem(node)
		return node

	def itemIgnore(self):
		return Node()

	def readItem(self):
		self.item=""
		self.getPointerElement()
		while((self.pointerElement not in NewickReader.indicators)&(self.pointer<self.newickLen)):
			self.getPointerElement()
		self.item=self.item[::-1][1:]

	def getPointerElement(self):
		self.pointerElement=self.newick[self.newickLen-1-self.pointer]
		self.item+=self.pointerElement
		self.pointer+=1

	def initAtributes(self,newick,tree,root,treeType,label2spice,weighted):
		self.initTree(tree,root)
		self.actualNode=self.root
		self.initItemInterpretation()
		self.newick=newick
		self.newickLen=len(newick)
		self.pointer=0
		self.item=""
		self.pointerElement=""
		if(label2spice==None): self.label2spice=lambda label:label
		else: self.label2spice=lambda label:label2spice[label]
		self.weighted=weighted

	def initTree(self,tree,root):
		if(tree==None):
			if(root==None):
				self.tree=Tree(createRoot=False,type=treeType)
				self.root=self.tree.root
			else:
				self.tree=root.tree
				self.root=root
		else:
			self.tree=tree
			if(root==None): self.root=tree.root
			else: self.root=root
		self.tree.triplets={}

	def initItemInterpretation(self):
		if(self.tree.type=="spices"): self.createNode=lambda reader: reader.item2label()
		elif(self.tree.type=="genes"): self.createNode=lambda reader: reader.item2event()
		elif(self.tree.type=="proteins"): self.createNode=lambda reader: reader.item2event()
		elif(self.tree.type==None): self.createNode=lambda reader: reader.itemIgnore()

class Recon:
	def __init__(self,t,T):
		# Reconciliates genes Tree t with spices tree T
		self.t=t
		self.T=T
		self.arecongruent,self.incongruets=areCongruent(t,T,returnIncongrunts=True)
		self.edades={}
		if(self.arecongruent):
			self.mapRoot()
			bfs=order.Tour(self.t.root)
			bfs.restriction=lambda node: node.isLeaf()
			bfs.actions=[lambda node: self.mapEvent(node)]
			bfs.go()
#		else:
#			print("pos no son congruentes :v\n{}\n".format(self.incongruets))

	def mapRoot(self):
		# maps the genes loses for spices of T not apearing in the branch of T induced by
		#	the las common ancestor of the spices involved in t.
		targets=self.t.root.inducedLeavesType
		nodeRestriction=self.T.typesLastCommonAncestor(targets)
		restriction=lambda node: node==nodeRestriction
		self.inherit(targets,restriction,root=self.T.root)
		self.t.genesAge=nodeRestriction.age

	def mapEvent(self,node):
		if(node.type=="S"): self.mapSpetiation(node)
		elif(node.type=="P"): self.mapDuplication(node)

	def mapSpetiation(self,node):
		restrictors=self.getSpetiationRestrictors(node)
		self.inherit(node.inducedLeavesType,restriction=lambda node: node in restrictors)

	def mapDuplication(self,node):
		dupNodeRoot=self.T.typesLastCommonAncestor(node.inducedLeavesType)
		dupNodeRoot.duplicateGene(len(node.soons)-1)
		for soon in node.soons:
			restrictor=self.getDuplicationRestrictor(soon)
			self.inherit(soon.inducedLeavesType,restriction=lambda node: node==restrictor,root=dupNodeRoot)

	def inherit(self,targetTypes,restriction,root=None):
		self.ttargetTypes=targetTypes	
		if(root==None): root=self.T.typesLastCommonAncestor(targetTypes)
		bfs=order.Tour(root)
		bfs.actions=[lambda node:node.sumGene()]
		bfs.condition=lambda node: len(set(targetTypes).intersection(node.inducedLeavesType))>0
		bfs.elseActions=[lambda node: node.loseGene(),lambda node: bfs.quitSoonFromItems(node)]
		bfs.quitSoonFromItemsCondition=restriction
		bfs.go()

	def getSpetiationRestrictors(self,node):
		restrictors=[]
		for soon in node.soons:
			if(not soon.isLeaf()): restrictors+=[self.T.typesLastCommonAncestor(soon.inducedLeavesType)]
		return restrictors

	def getDuplicationRestrictor(self,node):
		if(node.isLeaf()): return None
		else: return self.T.typesLastCommonAncestor(node.inducedLeavesType)

def identity(x):
	return x

def x2True(x):
	return True

class reconMT:

	def __init__(self,project,path,speciesTree,genesdicts,identifierKind,secuencesType,getFileName,\
			filterType=None,listFilter=None,filterPreFunction=identity,include_edited=True):
		self.initAtributes(project,path,speciesTree,genesdicts,identifierKind,secuencesType,getFileName,filterPreFunction,filterType,\
			listFilter=listFilter,include_edited=include_edited)
		self.go()

	def go(self):
		print("recon all trees")
		files=os.listdir(self.path+"Trees/")
		l=len(files)
		I=0.0
		for i in range(len(files)):
			file=files[i]
			progress=i/(l-1)
			if(progress>I):
				print("progress: {}".format(progress))
				I+=0.05
			if(self.filesFilter(file)):
				dif="-{}".format(file.split("-")[1].split(".")[0])
				with open(self.path+"Trees/"+file) as F: newick=F.readline().split(";")[0]
				self.treeCopies[dif]=treeCopier(self.T)
				individualTree=self.treeCopies[dif].ctree
				t=Tree(newick=newick,type=self.identifierKind,label2spice=self.label2spice)
				t.cc=dif
				# Revisa que se cumpla la condición para reconciliar árboles de genes
				if(self.genesTreeFilter(t)):
					self.filteredList+=[t.cc]
					self.t=t
					self.TleavsWeights={}
					self.tleavesTypeContribution={}
					for leave in self.T.leavs:
						self.TleavsWeights[leave.type]=leave.weight
						self.tleavesTypeContribution[leave.type]=0
					rec=Recon(self.t,individualTree)
					if(rec.arecongruent):
						self.saveAges()
						types={}									
						for leav in self.t.leavs:								
							leaveType=leav.type							
							if(leaveType in types): types[leaveType]+=1				
							else: types[leaveType]=1						
						for leaveType in types:								
							if(types[leaveType]!=individualTree.type2leaves[leaveType][0].weight):	
								print("------ not congruent leaves {}".format(t.cc))			
					else:
						if(t.cc not in self.incongruentCC): self.incongruentCC+=[t.cc]
		self.sumIndividualTrees()
		self.isCongruent,self.incongruents=wasCorrectMap(self.T,returnIncongruents=True)

	def sumIndividualTrees(self):
		tour=order.Tour(self.T.root,actions=[lambda node: self.sumIndividualNodes(node)])
		tour.go()

	def sumIndividualNodes(self,node):
		# Sums the atributes of the copyes of node to the atributes of node
		# the atributes are: weight, edgesGain and edgesLose
		for treeCopy in self.treeCopies:
			nodeCopy=self.treeCopies[treeCopy].tree2ctree[node]
			node.weight+=nodeCopy.weight
			for soon in node.edgesGain:
				soonCopy=self.treeCopies[treeCopy].tree2ctree[soon]
				node.edgesGain[soon]+=nodeCopy.edgesGain[soonCopy]
				node.edgesLose[soon]+=nodeCopy.edgesLose[soonCopy]

	def saveAges(self):
		self.CC2genes[self.t.cc]=[]
		self.CC2proteins[self.t.cc]=[]
		for node in self.t.leavs:
			self.saveLeaveInformation(node)

	def saveGenAge(self,node):
		gen=node.label
		spice=node.type
		self.genesAge[gen]=self.t.genesAge
		self.genesCC[gen]=self.t.cc
		# save spice and genes family
		self.genesSpice[gen]=spice
		self.spice2genes[spice]+=[gen]
		self.CC2genes[self.t.cc]+=[gen]
		# guardar ortólogos
		self.genOrthologous[gen]={spice1:[] for spice1 in self.spices}
		for ortholog in getLeavOrthologs(node):
			gen1=ortholog.label
			spice1=ortholog.type
			self.genOrthologous[gen][spice1]+=[gen1]

	def saveProteinAge(self,node):
		protein=node.label
		spice=node.type
		self.proteinsAge[protein]=self.t.genesAge
		self.proteinsCC[protein]=self.t.cc
		# save spice and genes family
		self.proteinsSpice[protein]=spice
		self.spice2proteins[spice]+=[protein]
		self.CC2proteins[self.t.cc]+=[protein]
		# guarda ortólogos
		self.proteinOrthologous[protein]={spice1:[] for spice1 in self.spices}
		for ortholog in getLeavOrthologs(node):
			protein1=ortholog.label
			spice1=ortholog.type	#self.genesdicts.protein2spice[ortholog]
			self.proteinOrthologous[protein][spice1]+=[protein1]

	def initAtributes(self,project,path,speciesTree,genesdicts,\
			identifierKind,secuencesType,getFileName,filterPreFunction,filterType,listFilter=None,include_edited=True):
		self.getFileName=getFileName
		# Load species tree
		self.speciesTree=speciesTree
		self.T=Tree(newick=open(self.speciesTree).readline().split(';')[0],type="spices")
		self.treeCopies={}
		self.projectName=project
		self.projectPath=path
		self.path=path+project+"/"
		if(listFilter!=None):self.listFilter=listFilter
		else: self.listFilter=[]
		self.filteredList=[]
		# para guardar edades
		self.genesAge={}	# { gen : age }
		self.proteinsAge={}
		self.genesCC={}	 # { gen : componente conexa }
		self.proteinsCC={}
		self.CC2genes={}	# { CC : [list f genes] }
		self.CC2proteins={}
		self.genesSpice={}
		self.proteinsSpice={}
		# para guardar ortólogos
		self.proteinOrthologous={}
		self.genOrthologous={}
		self.identifierKind=identifierKind
		self.secuencesType=secuencesType
		# filtto de genes
		self.filterPreFunction=filterPreFunction
		# Lista para guardar árboles no congruntes
		self.incongruentCC=[]
		# Definir consición para reconciliar o no a un grafo
		self.condition=x2True	
#		if(include_edited): self.condition=lambda f:(project+"_" in f[0])&("nPrime"==f[-2])
#		else: self.condition= lambda f: (project+"_" in f[0])&("nPrime"==f[-2])&(".edited" not in f[1])
		# Load dictionary of real genes names
		self.genesdicts=genesdicts
		# spices list
		self.spices=list(self.genesdicts.spices2labels)
		self.spice2genes={spice: [] for spice in self.spices}
		self.spice2proteins={spice: [] for spice in self.spices}
		# Definir función para obtener identificadores y salvar información de genes
		self.identifierKind=identifierKind
		if(identifierKind=="genes"):
			# identifier-spice dicts
			self.label2spice=self.genesdicts.gen2spice
			self.filesFilter=self.genesFilesFilter
			# save identifiers information
			self.saveLeaveInformation=self.saveGenAge
			self.printIdentifierInformation=self.printGenAge
			self.getOrthologous=self.getGenOrthologous
		elif(identifierKind=="proteins"):
			# identifier-spice dicts
			self.label2spice=self.genesdicts.protein2spice								
			self.filesFilter=self.proteinsFilesFilter
			# save identifiers information
			self.saveLeaveInformation=self.saveProteinAge
			self.printIdentifierInformation=self.printProteinAge
			self.getOrthologous=self.getProteinOrthologous
		else: print("reconMT initAtributes error: identifierkind '{}' not valid. label2identifiers not deined".format(secuencesType))	
		# Definir funcipon para filtrar árboles de genes
		self.filterType=filterType
		if(self.filterType==None): self.genesTreeFilter=x2True
		elif(self.filterType=="MOUSE"):
			def genesTreeFilter(t):		# está chafa este filtro 	
				return True in ["MOUSE" in name for name in t.leavs]
			self.genesTreeFilter=genesTreeFilter
		elif(self.filterType=="genesList"):
			self.genesTreeFilter=self.identifiersFilterF
			if(self.identifierKind=="genes"): self.identifier2label=identity
			elif(self.identifierKind=="proteins"):
				def identifier2label(label):
					return self.genesdicts.protein2gen[label]
				self.identifier2label=identifier2label
		elif(self.filterType=="proteinsList"):
			self.genesTreeFilter=self.identifiersFilterF
			if(self.identifierKind=="proteins"): self.identifier2label=identity
			elif(self.identifierKind=="genes"):
				def identifier2label(label):
					return self.genesdicts.gen2protein[label]
				self.identifier2label=identifier2label
		elif(self.filterType=="treesList"): self.genesTreeFilter=self.treesFilter

	def genesFilesFilter(self,file):
		return self.projectName in file and "-" in file and "genes" in file

	def proteinsFilesFilter(self,file):
		return self.projectName in file and "-" in file and "proteins" in file

	def identifiersFilterF(self,t):
		flag=False
		for leav in t.leavs:
			leav=self.identifier2label(leav.label)
			leav=self.filterPreFunction(leav)
			if(leav in self.listFilter):
				flag=True
				break
		return flag

	def treesFilter(self,t):
		return t.cc in self.listFilter

	def printAges(self,spice,printNoOrthologous=False):
		self.printeds=[]	
		# spices iterator
		self.spisIter=[]
		for spice1 in self.spices:
			if(spice!=spice1): self.spisIter+=[spice1]
		# Prepara diccionarioss label-gen y archivos de salida
		self.spice=spice
		# anotar edad de los genes de spice
		self.files={spice1:open(self.path+"Summary/"+self.projectName+"."+spice+"_"+spice1+"."+self.identifierKind+".edgesAge","w") for spice1 in self.spisIter}
		with open(self.path+"Summary/"+self.projectName+"."+spice+"_orthologous"+"."+self.identifierKind+".edgesAge","w") as F:
			# imprimir header para archivo general
			self.F=F
			F.write("genes_family,age\t"+spice+"\t")
			for spice1 in self.spisIter[:-1]: F.write(spice1+"\t")
			F.write(self.spisIter[-1]+"\n")
			# Imprimir header para archivos a pares
			for spisF in self.files: self.files[spisF].write("genes_family,age\t"+spice+"\t"+spisF+"\n")
			# imprimir todos los genes de la especie especificada
			if(self.identifierKind=="genes"):
				for gen in self.spice2genes[self.spice]:
					age=self.genesAge[gen]
					# cargar componente conexa para ver aristas
					cc=self.genesCC[gen].split(".")[0]
					# imprime edades
					self.printIdentifierInformation(gen,age,cc,spice1)
			elif(self.identifierKind=="proteins"):
				for protein in self.spice2proteins[self.spice]:
					age=self.proteinsAge[protein]
					cc=self.proteinsCC[protein]
					# imprime edades
					self.printIdentifierInformation(protein,age,cc,spice1)
		for spis in self.files: self.files[spis].close()
		del self.F
		del self.files

	def printNoOrthologous(self,spice):
		with open(self.path+"Summary/"+self.projectName+"."+spice+"_notOrthologous"+"."+self.identifierKind+".edgesAge","w") as F:
			F.write("genes_family,age\t"+spice+"\t")
			for spice1 in self.spisIter[:-1]: F.write(spice1+"\t")
			F.write(self.spisIter[-1]+"\n")
			if(self.filterType=="genesList"):
				primero=lambda item: F.write("-,-\t{}\t".format(item))
			elif(self.filterType=="treesList"):
				primero=lambda item: F.write("{},-\t-\t".format(item))
			for item in self.listFilter:
				if(item not in self.filteredList):
					primero(item)
					for spice1 in self.spisIter[:-1]:
						F.write("-\t")
					F.write("-\n")

	def getGenOrthologous(self,gen,G):
		self.orth={spice1:[] for spice1 in self.spisIter}
		for gen1 in G.edge[gen]:
			spice1=self.genesdicts.gen2spice[gen1]
			if(gen1 not in self.orth[spice1]): self.orth[spice1]+=[gen1]


	def getProteinOrthologous(self,protein,G):
		self.orth={spice1:[] for spice1 in self.spisIter}
		for protein1 in G.edge[protein]:
			spice1=self.genesdicts.protein2spice[protein1]
			if(protein1 not in self.orth[spice1]): self.orth[spice1]+=[protein1]

	def printProteinAge(self,gen,age,cc,spice1):
		if(gen in self.printeds): print("{} repeated from {} in printProteinAge".format(gen,cc))	
		else: self.printeds+=[gen]						
		# escribir a archivos a pares
		for spisF in self.files:
			for gen1 in self.proteinOrthologous[gen][spisF]:
				self.files[spisF].write("{},{}\t{}\t{}\n".format(cc,age,gen,gen1))
		# imprimir archivo general
		self.F.write("{},{}\t{}\t".format(cc,age,gen))
		for spice1 in self.spisIter[:-1]:
			if(len(self.proteinOrthologous[gen][spice1])>0):
				for gen1 in self.proteinOrthologous[gen][spice1][:-1]: self.F.write("{}|".format(gen1))
				self.F.write("{}\t".format(self.proteinOrthologous[gen][spice1][-1]))
			else: self.F.write("-\t")
		spice1=self.spisIter[-1]
		if(len(self.proteinOrthologous[gen][spice1])>0):
			for gen1 in self.proteinOrthologous[gen][spice1][:-1]: self.F.write("{}|".format(gen1))
			self.F.write("{}\n".format(self.proteinOrthologous[gen][spice1][-1]))
		else: self.F.write("-\n")

	def printGenAge(self,gen,age,cc,spice1):
		if(gen in self.printeds): print("{} repeated from {} in printGenAge".format(gen,cc))	
		else: self.printeds+=[gen]	
		# escribir a archivos a pares
		for spisF in self.files:
			for gen1 in self.genOrthologous[gen][spisF]:
				self.files[spisF].write("{},{}\t{}\t{}\n".format(cc,age,gen,gen1))
		# imprimir archivo general
		self.F.write("{},{}\t{}\t".format(cc,age,gen))
		for spice1 in self.spisIter[:-1]:
			if(len(self.genOrthologous[gen][spice1])>0):
				for gen1 in self.genOrthologous[gen][spice1][:-1]: self.F.write("{}|".format(gen1))
				self.F.write("{}\t".format(self.genOrthologous[gen][spice1][-1]))
			else: self.F.write("-\t")
		spice1=self.spisIter[-1]
		if(len(self.genOrthologous[gen][spice1])>0):
			for gen1 in self.genOrthologous[gen][spice1][:-1]: self.F.write("{}|".format(gen1))
			self.F.write("{}\n".format(self.genOrthologous[gen][spice1][-1]))
		else: self.F.write("-\n")

class plotTree:

	def __init__(self,tree,lx=1,ly=1,name="flujo.png",saveAndClose=False):
		# Inicializar características del dibujo
		self.tree=tree
		self.getDiameter()
#		plt.figure(figsize=(10,25))
		self.lx=lx
		self.ly=ly
		self.dx=lx/(self.diameter+1)
		self.dy=self.ly/(len(tree.leavs)+1)
		self.xNodes={}
		self.yNodes={}
		self.y=self.dy
		# Graficar raíz
		plt.axes(frame_on=False)
		plt.xticks([])
		plt.yticks([])
		plt.plot([-self.dx,0],[self.ly/2,self.ly/2],color="black")
		# Inicializar bottomUp
		bottomup=order.Tour(tree.root,kind="bottomUp")
		bottomup.actions=[lambda node: self.plotNodeAndSoons(node)]
		bottomup.go()
		if(saveAndClose): plt.savefig(name)

	def plotNodeAndSoons(self,node):
		# Calcular pos de hojas
		for soon in node.soons:
			if(soon.isLeaf()):
				self.xNodes[soon]=self.lx
				self.yNodes[soon]=self.y
				plt.text(self.lx,self.y+0.2*self.dy,soon.label)
				plt.text(self.xNodes[soon]+0.1*self.dx,self.yNodes[soon]-0.3*self.dy,"{}".format(soon.weight),color="blue")
				self.y+=self.dy
		# calcular posición del nodo
		self.xNodes[node]=len(node.path)*self.dx
		indY=[self.yNodes[indLeav] for indLeav in node.inducedLeaves]
		self.yNodes[node]=(min(indY)+max(indY))/2
		# Crear lineas
		plt.text(self.xNodes[node]+0.1*self.dx,self.yNodes[node],"{}".format(node.weight),color="blue")
		for soon in node.soons:
			X=[self.xNodes[node],self.xNodes[node],self.xNodes[soon]]
			Y=[self.yNodes[node]+0.1*self.dx,self.yNodes[soon],self.yNodes[soon]]
			plt.plot(X,Y,color="black")
			plt.text(self.xNodes[node]+0.1*self.dx,self.yNodes[soon]+0.1*self.dy,"+{}".format(node.edgesGain[soon]),color="green")
			plt.text(self.xNodes[node]+0.1*self.dx,self.yNodes[soon]-0.3*self.dy,"-{}".format(node.edgesLose[soon]),color="red")
#			plt.text(self.xNodes[soon]+0.1*self.dx,self.yNodes[soon]-0.3*self.dy,"{}".format(soon.weight),color="blue")	
		# graficar raíz



	def getDiameter(self):
		self.diameter=len(self.tree.leavs[0].path)
		for leave in self.tree.leavs[1:]:
			l=len(leave.path)
			if(l>self.diameter): self.diameter=l


