import nets as nt

def protocaderinas(filePath,group=None):
	genesFilter=[]
	with open(filePath) as F:
		for line in F:
			gen,protocaderin,genGroup=line.split()
			if(group==None): genesFilter+=[gen]
			elif(group==genGroup): genesFilter+=[gen]
	return genesFilter

def loadSegs(filePath):
	geneslist=[]
	with open(filePath) as F:
		F.readline()
		for line in F:
#			line=line[:-1]
#			gen,begin,end,chr,geneName=line.split()
			gen=line.split()[0]
			geneslist+=[gen]
	return geneslist

def mouse_SEGs_membrana(secuencesPath,TranscriptFormat=False):
	"Carga lista de single exone genes de ratón que son proteínas de membrana y diccionarios gen-clase, donde clase es un subconjunto de {SEG,UP,MEGS,...}"
	# Carga diccionarios de traducción -gen-trasncrpt
	gen2transcripts,transcript2gen=nt.loadEnsembleGenTranscriptDict(secuencesPath)
	# Crea función para guardar genes con iidentificador de trasncript o gen
	if(TranscriptFormat): name=lambda gen : gen2transcripts[gen]
	else: name=lambda gen : gen
	# Carga lista de transcripts y diccionarios de clases-gen
	transcripts=[]
	genes2class={}
	class2genes={}
	with open("/home/antonio/Archivos_katia/mouse_SEGs_membrana") as F:
		for line in F:
			kinds,gen=line.split()
			gen=name(gen)
			transcripts+=[gen]
			kinds=tuple(sorted(kinds.split(",")))
			genes2class[gen]=kinds
			for kind in kinds:
				if kind in class2genes: class2genes[kind]+=[gen]
				else: class2genes[kind]=[gen]
	return transcripts,gene2class,class2gene

def mouse_SEGs(secuencesPath,TranscriptFormat=False):
	"Carga lista de single exon genes de ratón y diccionarios gen-descripción"
	# Carga diccionarios de traducción gen-trasncrpt
	gen2transcripts,transcript2gen=nt.loadEnsembleGenTranscriptDict(secuencesPath)
	# Crea función para guardar genes con iidentificador de trasncript o gen
	if(TranscriptFormat):
		name=lambda gen : gen2transcripts[gen]
		nameCondition=lambda gen:gen in gen2transcripts
	else:
		name=lambda gen : [gen]
		nameCondition=lambda gen:True
	# Crear lista y diccionarios
	lista=[]
	notInFastas=[]
	with open("/home/antonio/Archivos_katia/mouse-single_exon_genes.tsv") as F:
		header=F.readline().split()
		for line in F:
			ls=line.split()
			gen=ls[0]
			if(nameCondition(gen)):
				genes=name(gen)
				lista+=genes
			else:
				notInFastas+=[gen]
	return lista,notInFastas

