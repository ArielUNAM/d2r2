# coding=utf-8
def makeMouseDict():
	# El arrchivo en path0 fue generado por katia
	#	No tiene el formato debido, que es el de path2
	#		Vamos a corregir el formato de path1 a partir de path2 y guardarlo en path1
	path0="/u/scratch/antonio/Secuencias_ensemble_pep/MOUSE.deKatia"
	path1="/u/scratch/antonio/Secuencias_ensemble_pep/MOUSE"
	path2="/u/scratch/antonio/FASTA/MOUSE"
	genesDkatia=[]
	with open(path0) as deKatia:
		for line in deKatia:
			if(line[0]==">"): genesDkatia+=[line[1:-1]]
	with open(path1,"w") as nuevo:
		with open(path2) as viejo:
			for line in viejo:
				if(line[0]==">"):
					lineE=line.split()[0].split(">")[1].split(".")[0]
					if(lineE in genesDkatia):
						nuevo.write(line)
						nuevo.write(viejo.readline())
