#!/usr/bin/python3.4
# coding=utf-8
import os
import shutil
import subprocess
import nets as nt
import pickle
import evolutionaryTree as evo
import tree as tr
import dicts as dct
import draw as dr
from status import Status

def getFilesLabels(path):
	filesLabels={}
	with open(path) as F:
		for line in F:
			file,label=line.split()
			filesLabels[file]=label
	return filesLabels

def clearDirsContent(path):
	# Borra recursivamente todo el contenido de un directorio
	for file in os.listdir(path):
		if(os.path.isfile(path+file)): os.remove(path+file)
		else: shutil.rmtree(path+file)

def load(Project,path):
	with open(path+Project+"/"+"Summary/"+Project+".bmgmd","rb") as F: pro=pickle.load(F)
	# Crea bander de diccionarios
	pro.dictLoaded={}
	return pro

def printNoSteep1(p):
	print("steep 1: project initialization\nmust be done manually with bmgmd.project\nor with bmgmd.project.load"),\

def identity(x):
	return x

def quitPoitToRight(label):
	return label.split(".")[0]

def executeBashCommand(command):
	y=subprocess.check_output(command.split(" "))


# Clase para administrar proyectos de D2R2
class project:
	# Directorios que necesita un proyecto
	dirs=["Dictionarys","Prt","Graphs","Summary","Trees","Tmp","Blast","Edges","Locus","Identifiers"]
	# Secuence kinds
	secuenceKinds=["genes","proteins"]
	# Dictionarys
	dictionarys={"EdgesDict":dct.EdgesDict.load,"GenesDicts":dct.GeneDicts.load,"LocusDicts":dct.LocusDicts.load}
	def __init__(self,projectName,projectPath,sequencesPath=None,fastasLabels=None,fastasFormat=None,speciesTree=None,\
		proteinorthoPath=None,overWrite=False,GTFsPath=None,GTFsLabels=[],secuencesKind=None,identifierKind=None,\
		proteinorthoSteeps=[0],editNocographs=True,reconEditedGrahs=True,reconFilterType=None,reconListFilter=None,\
		identifier2filterFunction=identity):
		print("---steep 1/7 (project creation) proceding...")
		self.status=Status(self)
		# Declarar atrubutos del programa
		self.initAtributes()
		self.status.setAtributesList(\
			["secuences_path","fasta_labels","fastas_format","secuences_kind","identifier_kind","species_tree",\
			"proteinortho","reconciliation_filter","recon_filter_object","prt_steeps","gff_path","gff_labels"],\
			[sequencesPath,fastasLabels,fastasFormat,secuencesKind,identifierKind,speciesTree,\
			proteinorthoPath,reconFilterType,reconListFilter,proteinorthoSteeps,GTFsPath,GTFsLabels])
		self.editNocographs=editNocographs
		self.reconEditedGrahs=reconEditedGrahs
		self.identifier2filterFunction=identifier2filterFunction
		# Inicializa diccionarios del pryecto
		self.dictLoaded={}
		# Crear directorios para el proyecto
		self.setWorkDir(projectPath,projectName,overWrite)
		# save projetc
		self.save()
		print("steep 1/5 (project creation) done")

	def initAtributes(slef):
		self.sequencesPath=None
		self.fastasLabels=None
		self.fastasFormat=None
		self.speciesTree=None
		self.proteinorthoPath=None
		self.reconFilterType=None
		self.reconListFilter=None
		self.proteinorthoSteeps=None
		self.GTFsPath=None
		self.GTFsLabels=None

	def analyze(self,steeps=[]):
		if(len(steeps)==0 or steeps==[0]): steeps=[2,3,4,5,6,7]
		steespList=list(steepFunctions)
		for steep in steeps:
			if(steep in steespList):
				print("---steep {}/7 ({}) proceding...".format(steep,project.steeps[steep]))
				steepFunctions[steep](self)
				self.save()
				print("steep {}/7 ({}) done!".format(steep,project.steeps[steep]))
			else:
				raise AttributeError("steep {} not defined for bmgmd.project.analyze".format(steep))

	def setSecuencesKind(self,secuencesKind):
		if(secuencesKind in project.secuenceKinds):
			self.secuencesKind=secuencesKind
			if(secuencesKind=="genes"):
				self.permitedIdentifiers=["genes"]
				self.proteinorthoKind="n"
			elif(secuencesKind=="proteins"):
				self.permitedIdentifiers=project.secuenceKinds
				self.proteinorthoKind="p"
		else:
			self.permitedIdentifiers=[]
			raise AttributeError("Error: bmgmd.project.setSecuencesKind: secuencesKind={} not defined".format(secuencesKind))

	def setIdentifierKind(self,identifierKind):
		if(identifierKind in project.secuenceKinds):
			if(identifierKind in self.permitedIdentifiers): self.identifierKind=identifierKind
			else: print("Error: bmgmd.project.setidentifierKind: can't set identifierKind={} when secuencesKind={}".format(identifierKind,self.secuencesKind))
		else: print("Error: bmgmd.project.setidentifierKind: identifierKind={} not defined".format(identifierKind))

		if(self.secuencesKind=="genes"):
			if(identifierKind=="genes"): self.identifierKind=identifierKind
			elif(identifierKind=="proteins"): print("Error: bmgmd.project: can't set identifierKind=proteins when secuencesKind=genes")
			else: print("Error: bmgmd.project: identifierkind={} not defined".format(identifierKind))
		elif(self.secuencesKind=="proteins"):
			if(identifierKind=="genes"): self.identifierKind=identifierKind
			elif(identifierKind=="proteins"): self.identifierKind=identifierKind
			else: print("Error: bmgmd.project: identifierkind={} not defined".format(identifierKind))
		else: print("Error: bmgmd.project.setIdentifierKind: can't set secuencesKind=proteins whrn not defined")

	def getFileName(self,dir,ext,name=None,dif=None,autoCorrectDif=False):
		filePath="{}{}".format(self.path,dir)
		if(dif==None):
			dif=""
			difbase=""
		else:
			difbase=dif
			dif="-{}".format(dif)
		difInt=1
		if(name==None): name=self.projectName
		fileName="{}{}.{}".format(name,dif,ext)
		if(autoCorrectDif):
			files=os.listdir(filePath)
			while(fileName in files):
				dif="-{}{}".format(difbase,difInt)
				difInt+=1
				fileName="{}{}.{}".format(name,dif,ext)
		return filePath,fileName

	def reLoadDict(self,dict):
		self.dictLoaded[dict]=project.dictionarys[dict](self.projectName,self.projectPath)
		print(dict+" loaded")
		self.setDictSecuenceKind(dict)
		return self.dictLoaded[dict]

	def loadDict(self,dict):
		if(not dict in self.dictLoaded):
			if(dict=="GenesDicts"): self.dictLoaded[dict]=project.dictionarys[dict](self.projectName,self.path)
			elif(dict=="EdgesDict"): self.dictLoaded[dict]=project.dictionarys[dict](self.projectName,self.projectPath,self.spices)
			elif(dict=="LocusDicts"): self.dictLoaded[dict]=project.dictionarys[dict](self.projectName,self.projectPath)
			print(dict+" loaded")
		self.setDictSecuenceKind(dict)
		return self.dictLoaded[dict]

	def setDictSecuenceKind(self,dict):
		# Si es GenesDicts, agragr atributo donde el name es de tipo self.identifierKind
		if(dict=="GenesDicts"):
			self.dictLoaded[dict].nameKind=self.identifierKind
			if(self.identifierKind=="genes"):
				self.dictLoaded[dict].label2name=self.dictLoaded[dict].label2gen
				self.dictLoaded[dict].name2label=self.dictLoaded[dict].gen2label
				self.dictLoaded[dict].name2spice=self.dictLoaded[dict].gen2spice
			elif(self.identifierKind=="proteins"):
				self.dictLoaded[dict].label2name=self.dictLoaded[dict].label2protein
				self.dictLoaded[dict].name2label=self.dictLoaded[dict].protein2label
				self.dictLoaded[dict].name2spice=self.dictLoaded[dict].protein2spice
			else:
				print("ERROR: No existe identifierKind="+self.identifierKind)
				print("Tendras errores al usar GenesDicts.label2name y GenesDicts.name2label")
		if(dict=="LocusDicts"):
			if(self.identifierKind=="genes"):
				self.dictLoaded[dict].nameLoci=self.dictLoaded[dict].genLoci
			else:
				self.dictLoaded[dict].nameLoci=self.dictLoaded[dict].proteinLoci

	def printSpiceOrthologos(self,spice,includeAge=True,genesFilter=None,printNoOrthologous=False):
		# Prepara diccionarioss label-gey y archivos de salida
		genesdict=self.loadDict("GenesDicts")
		spices=list(genesdict.spices2labels)
		genes2orth={}
		spices2genes={}
		genesAge={}
		genesGraph={}
		graph2genes={}
		filteredGenes=[]
		# Hacer funciones de filtro de genes
			#Verifica que el gen está en la lista self.genesFilter.
			# si self.genesFilter==None --> siempre pasa el filtro
		if(genesFilter==None): filter=lambda gen: True
		else: filter=lambda gen:gen in genesFilter
		# Definir iterador de especies
		spisIter=[]
		for spice1 in self.spices:
			if(spice!=spice1): spisIter+=[spice1]
		# anotar edad de los genes de spice
		files={spice1:open(self.path+"Summary/"+self.projectName+"."+spice+"_"+spice1+".edgesAge","w") for spice1 in spisIter}
		with open(self.path+"Summary/"+self.projectName+"."+spice+"_orthologous.edgesAge","w") as F:
			F.write("genes_family,age\t"+spice+"\t")
			for spisF in files: files[spisF].write("genes_family,age\t"+spice+"\t"+spisF+"\n")
			for spice1 in spisIter[:-1]: F.write(spice1+"\t")
			F.write(spisIter[-1]+"\n")
			for label in self.spices2genes[spice]:
				age=self.genesAge[label]
				gen=genesdict.label2name[label]
				if(filter(gen)):
					filteredGenes+=[gen]
					# cargar componente conexa para ver aristas
					cc=self.genesCC[label]
					G=nt.Graph.loadAdjL(cc,self.path,weight=False)	
					# obtener ortólogos
					orth={spice1:[] for spice1 in spisIter}
					for label1 in list(G.edge[label]):
						spice1=self.genesSpice[label1]
						gen1=genesdict.label2name[label1]
						orth[spice1]+=[gen1]
					# escribir a archivo
					F.write("{},{}\t{}\t".format(cc,age,gen))
					for spisF in files:
						if(len(orth[spisF])>0):
							files[spisF].write("{},{}\t{}\t".format(cc,age,gen))
							for gen1 in orth[spisF][:-1]: files[spisF].write("{}|".format(gen1))
							files[spisF].write("{}\n".format(orth[spisF][-1]))
					for spice1 in spisIter[:-1]:
						if(len(orth[spice1])>0):
							for gen1 in orth[spice1][:-1]: F.write("{}|".format(gen1))
							F.write("{}\t".format(orth[spice1][-1]))
						else: F.write("-\t")
					spice1=spisIter[-1]
					if(len(orth[spice1])>0):
						for gen1 in orth[spice1][:-1]: F.write("{}|".format(gen1))
						F.write("{}\n".format(orth[spice1][-1]))
					else: F.write("-\n")
			if(printNoOrthologous):
				for gen in genesFilter:
					if(gen not in filteredGenes):
						F.write("-,-\t"+gen+"\t")
						for spice1 in spisIter[:-1]:
							F.write("-\t")
						F.write("-\n")
		for spis in files: files[spis].close()

	def locusComparation(self,spice0,spice1,genesFilter=None,distinction=""):
		locusdicts=self.loadDict("LocusDicts")
		genesdict=self.loadDict("GenesDicts")
		return dr.locusComparation(self.projectName,self.projectPath,\
			spice0,spice1,locusdicts,genesdict,genesFilter=genesFilter,distinction=distinction)

	def proteinortho(self):
		steps=self.proteinorthoSteeps
		kind=self.proteinorthoKind
		# Corre proteinrtho por cada uno de los pasos
		files=getFastasStringList()
		for step in steps:
			prtStep="proteinortho-method-{}".format(step)
			if(self.status.areDependenciesSatisfied(prtStep)):
				executeBashCommand(getProteinorthoCommand(step,kind,files))
				self.status.setNodeStatus(prtStep,True)
		śelf.mvPrtFiles()
		self.mvFastaFiles()

	def mvFastaFiles(self):
		# Mover archivos de blast y cambiar nombre
		for file in os.listdir(self.path+"Tmp/"):
			if(".bla" in file):
				command="mv {0}Tmp/{1} {0}Blast/{2}.{1}".format(\
					self.path,\
					file,\
					self.projectName)
				executeBashCommand(command)

	def mvPrtFiles(self):
		# Mover archivos de proteiortho
		commands=["mv {}.blast-graph {}".format(self.projectName,self.path+"Prt/"),\
			"mv {}.proteinortho {}".format(self.projectName,self.path+"Prt/"),\
			"mv {}.proteinortho-graph {}".format(self.projectName,self.path+"Prt/")]
		for command in commands:
			executeBashCommand(command)

	def getProteinorthoCommand(self,step,kind,files):
		return "{} -project={} -verbose -graph -keep -singles -step={} --p=blast{} -temp={} {}".format(\
				self.proteinorthoPath,\
				self.projectName,\
				steep,\
				kind,\
				self.path+"Tmp/",\
				files)

	def getFastasStringList(self):
		files=""
		for file in os.listdir(self.sequencesPath):
			ext=file.split(".")[-1]
			if(ext in ["fa","fasta"]): files+="{} ".format(self.sequencesPath+file)
		return files[:-1]

	def proteinortho2adjL(self):
		if(not self.status.isNodeSeted("proteinortho-2-adjacencyList")):
			self.checkMethodDependencies("proteinortho-2-adjacencyList")
				# Carga diccionario de genes
				genedicts=self.loadDict("GenesDicts")
				# Crear listas de adjacencia
#				prt2gra=nt.prt2gra(self.projectName,self.path,genedicts,p.identifierKind)
				proteinOrthoGraph="{}.proteinortho-graph".format(self.projectName)
				graph=nt.Graph(proteinOrthoGraph=proteinOrthoGraph,\
					project=self.projectName,projectPath=self.projectPath,\
					secuencesType=self.identifierKind,genesdicts=genedicts)
				# imprimir lista de adjacencia
				fileName="{}.{}.adjL".format(self.projectName,self.secuencesKind)
				filePath="{}Graphs/".format(self.path)
				nt.fprintAdjL(graph.G,fileName,filePath)
				# Guardar status
				self.status.setNodeStatus("proteinortho-2-adjacencyList",True)
				return graph


	def getSingletones(self):
		ls=len(self.spices)+3
		F=open(self.path+"Prt/"+self.projectName+".proteinortho")
		head=F.readline().split("#")[1].split()
		singletones=open(self.path+"Prt/"+self.projectName+".singletones","w")
		for line in F:
			dat=line.split()
			if(dat[1]=="1"):
				genes=[da=="*" for da in dat[3:ls]]
				idx=genes.index(False)
				singletones.write(dat[idx+3]+"\n")

	def getCC(self):
		if(not self.status.isNodeSeted("conexComponents-method")):
			self.checkMethodDependencies("conexComponents-method")
			adjacencyList="{}Graphs/{}.{}.adjL".format(self.path,self.projectName,self.secuencesKind)
			graph=nt.Graph(adjacencyList=adjacencyList,secuencesType=self.secuencesKind,identifierType=self.identifierKind,\
				genesdicts=self.loadDict("GenesDicts"))
			G=graph.G
			CC=nt.conComp(G)
			baseName="{}.{}".format(self.projectName,self.identifierKind)
			nt.fprintCC(CC,G,baseName,self.path+"Graphs/",self.identifierKind) #,self.projectName,self.path,self.identifierKind)
			self.status["CC"]=self.status["adjacencyList"]

	def getEventTrees(self):
		# create genes dicts if ensemble format fasta given and is not created yet
		if(self.fastasFormat!=None): self.setDictionary("GenesDicts")
		self.proteinortho2adjL()
		self.getCC()
		if(not self.status.isNodeSeted("modular-decomposition")):
			self.checkMethodDependencies("modular-decomposition")
			editPrimeModules=self.editNocographs
			# Apica descomposción modular recursivamente
			nt.descomponerRCC(self.projectName,self.projectPath,self.secuencesKind,self.identifierKind,\
				self.loadDict("GenesDicts"),editPrimesModules=editPrimeModules)
			self.status.setNodeStatus("modular-decomposition",True)

	def reconciliate(self,printAges=True):
		if(not self.status.isNodeSeted("tree-reconciliation-method")):
			self.checkMethodDependencies("tree-reconciliation-method")
			self.printAges=printAges
			rmt=evo.reconMT(self.projectName,\
				self.projectPath,\
				self.speciesTree,\
				self.loadDict("GenesDicts"),\
				self.identifierKind,\
				self.secuencesKind,\
				getFileName=self.getFileName,\
				filterType=self.reconFilterType,\
				listFilter=self.reconListFilter,\
				include_edited=self.reconEditedGrahs,\
				filterPreFunction=self.identifier2filterFunction)
			self.genesAge=rmt.genesAge
			self.genesCC=rmt.genesCC
			self.CC2genes=rmt.CC2genes
			self.genesSpice=rmt.genesSpice
			self.spices2genes=rmt.spice2genes
			with open("{}Summary/{}.{}.reconciliationTree".format(self.path,self.projectName,self.identifierKind),"w") as F:
				F.write(rmt.T.root.labelAsSuperNode(weight=True))
			for dif in rmt.treeCopies:
				with open("{}Summary/{}.{}-{}.reconciliationTree".format(self.path,self.projectName,self.identifierKind,dif),"w") as F:
					F.write(rmt.treeCopies[dif].ctree.root.labelAsSuperNode(weight=True))
			if(self.printAges):
				for spice in rmt.spices: rmt.printAges(spice)
				with open(self.path+"Summary/"+self.projectName+".notFound"+"."+self.identifierKind+".edgesAge","w") as F:
					if(len(rmt.listFilter)>0):
						for gen in rmt.listFilter[:-1]:
							if(gen not in rmt.filteredList): F.write(gen+"\n")
						if(rmt.listFilter[-1] not in rmt.filteredList): F.write(rmt.listFilter[-1]+"\n")
			self.rmt=rmt
			return rmt


	def save(self):
		del self.dictLoaded
		self.dictLoaded={}
		with open(self.path+"Summary/"+self.projectName+".bmgmd","wb") as F: pickle.dump(self,F)

	def setFastas(self,sequencesPath,fastasLabels,ignoreDotFiles=False):
		"Verifica que los nombres de los archviso fasta sean válidos"
		# verificar que los directorios estén inicializados
		if(self.status["initialiced"]>0):
			species=os.listdir(sequencesPath)
			if((False in [len(specie.split("."))==1 for specie in species])&(not ignoreDotFiles)):
				print("ERROR: The neame of each fasta file in "+sequencesPath+" must be of length 5 and without dot character (.)")
				print("so each fasta file contains the set of genes of one specie. The file name must be the identifier of that spices\n")
				print("If you want to ignore the files with dot character set ignoreDotFiles=True\n")
#				self.status["sequences"]=False
			else:
				self.sequencesPath=sequencesPath
#				self.status["sequences"]=self.status["initialiced"]
		else:
			print("ERROR: Firsr you need to set the files directoy")
			print("use project.setWorkDir()")

	def setDictionary(self,dictKind):
		if(dictKind=="GenesDicts"):
			if(not self.status.isNodeSeted("geneDicts")):
				if(self.fastasFormat=="ensemble"):
					self.status.checkMethodDependencies("getGeneDicts")
					self.dictLoaded["GenesDicts"]=dct.GeneDicts(self.projectName,self.projectPath,self.sequencesPath,self.fastasLabels)
					self.dictLoaded["GenesDicts"].save()
					self.setDictSecuenceKind("GenesDicts")
					self.spices=list(self.dictLoaded["GenesDicts"].spices2labels)
					self.status.setNodeStatus("getGeneDicts",True)
				else: raise AttributeError("fasta format {} not avalible".format(self.fastasFormat))
		elif(dictKind=="EdgesDict"):
			self.dictLoaded["EdgesDict"]=dct.EdgesDict(self.projectName,self.projectPath,\
				self.dictLoaded["GenesDicts"],self.fastasLabels)
			self.dictLoaded["EdgesDict"].save()
		elif(dictKind=="LocusDicts"):
			self.dictLoaded["LocusDicts"]=dct.LocusDicts(self.GTFsPath,self.projectName,self.projectPath,\
				self.dictLoaded["GenesDicts"],GTFsLabels=self.GTFsLabels)
				self.dictLoaded["LocusDicts"].save()
		else: raise AttributeError("dictionary {} not defined".format(dictKind))

	def setWorkDir(self,projectPath,projectName,overWrite):
		# Crear atrubutos
		self.projectPath=projectPath
		self.projectName=projectName
		self.path=projectPath+projectName+"/"
		# Comprobar que el directorio no contenga otro proyecto
		if(os.path.exists(self.path)):
			if(overWrite):
				clearDirsContent(self.path)
				freeDir=True
			else:
				freeDir=False
		else:
			os.makedirs(self.path)
			freeDir=True
		if(freeDir):
			# Crear directorios para archivos de projectos
			for dir in project.dirs: os.makedirs(self.path+dir)
		else:
			raise filename("ERROR: There is alrredy a project '{}'\n If you don't care at all set overWrite=True\n".format(projectPath+projectName))
