# coding=utf-8
import networkx as nx
import os
from copy import deepcopy
from order import Order

def edgesWeightFunction(Dict):
	if(Dict==None):
		weightFun=lambda nodeC,node: 1
	else:
		weightVer=lambda nodeC,node: (node,nodeC) in Dict
		weightFun=lambda nodeC,node: Dict[(nodeC,node)] if weightVer(nodeC,node) else 10
	return weightFun

def weightedComplement(G,edgesWeight):
	Gc=nx.Graph()
	Gc.add_nodes_from(G.nodes())
	visitados=[]
	nodes=G.nodes()
	# Las aristas se van agregando por nodo:
	for node in nodes:
		visitados+=[node]
		# Calcula los nodos que tendrán arista al nodo: Complemento(  vecinos+nodos visitados )
		possibleNeighbors=set(nodes).difference(list(G[node])+visitados)
		# Revisa las aristas
		for nodeC in possibleNeighbors: Gc.add_edge(node,nodeC,weight=edgesWeight(nodeC,node))
	return Gc

class Graph:

	def __init__(self,G=None,adjacencyList=None,proteinOrthoGraph=None,project=None,projectPath=None,\
			secuencesType=None,identifierType=None,genesdicts=None):
		self.initAtributes(G,adjacencyList,project,projectPath,secuencesType,identifierType,proteinOrthoGraph,genesdicts)

	def initAtributes(self,G,adjacencyList,project,projectPath,secuencesType,identifierType,proteinOrthoGraph,genesdicts):
		self.genesdicts=genesdicts
		if(G==None): self.G=G=nx.Graph()
		else: self.G=G
		if(project==None): self.project=""
		else: self.project=project
		if(projectPath==None): self.projectPath="./"
		else: self.projectPath=projectPath
		self.path=self.projectPath+self.project+"/"
		self.secuencesType=secuencesType
		self.identifierType=identifierType
		self.adjacencyList=adjacencyList
		if(adjacencyList!=None):
			self.add_adjacencyList(adjacencyList) #,self.secuencesType,genesdicts)
		self.proteinOrthoGraph=proteinOrthoGraph
		if(proteinOrthoGraph!=None):
			self.add_proteinOrthoGraph(proteinOrthoGraph,genesdicts)

	def add_adjacencyList(self,filePath): #h ,secuencesType,genesdicts): #,weight=False):
		if(self.identifierType=="genes"):
			if(self.secuencesType=="genes"): edgesss=lambda line:self.getEdgesAdjL(line)
			elif(self.secuencesType=="proteins"):
				edgesss=lambda line:self.getProtein2GenEdgesAdjL(line)
			else: print("erro no secuencesType valid")
		elif(self.identifierType=="proteins"):
			if(self.secuencesType=="genes"): print("error nets.add_adjL cant get genes identifierType from proteins secuencesType")	
			elif(self.secuencesType=="proteins"): edgesss=lambda line:self.getEdgesAdjL(line)
			else: print("nets.Graph.add_adjacencyList error: no secuencesType valid")
		else: print("nets.Graph.add_adjacencyList error: no secuencesType valid")
		with open(filePath) as F:
			for line in F:
				line=line[:-1]
				edges=edgesss(line)
				self.addEdges(edges)

	def add_proteinOrthoGraph(self,proteinOrthoGraph,genesdicts):
		if(self.secuencesType=="genes"): getEdge=lambda line: self.getProteinEdgeProteinortho(line)  #self.getGenEdgeProteinortho(line,genesdicts)
		elif(self.secuencesType=="proteins"): getEdge=lambda line: self.getProteinEdgeProteinortho(line)
		else: print("Error, secuencesType '{}' not soported. Methods not initialiced".format(self.secuencesType))
		file="{}Prt/{}".format(self.path,proteinOrthoGraph)
		with open(file) as F:
			edges=[]
			lines=F.readlines()
			for line,i in zip(lines,range(len(lines))):
				if(line[0]!="#"):
					edges+=[getEdge(line)]
			self.addEdges(edges)

	def getProteinEdgeProteinortho(self,line):
		prota,protb,eval_ab,bits_ab,eval_ba,bits_ba=line.split()
		eval_ab,eval_ba=float(eval_ab),float(eval_ba)
		# Agregar arista con peso= promedio de evalue
		return prota,protb,(eval_ab+eval_ba)/2

	def getGenEdgeProteinortho(self,line,genesdicts):
		prota,protb,eval_ab,bits_ab,eval_ba,bits_ba=line.split()
		gena,genb=genesdicts.protein2gen[prota],genesdicts.protein2gen[protb]
		eval_ab,eval_ba=float(eval_ab),float(eval_ba)
		return gena,genb,(eval_ab+eval_ba)/2

	def addEdges(self,edges):
		for edge in edges:
			self.G.add_edge(edge[0],edge[1],weight=edge[2])

	def getEdgesAdjL(self,line):
		nodea,nodesb=line.split("|")
		nodesb=nodesb.split(",")
		edges=[]
		for nodebw in nodesb:
			nodeb,weight=nodebw.split(":")
			weight=float(weight)
			edges+=[tuple([nodea,nodeb,weight])]
		return edges

	def getProtein2GenEdgesAdjL(self,line):
		nodea,nodesb=line.split("|")
		gena=self.genesdicts.protein2gen[nodea]
		nodesb=nodesb.split(",")
		genesb=[]
		edges=[]
		for nodebw in nodesb:
			nodeb,weight=nodebw.split(":")
			genb=self.genesdicts.protein2gen[nodeb]
			if(genb not in genesb):
				genesb+=[genb]
				weight=float(weight)
				edges+=[tuple([gena,genb,weight])]
		return edges

class prt2gra:

	def __init__(self,project,projectPath,genedicts,secuencesType):
		self.initAtributes(project,projectPath,genedicts,secuencesType)
		self.initMethods(secuencesType)
		G=nx.Graph()
		with open(path+"Prt/"+project+".proteinortho-graph") as F:
			lines=F.readlines()
			for line,i in zip(lines,range(len(lines))):
				if(line[0]!="#"):
					self.addEdge(line)
		self.G=G

	def addProteinEdge(self,line):
		prota,protb,eval_ab,bits_ab,eval_ba,bits_ba=line.split()
		eval_ab,eval_ba=float(ab),float(eval_ba)
		# Agregar arista con peso= promedio de evalue
		G.add_edge(prota,protb,weight=(eval_ab+eval_ba)/2)

	def addGenEdge(self,line):
		gena,genb=self.genesdicts.protein2gen[prota],self.genesdicts.protein2gen[protb]
		print("lol no se ha implementado nets.prt2gra.addGenEdge :v")

	def initAtributes(self,project,path,genedicts,secuencesType):
		self.genesdicts=genesdicts
		self.project=project
		self.projectPath=projectPath
		self.path=projectPath+project+"/"
		self.secuencesType=secuencesType

	def initMethods(self):
		if(self.secuencesType=="genes"): self.addEdge=lambda line: self.addGenEdge(line)
		elif(self.secuencesType=="proteins"): self.addEdge=lambda line: self.addProteinEdge(line)
		else: print("Error, secuencesType {} not soported. Methods not initialiced".format(self.secuencesType))

def netName(G,dif=None,weight=False):	# Genera nombre de red
	name="N"+str(len(G.nodes()))
	if(dif!=None): name+="."+str(dif)
	if(weight): name+=".weighted"
	else: name+=".no_weighted"
	return(name)

def fprintCC(CC,G,baseName,filesPath,secuencesType):	# Imprime CC de G en un directorio name dentro de path
	for i in range(len(CC)):
		fprintAdjL(G.subgraph(CC[i]),"{}-cc{}.adjL".format(baseName,i),filesPath)

class fprintAdjL:
	def __init__(self,graph,fileName,filePath):
		"Imprime lista de adjacencia con formato para descomposició modular con chicken"
		self.G=graph
		with open(filePath+fileName,"w") as F:
			for node in self.G.nodes(): self.printNode(node,F)
#		else:
#			print("error: {} alrredy exists".format(filePath+fileName))

	def printNode(self,node,F):
		F.write(node+"|")
		edges=self.G.edges(node,data=True)
		for edge in edges[:-1]: F.write("{}:{},".format(edge[1],edge[2]["weight"]))
		F.write("{}:{}\n".format(edges[-1][1],edges[-1][2]["weight"]))

def conComp(G):	# Regresa lista de listas
		# Cada sublista contiene los nodos de cada componente cnexa
	return ([list(com) for com in list(nx.connected_components(G))])

def cut(G,minCut=True,peso=lambda e:float(e[2]['weight'])):
	"Retorna cuts:,cutW. cuts es una lista de listas, donde cada sub-lista contiene las aristas que cortan a G, y cuyos pesos suman cutW"
	CC=conComp(G)
	if(len(CC)==1):
		# Inicializar lista de descubiertos
		Nodes=list(G.nodes())
		E=list(G.edges(data=True))
		A=[Nodes[0]]
		# Inicializamos peso de corte y desigualdad
		if(minCut): inequality=lambda w,cutW: w<cutW
		else: inequality=lambda w,cutW: w>cutW
		# Inicializa lista A de descubiertos y de pesos de vecinos por conjuntos de cortes
		cutW,vecinos,edges=computeCutW(G,A,peso=peso)
		cuts=[[(edge[0],edge[1]) for edge in edges]]
		# Continua agregando nodos a A y calculando los cortes
		while(len(A)<len(Nodes)-1):
			A+=[escoge(vecinos,min=not minCut)]
			w,vecinos,edges=computeCutW(G,A,peso=peso)
			if(inequality(w,cutW)):
				cuts=[[(edge[0],edge[1]) for edge in edges]]
				cutW=w
			elif(w==cutW): cuts+=[[(edge[0],edge[1]) for edge in edges]]
		return cuts,cutW

	else:
		return None,None

def computeCutW(G,S,minCut=True,peso=lambda e:float(e[2]['weight'])):
	"Calcula el peso de un corte para separar a los nodos S, regresa peso y nodos externos a S necesarios para hacer un corte S"
	if(minCut): inequality=lambda w,cutW: w<cutW
	else: inequality=lambda w,cutW: w>cutW
	s=0
	vecinos={}
	edges=[]
	# Por cada arista del conjunto S
	for edge in G.edges(S,data=True):
		e2=edge[1]
		if(e2 not in S):
			edges+=[edge]
			s+=peso(edge)
			if(e2 in vecinos): vecinos[e2]+=peso(edge)
			else: vecinos[e2]=peso(edge)
	return s,vecinos,edges

def escoge(vecinos,min=True):
	"Escoje vecino de un diccionario de vecinos y el peso de las aristas para llegar a él"
	if(min): comparation=lambda w1,w2: w1<w2
	else: comparation=lambda w1,w2: w1>w2
	V=list(vecinos)[0]
	M=vecinos[V]
	for v in vecinos:
		if(comparation(vecinos[v],M)):
			V=v
			M=vecinos[v]
	return V

def changeStatus(s):
	return "P" if s=="S" else "S"

def subModules(G,edgesWeight):
	Gc=weightedComplement(G,edgesWeight)
	CC=conComp(Gc)
	return Gc,CC

class modularDecomposition:
	def __init__(self,Go,editPrimesModules=False,edgesWeight=lambda a,b:1):
		# Inicializa ediciones
		self.initEditions(editPrimesModules)
		# Función de peso para las aristas del complemento
		self.edgesWeight=edgesWeight
		# Inicializa módulos
		self.initModules(Go)
		# Inicializa recorrido por los módulos
		moduleID=self.initDFS()
		# Iniciar descomposición
		while(self.DFSCondition()):
			if(moduleID==None): self.returnModule()
			else: self.addNode(moduleID)
			moduleID=self.popModuleSoon()

	def printModules(self):
		print([self.modules[module].nodes() for module in self.modules])

	def initEditions(self,editPrimesModules):
		self.editPrimesModules=editPrimesModules
		self.editions={"added":[],"removed":[]}

	def returnModule(self):
		self.stackModules.pop()
		self.newik="({}".format(self.newik)
		if(not self.isLastPeakModulesSoon()):
			self.newik=",{}".format(self.newik)

	def addLeaf(self,moduleID):
		# Contar la hoja
		self.singleLeavs+=1
		self.newik="{}{}".format(self.modules[moduleID].nodes()[0],self.newik)
		# Sí es el último hijo del módulo en la punta de la pila
		if(not self.isLastPeakModulesSoon()):
			self.newik=",{}".format(self.newik)

	def addNode(self,moduleID):
		if(self.isModuleLeaf(moduleID)):
			self.addLeaf(moduleID)
		else:
			self.pushModule(moduleID)
			label=self.modulesLabel[moduleID]
			if(label=="*"):
				# Agregar a newik
				soons=""
				for soon in self.modules[moduleID].nodes(): soons+="{},".format(soon)
				soons=soons[:-1]
				self.newik="({})*{}".format(soons,self.newik)
				# Sí es el último hijo del módulo en la punta de la pila
				if(not self.isLastPeakModulesSoon()):
					self.newik=",{}".format(self.newik)
				self.stackModules.pop()
			else:
				# agregar a newik
				self.newik="){}{}".format(label,self.newik)

	def isModuleLeaf(self,moduleID):
		return len(self.modules[moduleID].nodes())==1

	def isLastPeakModulesSoon(self):
		return self.queueSoons[self.stackModules.next()].size()==0

	def popModuleSoon(self):
		id=self.queueSoons[self.stackModules.next()].pop()
		if(id==None):
			del self.queueSoons[self.stackModules.next()]
		else:
			if(id not in self.visitedModules):
				self.addModuleSoons(id)
				self.visitedModules+=[id]
		return id

	def pushModule(self,moduleID):
		self.stackModules.push(moduleID)
		self.queueSoons[moduleID]=Order(kind="queue")
		self.queueSoons[moduleID].pushList(self.modulesSoons[moduleID])

	def addModuleSoons(self,moduleID):
		self.modulesSoons[moduleID]=[]
		if(not self.isModuleLeaf(moduleID)):
			Gc,CC=subModules(self.modules[moduleID],self.edgesWeight)
			label=changeStatus(self.modulesLabel[moduleID])
			if(len(CC)==1):
				if(self.editPrimesModules):
					Gc,CC=self.cutGraphModule(Gc,label)
					self.defineModulesFromCC(Gc,CC,label,moduleID)
				else: self.modulesLabel[moduleID]="*"
			else:
				self.defineModulesFromCC(Gc,CC,label,moduleID)

	def defineModulesFromCC(self,Gc,CC,label,moduleFather):
		for cc in CC:
			# Agregar módulo hijo e infoormación
			self.modules[self.LenModules]=Gc.subgraph(cc)
			self.modulesLabel[self.LenModules]=label
			self.modulesFather[self.LenModules]=moduleFather
			self.modulesSoons[moduleFather]+=[self.LenModules]
			# Actualizar número de módulos
			self.LenModules+=1

	def cutGraphModule(self,Gc,label):
		if(len(Gc.nodes())>1):
			# Si el módulo es serie --> se quitan aristas,
			# como el peso de la arista indica disimilaridad --> quitar las mas pesadas --> usar maxCut
			# Validar corte: Si cutw==0 --> usar función de peso=1
			if(label=="S"):
				cuts,cutw=cut(Gc,minCut=False)
				if(cutw==0): cuts,cutw=cut(Gc,minCut=False,peso=lambda e:1)
				edition="removed"
			else:
				cuts,cutw=cut(Gc,minCut=True)
				if(cutw==0): cuts,cutw=cut(Gc,minCut=True,peso=lambda e:1)
				edition="added"
			Gc.remove_edges_from(cuts[0])
			self.editions[edition]+=[cuts[0]]
			return Gc,conComp(Gc)
		else: return None,None


	def initModules(self,Go):
		self.modules={0:deepcopy(Go)}
		self.modulesLabel={0:"S"}
		self.modulesFather={0:None}
		self.modulesSoons={}
		self.LenModules=1
		self.addModuleSoons(0)
		self.newik=";"

	def initDFS(self):
		# Inicializar pilas
		self.stackModules=Order(kind="stack")
		self.queueSoons={None:Order()}
		# Definir número de hojas totales
		self.N=len(self.modules[0].nodes())
		# Inicializar número de hojas
		self.singleLeavs=0
		# Lista de módulos visitados
		self.visitedModules=[0]
		return 0

	def DFSCondition(self):
		# Condición para continuar con la descomposición
		return (self.stackModules.size()>0)|(self.newik==";")

def graphsList(project,projectPath,weighted=False):
	# Si es pesao o no
	if(weighted): wFilter=lambda item:".Weighted." in intem
	else: wFilter=lambda item:".noWeighted." in item
	# Definir path
	path=projectPath+project+"/"
	# Crear cola de grafos a procesar
	Q=[]
	for file in os.listdir(path+"Graphs/"):
		# Sí el archivo es componente conexa del proyecto y cumple condició de peso
		if(project+"_" in file and wFilter(file)): Q+=[file.split(".")[0]]
	return Q

class descomponerRCC:
	def __init__(self,project,projectPath,secuencesType,identifierType,genesdict,\
			edgesWeight=lambda a,b:1,editPrimesModules=False):
		self.project=project
		self.projectPath=projectPath
		self.path=projectPath+project+"/"
		self.editPrimesModules=editPrimesModules
		self.secuencesType=secuencesType
		self.identifierType=identifierType
		self.genesdict=genesdict
		self.edgesWeight=edgesWeight
		# Crea cola de archivos a descomponer y borra ediciones anteriore
		self.initQueue()
		# Abre archivos de resumen
		self.initFiles()
		self.go(edit=False)
		if(editPrimesModules):
			self.Q=self.toEdit
			self.go(edit=True)
		# Cierra archivos
		self.closeFiles()

	def go(self,edit=True):
		# Coemnzar con descomposición modular
		l=len(self.Q)
		action="editing" if edit else "decomposing"
		progress=lambda Q: (l-len(self.Q))/l
		I=0
		while(len(self.Q)>0):
			# Obtener siguiente grafo en la cola
			graph=self.nextInQ()
			adjacencyList="{}Graphs/{}.{}-{}.adjL".format(self.path,self.project,self.identifierType,graph)
			prog=progress(self.Q)
			if(prog>=I):
				print("{}\t{}".format(action,prog))
				I+=0.1
			# Aplica descomposición modular, primo=True si no se pudo descomponer
			Go=Graph(adjacencyList=adjacencyList,secuencesType=self.secuencesType,identifierType=self.secuencesType,\
				genesdicts=self.genesdict).G
			md=modularDecomposition(Go,editPrimesModules=edit,edgesWeight=self.edgesWeight)
			# Guardar árbol y ediciones
			self.saveDescomposition(md,graph)

	def saveDescomposition(self,md,graph):
		if("*" in md.newik):
			self.toEdit+=[graph]
		else:
			filePath="{}Trees/{}.{}-{}.adjL".format(self.path,self.project,self.identifierType,graph)
			with open(filePath,"w") as F:
				F.write(md.newik)
			if(self.editPrimesModules):
				if(md.editPrimesModules):
					self.Feditions.write("# "+graph+"\n")
					for kind in md.editions:
						self.Feditions.write("> "+kind+"\n")
						for edition in md.editions[kind]:
							self.Feditions.write(str(edition)+"\n")

	def initQueue(self):
		filterb=lambda file: self.project in file and "-cc" in file and ".adjL" in file
		if(self.secuencesType=="genes"):
			if(self.identifierType=="genes"): filter=lambda file: filterb(file) and "genes" in file
			elif(self.identifierType=="proteins"): print("ERROR can't have proteins identifierType if you hace genes secuencesType")
		elif(self.secuencesType=="proteins"):
			if(self.identifierType=="genes"): filter=lambda file: filterb(file) and "genes" in file
			elif(self.identifierType=="proteins"): filter=lambda file: filterb(file) and "proteins" in file
		else: print("reconMT initAtributes error: identifierkind '{}' not valid. label2identifiers not deined".format(secuencesType))	
		# Crear cola de grafos a procesar
		self.toEdit=[]
		self.Q=[]
		filesPath=self.path+"Graphs/"
		for file in os.listdir(filesPath):
			if(filter(file)):
#				self.Q+=[filesPath+file]
				self.Q+=[file.split("-")[1].split(".")[0]]

	def initFiles(self):
#		self.Ftrees=open(self.path+"Trees/"+self.project+".modular_decomposition","w")
		if(self.editPrimesModules): self.Feditions=open(self.path+"Summary/"+self.project+".editions","w")

	def closeFiles(self):
#		self.Ftrees.close()
		if(self.editPrimesModules): self.Feditions.close()

	def nextInQ(self):
		"Retorna el grafo y qué componente conexa es"
		graph=self.Q[0]
		del self.Q[0]
		return graph
