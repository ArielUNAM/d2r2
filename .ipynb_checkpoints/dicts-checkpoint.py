import os
import subprocess
import pickle
import numpy as np

class LocusDicts:
	def __init__(self,GTFsPath,project,projectPath,genesdict,GTFsLabels={},nameKind="gene_id"):
		i=0
		self.noTraducibles=[]
		self.genesdict=genesdict
		self.nameKind=nameKind
		self.label2file={}
		self.file2label={}
		self.genLoci={}
		self.proteinLoci={}
		self.project=project
		self.projectPath=projectPath
		self.path=projectPath+project+"/"
		for file in os.listdir(GTFsPath):
			if(file[-4:]==".gtf"):
				# Obtener label de la especie del archivo
				if(file in GTFsLabels):
					label=GTFsLabels[file]
					self.label2file[label]=file
					self.file2label[file]=label
				else:
					label="spice{}".format(i)
					i+=1
					self.label2file[label]=file
					self.file2label[file]=label
				self.genLoci[label]={}
				self.proteinLoci[label]={}
				# Leer GTF
				with open(GTFsPath+file) as F:
					for line in F:
						if(line[0]!="#"):
							# Obtener loci del gen
							lineSplit=line.split("\t")
							chr=lineSplit[0]
							type=lineSplit[2]
							begin=lineSplit[3]
							end=lineSplit[4]
							if(type=="gene"):
								atributes=lineSplit[8].split(";")
								for atribute in atributes:
									atributeSplit=atribute.split('"')
									if(self.nameKind in atributeSplit[0]):
										gen=atributeSplit[1]
										# Escribir loci del gen
										self.genLoci[label][gen]=(chr,int(begin),int(end))
										# Escribir loci cn prtoteína, si se puede traducir
										if(gen in self.genesdict.gen2label):
											self.proteinLoci[label][\
											self.genesdict.label2protein[
											self.genesdict.gen2label[gen]]]=self.genLoci[label][gen]
										else: self.noTraducibles+=[gen]
		self.sortDicts()

	def sortDicts(self):
		self.sortedGenes={}
		for label in self.genLoci:
			self.sortedGenes[label]={}
			genes,begins=self.getMinLocis(label)
			for chr in begins:
				order=np.argsort(begins[chr])
				self.sortedGenes[label][chr]=[genes[chr][i] for i in order]

	def getMinLocis(self,label):
		begins={}
		genes=list(self.genLoci[label])
		genesChr={}
		for gen in genes:
			chr,begin,end=self.genLoci[label][gen]
			if(chr not in begins):
				begins[chr]=[]
				genesChr[chr]=[]
			begins[chr]+=[min(begin,end)]
			genesChr[chr]+=[gen]
		return genesChr,begins


	def save(self):
		with open(self.path+"Locus/"+self.project+".LocusDicts","wb") as F: pickle.dump(self,F)

	def load(project,path):
		with open(path+project+"Locus/"+"Summary/"+project+".LocusDicts","rb") as F: pro=pickle.load(F)
		return pro

class GeneDicts:
	def __init__(self,project,projectPath,sequencesPath,fastasLabels):
		self.project=project
		self.projectPath=projectPath
		self.sequencesPath=sequencesPath
		self.path=projectPath+project+"/"
		self.fastasLabels=fastasLabels
		self.label2file={}
		self.file2label={}
		# Crea archivos de diccionarios
		self.makeDicts()
		# Carga los diccionarios
		self.loadDicts()

	def save(self):
		with open(self.path+"Identifiers/"+self.project+".genesDict","wb") as F: pickle.dump(self,F)

	def load(project,path):
	#	with open(path+"Summary/"+project+".genesDict","rb") as F: pro=pickle.load(F)
		with open(path+"Identifiers/"+project+".genesDict","rb") as F: pro=pickle.load(F)
		return pro

	def loadDicts(self):
		self.gen2label={} 
		self.label2gen={} 
		self.protein2label={}
		self.label2protein={} 
		self.spices2labels={} 
		self.labels2spices={} 
		self.gen2proteins={}
		self.protein2gen={}
		self.spice2genes={}
		self.gen2spice={}
		self.protein2spice={}
		path=self.path+"Dictionarys/"
		for file in os.listdir(path):
			prj,especie=file.split(".")
			if(prj==self.project):
				self.spices2labels[especie]=[] 
				self.spice2genes[especie]=[]
				with open(path+file) as F:
					i=0 
					for line in F:
						# Obtiene nombres de las secuencias
						protein,gen=line.split()
						if(gen in self.gen2proteins):
							self.gen2proteins[gen]+=[protein]
						else:
							self.gen2proteins[gen]=[protein]
							# Asigna label
							label=especie+"{:011d}".format(i) 
							# Guarda relaciones gen-label
							self.gen2label[gen]=label 
							self.label2gen[label]=gen 
							i+=1 

						self.protein2gen[protein]=gen
						# Guardar relaciones proteinas-label
						self.protein2label[protein]=label 
						self.label2protein[label]=protein 
						# guardar relaciones especie-label_genes
						self.spices2labels[especie]+=[label] 
						self.labels2spices[label]=especie 
						self.spice2genes[especie]+=[gen]
						self.gen2spice[gen]=especie
						self.protein2spice[protein]=especie

	def makeDicts(self):
		i=0
		for file in os.listdir(self.sequencesPath):
			ext=file.split(".")[-1]
			if(ext in ["fa","fasta"]):
				if(file in self.fastasLabels): label=self.fastasLabels[file]
				else:
					label="spice{}".format(i)
					i+=1
				self.label2file[label]=file
				self.file2label[file]=label
				ppath="/".join(os.path.abspath(__file__).split('/')[:-1])+"/"
				command="{}makeDictionary.sh {} {} {} {} {}".format(\
					ppath,\
					self.project,\
					self.path,\
					self.sequencesPath,\
					file,\
					label)
				subprocess.call(command,shell=True)

# Clase para ver todas las aristas del proyecto
class EdgesDict:
	def __init__(self,project,path,genesdict,file2label):
		# Inicializa información de directorios
		self.project=project
		self.projectPath=path
		self.path=path+project+"/"
		# Inicliliza diccionarios
		self.initDicts(file2label)
		# Revisar todos los archivos del directorio Blast
		for file in os.listdir(self.path+"Blast/"):
			# Revisar si es un blast del proyecto
			if((project+"." in file)&(".bla" in file)):
				self.addDict(file,genesdict)

	def addSpi2edgesDict(self,L):
		for si in L:
			if(si not in self.spices):
				self.spices2edges[si]={}
				self.edgesWeight[si]={}
				for spice in self.spices:
					self.spices2edges[spice][si]=[]
					self.spices2edges[si][spice]=[]
					self.edgesWeight[spice][si]={}
					self.edgesWeight[si][spice]={}
				self.spices+=[si]

	def addEdge(self,edge,evalue,s1,s2):
		# write weigth
		if(edge in self.edgesWeight[s1][s2]):
			self.edgesWeight[s1][s2][edge]+=float(evalue)/2
			self.edgesWeight[s2][s1][edge[::-1]]+=self.edgesWeight[s1][s2][edge]
		else:
			self.edgesWeight[s1][s2][edge]=float(evalue)/2
			self.edgesWeight[s2][s1][edge[::-1]]=self.edgesWeight[s1][s2][edge]
		# write spices involved
		self.spices2edges[s1][s2]+=[edge]
		self.spices2edges[s2][s1]+=[edge[::-1]]

	def addDict(self,file,genesdict):
		# Agrega información de "file" al diccionario "self". "genesdict" es diccio de genes
		# Obtiene la información del nombre del archivo
		project=file.split(".")[0]
		filea,fileb=file.split(".vs.")
		s1=self.file2label[filea[len(project)+1:]]
		s2=self.file2label[fileb[:-4]]
		# Agregar especies al diccionarios
		self.addSpi2edgesDict([s1,s2])
		# Leer archivo
		with open(self.path+"Blast/"+file) as F:
			# Lee todas las lineas
			for line in F:
				# Obtiene la arista
				qseqid,sseqid,pident,length,mismatch,gapopen,qstart,qend,sstart,send,evalue,bitscore=line.split()
				edge=(sseqid,qseqid)
#				edge=(genesdict.name2label[sseqid],genesdict.name2label[qseqid])
				# Agregar arista
				self.addEdge(edge,evalue,s1,s2)

	def initDicts(self,file2label):
		self.file2label=file2label
		self.edgesWeight={}
		self.spices2edges={}
		self.spices=[]

	def save(self):
		# Quitar diccionarios de la estructura de dato
		edgesWeight=self.edgesWeight
		spices2edges=self.spices2edges
		del self.edgesWeight
		del self.spices2edges
		# Guardar estructura y diccionariis
		with open(self.path+"Edges/"+self.project+".edgesDict","wb") as F: pickle.dump(self,F)
		# Guarda aristas por pares de especies
		nSpices=len(self.spices)
		for s1,i in zip(self.spices,range(nSpices)):
			for s2 in self.spices[i+1:]:
				with open(self.path+"Edges/"+self.project+\
					".edgesDict_"+s1+"_"+s2+".genes","wb") as F:\
					pickle.dump(spices2edges[s1][s2],F)		
				with open(self.path+"Edges/"+self.project+\
					".edgesDict_"+s1+"_"+s2+".weight","wb") as F:\
					 pickle.dump(edgesWeight[s1][s2],F)		

	def load(project,path,spices):
		with open(path+project+"/"+"Edges/"+project+".edgesDict","rb") as F: pro=pickle.load(F)
		pro.edgesWeight={}
		pro.spices2edges={}
		nSpices=len(spices)
		# Inicializar diccionarios de aristas
		for s1,i in zip(spices,range(nSpices)):
			if(s1 not in pro.edgesWeight):
				pro.edgesWeight[s1]={}
				pro.spices2edges[s1]={}
			for s2 in spices[i+1:]:
				print(s1+" "+s2)	
				spicesStr=EdgesDict.edgesDictFileoNamefSpices(s1,s2,path,project)
				with open(path+project+"/"+"Edges/"+project+".edgesDict_"+spicesStr+".genes","rb") as F:
					pro.spices2edges[s1][s2]=pickle.load(F)
				with open(path+project+"/"+"Edges/"+project+".edgesDict_"+spicesStr+".weight","rb") as F:
					pro.edgesWeight[s1][s2]=pickle.load(F)
				if(s2 in pro.edgesWeight):
					pro.edgesWeight[s2][s1]=pro.edgesWeight[s1][s2]
					pro.spices2edges[s2][s1]=pro.spices2edges[s1][s2]
				else:
					pro.edgesWeight[s2]={s1:pro.edgesWeight[s1][s2]}
					pro.spices2edges[s2]={s1:pro.spices2edges[s1][s2]}
		""" 
		# Rellenar diccionarios
		for s1,i in zip(spices,range(nSpices)):
			for s2 in spices[i+1:]:
				for edge in pro.edgesWeight[s1][s2]:
					edge_1=edge[::-1]
					pro.edgesWeight[s2][s1][edge_1]=pro.edgesWeight[s1][s2][edge]
					pro.spices2edges[s2][s1]+=[edge_1]
		"""
		return pro

	def edgesDictFileoNamefSpices(s,s1,path,project):
		filesPath=path+project+"/"+"Summary/"
		fileprename=project+".edgesDict_"
		spices=s+"_"+s1
		spices1=s1+"_"+s
		name=fileprename+spices+".genes"
		name1=fileprename+spices1+".genes"
		files=os.listdir(filesPath)
		if(name in files): return spices
		if(name1 in files): return spices1

def loadEnsembleGenTranscriptDict(secuencesPath):
	"Crea diccionarios gen-trasncript de archivos fasta de ensemble. Solo se toman en cuenta archivos cullo nombre no tiene '.'"
	gen2transcripts={}
	transcript2gen={}
	for file in os.listdir(secuencesPath):
		if(len(file.split("."))==1):
			with open(secuencesPath+file) as F:
				for line in F:
					if(">" in line):
						ls=line[1:].split()
						tr=ls[0].split(".")[0]
						gen=ls[3][5:].split(".")[0]
						if(gen in gen2transcripts): gen2transcripts[gen]+=[tr]
						else: gen2transcripts[gen]=[tr]
						transcript2gen[tr]=gen
	return gen2transcripts,transcript2gen
