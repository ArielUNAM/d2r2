# coding=utf-8
import bmgmd as pr

# Define the atributes of your project:

# (required)

# Name of the project and path where it's folder will be created:
projectName="example"
projectPath="/u/scratch/antonio/Ensemble_proteinas/"

# (optional)

# Secuences information:
# FASTA files path, kind of sequences, desired identifier format
# These files will be passed to proteinortho and BLAST
# to get an aproximation of the orthology graph
secuencesPath="/u/scratch/antonio/FASTA_test/"
secuencesKind="proteins"
identifierKind="genes"

# You can set a label for the spice of each FASTA file in a dict
# { fasta_file_name : spice_label }
# if labels don't provided, the labels will be asigned atumatically
# With the function bmgmd.getFilesLabels you can create this dict from a
# tsv file with 2 columns: 1.- fasta file name 2.- spice label
fastasLabels=pr.getFilesLabels("/u/scratch/antonio/fasta2label")

# Evolutionary information:
# Spices tree (newick format)
# The leavs in the newick must correspond with the spices labels
speciesTree="/u/scratch/antonio/Species_trees/15s.speciesTree.awk"

# Spatial information:
# GFT files path, labels for GTFs files
GTFsPath="/u/scratch/antonio/GTF/"
# In the same way you passed the spices label of fastas files
# you can provide a GTFs label dict using the same function
# but diferent file
GTFsLabels=pr.getFilesLabels("/u/scratch/antonio/gtf2label")

# Needed programs:
# The command needed to run proteinortho in your system
proteinorthoPath="proteinortho5.pl"


""" 
# Create a new project
p=pr.project(\
	projectName,\
	projectPath,\
	sequencesPath=secuencesPath,\
	fastasLabels=fastasLabels,\
	speciesTree=speciesTree,\
	proteinorthoPath=proteinorthoPath,\
	GTFsPath=GTFsPath,\
	GTFsLabels=GTFsLabels,\
	secuencesKind=secuencesKind,\
	identifierKind=identifierKind,\
	overWrite=True)
# Analize your data
# Once you have specified your data and parameters
# you can analyce your data wirt bmgmd.analyze
# You can specify a list of wich steeps you want to run.
# If you want to make the complete analysis you can put void list
# or simple dont use the parameter "steeps"
# To see what does each steep you can see bmgmd.project.steeps
p.analyze(steeps=[])
"""


# The project is saved automatically after each steep
# if you want to load it you just need to use bmgmd.load
p=pr.load(projectName,projectPath)

# You can change the identifiertype and re-run the analysis
p.setIdentifierKind("genes")
p.analyze(steeps=[5,6,7])
