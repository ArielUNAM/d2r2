class Order:
	"Case de Pilas y colas que se usan para recorrer los árboles"
	def __init__(self,kind="stack",scoreFunction=lambda item:0,scoreCriterium=lambda items,itemsScore:list(items)[0]):
		# L = Lista (puedeser pila o cola)
		self.L=[]
		self.kind=kind
		# La estructura de dato es la lista L
		# self.out es el elemento por el cual se sacarán los lementos
		# "apuntador al siguiente elemento"
		self.initOutFunction(kind,scoreFunction,scoreCriterium)

	def initOutFunction(self,kind,scoreFunction,scoreCriterium):
		# if kind is scoreSelection:
		# 	scoreFunction is a fungctions that given a n item, returns it's score
		# 	scoreCriterium is a function that gieevn a list of items and a dctionary of the items weight
		#		, returns the item that match some criterium
		#		items is the dictionary of the sxore of each item: { item : item_score }
		#	default: all items have score 0, the selected item is the firs of the list
		#		i.e. the thefaul score selection is a queue
		if(kind=="stack"):
			self.out=lambda order:-1
			self.push=self.basicPush
			self.pop=self.basicPop
			self.next=self.basicNext
		elif(kind=="queue"):
			self.out=lambda order:0
			self.push=self.basicPush
			self.pop=self.basicPop
			self.next=self.basicNext
		elif(kind=="scoreSelection"):
			self.itemsScore={}
			self.scoreFunction=scoreFunction
			self.scoreCriterium=scoreCriterium
			self.out=lambda order: order.scoreCriterium(order.L,order.itemsScore)
			self.push=self.pushScore
			self.pop=self.popScore
			self.next=self.nextScore
		else:
			print("ERROR! No existe tipo "+kind)

	def basicPush(self,A):
		self.L+=[A]

	def pushScore(self,A):
		self.basicPush(A)
		self.itemsScore[A]=self.scoreFunction(A)

	def basicPop(self):
		if(len(self.L)>0): return self.L.pop(self.out(self))

	def popScore(self):
		if(len(self.L)>0):
			item=self.out(self)
			self.quitFromItems(item)
			return item

	def quitFromItems(self,item):
		if(item in self.L):
			idx=self.L.index(item)
			del self.L[idx]

	def size(self): return len(self.L)

	def basicNext(self): return self.L[self.out(self)] if len(self.L)>0 else None

	def nextScore(self):
		if(len(self.L)>0):
			item=self.out(self)
			idx=self.L.index(item)
			return item

	def pushList(self,L):
		self.L+=L

	def isVoid(self):
		return len(self.L)==0

class Tour:
	# travel tree by BFS or DFS
	def __init__(self,root,kind="BFS",DFSOrder="pre",condition=lambda node: True,actions=[lambda node: None],\
			elseActions=[lambda n0: None],restriction=lambda node: False,\
			quitSoonFromItemsCondition=lambda node: False):
		self.initAtributes(root,kind,DFSOrder,condition,actions,elseActions,restriction,quitSoonFromItemsCondition)

	def initAtributes(self,root,kind,DFSOrder,condition,actions,elseActions,restriction,quitSoonFromItemsCondition):
		self.condition=condition
		self.actions=actions
		self.elseActions=elseActions
		self.restriction=restriction
		self.breackConditions=[] #breackConditions
		self.quitSoonFromItemsCondition=quitSoonFromItemsCondition
		if(root!=None):
			self.treeRoot=root.tree.root
			if(kind=="BFS"):
				self.O=Order(kind="queue")
				self.O.push(root)
				self.pushNodes=self.basicPush
				self.steep=self.preOrderSteep
			elif(kind=="DFS"):
				self.O=Order(kind="stack")
				self.O.push(root)
				self.pushNodes=self.basicPush
				if(DFSOrder=="pre"): self.steep=self.preOrderSteep
				elif(DFSOrder=="pos"): self.steep=self.posOrderSteep
				else: print("ERROR: order.Tour.initAtributes: DFSOrder={} not defined".format(DFSOrder))
				self.redyForFunctions=set(self.treeRoot.tree.leavs)
#				print(self.redyForFunctions)	
			elif(kind=="bottomUp"):
				self.O=Order(kind="scoreSelection",\
					scoreFunction=lambda node: len(node.path),\
					scoreCriterium=lambda nodesList,nodesScore: self.scoreCriterium(nodesList,nodesScore))

				for node in root.tree.leavs: self.O.push(node)
				self.pushNodes=self.scorePush
				self.steep=self.preOrderSteep
		else:
			self.breackConditions+=[True]


	def go(self):
		while(not self.breackContition()):
			self.steep()

	def breackContition(self):
		if(True in self.breackConditions): return True
		else: return self.O.isVoid()

	def quitSoonFromItems(self,node):
		# Quita a los hijos del nodo actual de la lista de order
		for soon in node.soons: self.O.quitFromItems(soon)

	def nodeProcecing(self,node):
		if(not self.restriction(node)):
			self.pushNodes(node)
			if(self.condition(node)):
				for action in self.actions: action(node)
			else:
				for action in self.elseActions: action(node)
		return node

	def nodePosProcecing(self,node):
		if(not self.restriction(node)):
			if(self.condition(node)):
				for action in self.actions: action(node)
			else:
				for action in self.elseActions: action(node)
		return node

	def posOrderSteep(self):
		node=self.O.next()
#		print(len(self.O.L))	
#		print(node)		
#		print(self.redyForFunctions)	
		if(node in self.redyForFunctions):
#			print("")	
#			print("in redyForFunctions")	
#			print([node.label for node in self.O.L])			
#			print(node)			
			self.O.pop()
			self.redyForFunctions.remove(node)
			self.redyForFunctions.add(node.dad)
			return self.nodePosProcecing(node)
		else:
#			print("not in redyForFunctions")	
			self.pushNodes(node)

	def preOrderSteep(self):
		node=self.O.pop()
		if(self.quitSoonFromItemsCondition(node)): self.quitSoonFromItems(node)
		else:
			return self.nodeProcecing(node)

	def scoreCriterium(self,nodesList,nodesScore):
		maxScore=nodesScore[nodesList[0]]
		maxNode=nodesList[0]
		for node in nodesList[1:]:
			if(nodesScore[node]>maxScore):
				maxScore=nodesScore[node]
				maxNode=node
		return maxNode.dad

	def basicPush(self,node):
		for soon in node.soons: self.O.push(soon)

	def scorePush(self,node):
		if(node==self.treeRoot):
			for soon in node.soons:
				self.O.quitFromItems(soon)
		else:
			# agregar nodo a las hojas
			self.O.push(node)
			for soon in node.soons:
				self.O.quitFromItems(soon)
