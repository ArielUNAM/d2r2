# coding=utf-8

import nets as nt
import networkx as nx
import tree as tr


# Max Cut
G=nx.Graph()
G.add_weighted_edges_from([(1,2,0),(2,3,0),(3,1,0),(1,4,0)])
cuts,cutW=nt.cut(G,minCut=False)


# Congruencia
path="../Trees/"
tset="N142444"
T=tr.Tree(open(path+tset+"_r/species.awk").readline().split(';')[0],weigth='TRUE')
t=tr.Tree("((HUMAN0001,MOUSE000001)P,CHICK00000001)S")
print(tr.areCongruent(t,T))
