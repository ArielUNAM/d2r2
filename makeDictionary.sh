# Funciona para archivos con formato ensemble

# Crear diccionario de identificadores que crearémos:
#	- Los nuevos identificadores son una tupla: (int,specie)
#	- Los diccionarios se guardan en <projectPath>/Dictionaris
#		- Hay un archivo por especie
#		- Las especies son los nombres de los archivos en path
#	- Cada diccionario es una lista de los identificadores de los fastas en path
#		- El int que se le asigna en los nuevos identificadores es el número de linea-1 en el archivo diccionario

# Parámetros de entrdada
project=$1
projectPath=$2
sequencesPath=$3
file=$4
specie=$5

# Programa
#grep ">" $(echo $sequencesPath$specie) | cut -d" " -f1 | cut -d">" -f2 > genesDict.tem
grep ">" $(echo $sequencesPath$file) | cut -d" " -f1 | cut -d">" -f2 > genesDict.tem
#grep ">" $(echo $sequencesPath$specie) | cut -d" " -f4 | cut -d":" -f2 > proteinsDict.tem
grep ">" $(echo $sequencesPath$file) | cut -d" " -f4 | cut -d":" -f2 > proteinsDict.tem
paste genesDict.tem proteinsDict.tem > $projectPath"Dictionarys/"$project"."$specie
rm proteinsDict.tem genesDict.tem
ensembl