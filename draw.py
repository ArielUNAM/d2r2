# coding=utf-8

import pickle
import nets as nt
import numpy

class locusComparation:
	def __init__(self,projectName,projectPath,spice0,spice1,locusdicts,genesdict,\
			genesFilter=None,distinction='',printNameOf="genes"):
		self.printNameOf=printNameOf
		self.projectName=projectName
		self.projectPath=projectPath
		self.path=projectPath+projectName+"/"
		self.distinction=distinction if distinction=="" else "."+distinction
		self.Allspices=[spice0,spice1]
		self.nSpices=len(self.Allspices)
		self.locusdicts=locusdicts
		self.genesdict=genesdict
		self.genesFilter=genesFilter
		if(len(self.genesFilter)==0): print("ATENCION: len(genesFiler=0)")
		self.initDicts()
		self.initGenesFilter()
		self.analize()

	def analize(self):
		self.searchOrtologous()
		self.addIntermediateGenes(self.genesSpice1,self.Allspices[1])
		self.addExternGenes(self.genesSpice1,self.Allspices[1])
		self.delTrash()

	def addExternGenes(self,genesList,spice):
		# agrega genes que rodean a genesList (cromosoma fijo)
		#	por una ditancia máxima de mu+sd
		#	donde mu es la distancia promedio entre genes
		#	y sd=var**(1/2) es la desviación standart
		# perparar métricas
		locusDict=self.locusdicts.nameLoci[spice]
		mu,var=self.getDistStatistics(genesList,locusDict)
#		print("----------------------------")		
#		print(mu,var)					
		sd=var**(1/2)
		order=self.sortGenesList(genesList)
		firstGen=genesList[order[0]]
		lastGen=genesList[order[-1]]
		_,chr,_,_=self.genesInf[lastGen]
		maxD=mu+sd
		sorted=self.locusdicts.sortedGenes[spice][chr]
		l=len(sorted)
		# Inicializar index de inicio, función para avanzar, funciónes de distancia, condiciones de index y función de concatenar
		initI=[sorted.index(lastGen),sorted.index(firstGen)]
		dI=[1,-1]
		distFunctions=[lambda gen,gen1:self.maribelDistance(gen,gen1,locusDict),lambda gen,gen1:self.maribelDistance(gen1,gen,locusDict)]
		indexConditions=[lambda i:i<l,lambda i:i>=0]
		concatenateFunctions=[lambda a,b:a+b,lambda a,b:b+a]
		# Agregar extremos
		for i0,di,checkDist,idCond,concF in zip(initI,dI,distFunctions,indexConditions,concatenateFunctions):
			# Inicializar lista de genes a agregar
			added=[]
			i=i0
			dif=checkDist(sorted[i],sorted[i+di])
#			print(dif)					
			while((dif<maxD)&(idCond(i))):
				added+=[sorted[i+di]]
				i+=di
				dif=checkDist(sorted[i],sorted[i+di])
#				print(dif)				
#			print("....")
#			print(added)					
#			print("")					
			for gen in added:
				self.added+=[gen]
				# Agregar punto a lista de puntos
				chr,begin,end=locusDict[gen]
				dot=(gen,spice, chr, begin, end)
				# agregar información del gen
				self.addGenSpice1(dot)

	def maribelDistance(self,gen,gen1,locusDict):
		_,begin,end=locusDict[gen]
		_,begin1,end1=locusDict[gen1]
		if(begin1<=end): return 0
		else: return begin1-end

	def getDistStatistics(self,genesList,locusDict):
		# Obtener la distribución de distancias entre genes
		if(len(genesList)>1):
			order=self.sortGenesList(genesList)
			dist=[self.maribelDistance(genesList[i],genesList[j],locusDict) for i,j in zip(order[:-1],order[1:])]
			l=len(dist)
			mu=sum(dist)/l
			var=sum([x-mu for x in dist])/l
			return mu,var
		else: return 0,0


	def searchOrtologous(self):
		for graph in nt.graphsList(self.projectName,self.projectPath):
			G=nt.loadAdjL(graph,self.path,weight=False)
			for edge in G.edges():
				# obtiene labels, genes y especies
				genes,spices,chr,begin,end,chr1,begin1,end1=self.getEdgeInf(edge)
				if(self.flag):
					# Actualizar diccionarios de cromosomas
					# Crear putnos
					dot=(genes[0],spices[0], chr, begin, end)
					dot1=(genes[1],spices[1], chr1, begin1, end1)
					# Agregar información del gen y su ortólogo
					self.addGenOrth(dot,dot1)
				elif(self.dotFlag):
					# Actualizar diccionarios de cromosomas
					# Agregar punto a lista de puntos
					dot=(genes[0],spices[0], chr, begin, end)
					# agregar información del gen
					self.addGenSpice0(dot)

	def addFilterUsed(self,gen):
		if(gen not in self.filterUsed): self.filterUsed+=[gen]

	def addGenSpice0(self,dot):
		if(dot[0] not in self.genesSpice0):
			self.genesInf[dot[0]]=dot[1:]
			self.genesOth[dot[0]]=[]
			self.genesSpice0+=[dot[0]]
			spice,chr,begin,end=dot[1:]
			self.checkChromosomeOfGen(chr,begin,end,spice)		

	def addGenSpice1(self,dot):
		if(dot[0] not in self.genesSpice1):
			spice,chr,begin,end=dot[1:]
			self.genesInf[dot[0]]=dot[1:]
			self.genesOth[dot[0]]=[]
			self.genesSpice1+=[dot[0]]
			self.checkChromosomeOfGen(chr,begin,end,spice)		


	def addGenOrth(self,dot,dot1):
		self.orthGenes+=[dot1[0]]	
		if(dot[0] in self.genesInf): self.genesOth[dot[0]]+=[dot1[0]]
		else:
			self.genesInf[dot[0]]=dot[1:]
			self.genesOth[dot[0]]=[dot1[0]]
			self.genesSpice0+=[dot[0]]
		if(dot1[0] in self.genesInf): self.genesOth[dot1[0]]+=[dot[0]]
		else:
			self.genesInf[dot1[0]]=dot1[1:]
			self.genesOth[dot1[0]]=[dot[0]]
			self.genesSpice1+=[dot1[0]]

	def delTrash(self):
		# Borrar banderas
		del self.flag
		del self.dotFlag

	def sortGenesList(self,genesList):
		minLocis=[]
		for gen in genesList: minLocis+=[self.genesInf[gen][2]]
		return numpy.argsort(minLocis)

	def write(self):
		# Escribir resultados
		with open(self.path+"Summary/"+self.projectName+"."+self.Allspices[0]+"_"\
				+self.Allspices[1]+self.distinction+".locusComparation_"\
				+self.Allspices[0]+"_orthologous","w") as F:
			self.printSortedGenesList(self.genesSpice0,F)
#		# crear orden de los genes de spice1
		# Escribir resultados
		order=self.sortGenesList(self.genesSpice1)
		with open(self.path+"Summary/"+self.projectName+"."+self.Allspices[0]+"_"\
				+self.Allspices[1]+self.distinction+".locusComparation_"\
				+self.Allspices[1]+"_orthologous","w") as F:
			self.printSortedGenesList(self.genesSpice1,F)

	def createDictPrinter(self):
		if(self.printNameOf=="genes"):
			if(self.genesdict.nameKind=="genes"): gen2name=lambda gen: gen
			else: gen2name=lambda gen: self.genesdict.label2gen[self.genesdict.protein2label[gen]]
		else:
			if(self.genesdict.nameKind=="proteins"): gen2name=lambda gen: gen
			else: gen2name=lambda gen: self.genesdict.label2protein[self.genesdict.gen2label[gen]]
		return gen2name

	def printSortedGenesList(self,genesList,F):
		order=self.sortGenesList(genesList)
		gen2name=self.createDictPrinter()
		for i in order:
			gen=genesList[i]
			kind=self.getGenKind(gen)
			F.write("{},{},{},{},{}\t".format(gen2name(gen),self.genesInf[gen][1],\
				str(self.genesInf[gen][2]),str(self.genesInf[gen][3]),kind))
			if(len(self.genesOth[gen])>0):
				for gen1 in self.genesOth[gen][:-1]:
					kind=self.getGenKind(gen1)
					F.write("{},{},{},{},{}|".format(gen2name(gen1),self.genesInf[gen1][1],\
						str(self.genesInf[gen1][2]),str(self.genesInf[gen1][3]),kind))
				gen1=self.genesOth[gen][-1]
				kind=self.getGenKind(gen1)
				F.write("{},{},{},{},{}\n".format(gen2name(gen1),self.genesInf[gen1][1],\
					str(self.genesInf[gen1][2]),str(self.genesInf[gen1][3]),kind))
			else: F.write("\n")
	def getGenKind(self,gen):
		if(gen in self.added): return "added"
		elif(gen in self.filterUsed): return "filter"
		elif(gen in self.orthGenes): return "orthologous"
		else: return "*"

	def addIntermediateGenes(self,genesList,spice):
		if(len(genesList)>0):
			order=self.sortGenesList(genesList)
			locusDict=self.locusdicts.nameLoci[spice]
			firstGen=genesList[order[0]]
			lastGen=genesList[order[-1]]
			begin=self.genesInf[firstGen][2]
			end=self.genesInf[lastGen][3]
			chr=self.genesInf[lastGen][1]
			for gen in locusDict:
				if(locusDict[gen][0]==chr):
					if((locusDict[gen][1]>=begin)&(locusDict[gen][2]<=end)):
						if(gen not in genesList):
							# Agregar punto a lista de puntos
							dot=(gen,spice, chr, locusDict[gen][1],locusDict[gen][2])
							# agregar información del gen
							self.addGenSpice1(dot)
							self.added+=[gen]

	def checkChromosomeOfGen(self,chr,begin,end,spice):
		if(chr not in self.spices2chrs[spice]):
			self.spices2chrs[spice]+=[chr]
			self.spices2maxSize[spice][chr]=end
			self.spices2minSize[spice][chr]=begin
		else:
			if(begin<self.spices2minSize[spice][chr]): self.spices2minSize[spice][chr]=begin
			if(end > self.spices2maxSize[spice][chr]): self.spices2maxSize[spice][chr]=end

	def getEdgeInf(self,edge):
		self.flag=self.dotFlag=True
		# Preparar lista de salida
		ret=[None for i in range(8)]
		# Obtiene genes y especies
		genes=[self.genesdict.label2name[label].split(".")[0] for label in edge]
		spices=[self.genesdict.labels2spices[label] for label in edge]
		# Obtener índices de de la spice0 en la arista
		spiceIndex=self.spiceListIdx(spices)
		# Verificar que spice0 exista en la arista
		if(len(spiceIndex)==1):
			# Verificar que el elemento 0 de la arista sea de la especie "spice0"
			if(spiceIndex[0]==1):
				edge=edge[::-1]
				genes=genes[::-1]
				spices=spices[::-1]
			ret[:2]=genes,spices
			# Filtar genes solo para protocaderinas
			if(self.filter(genes[0])):
				self.addFilterUsed(genes[0])
				# Obtner información del gen de spice0
				chr,begin,end=self.getGenInf(genes[0],spices[0])
				if(chr==None): self.flag=self.dotFlag=Flase
				else: ret[2:5]=chr,begin,end
				if(spices[1]==self.Allspices[1]):
					# Obtener info del ortólogo
					chr1,begin1,end1=self.getGenInf(genes[1],spices[1])
					if(chr1==None): self.flag=False
					else: ret[5:]=chr1,begin1,end1
				else: self.flag=False
			else:
				self.flag=self.dotFlag=False
		else: self.flag=self.dotFlag=False
		return ret

	def getGenInf(self,gen,spice):
		if(gen in self.locusdicts.nameLoci[spice]):
			chr,begin,end=self.locusdicts.nameLoci[spice][gen]
			begin=int(begin)
			end=int(end)
			if(begin<end): return chr,begin,end
			else: return chr,end,begin
		else:
			self.notInLocusDicts+=[gen]
			return None,None,None

	def initDicts(self):
		self.genesInf={}	# { gen : dot }, dot=(spice,chr,begin,end)
		self.genesOth={}	# { gen : [gen0 , gen1 , ...] }
		self.genesSpice0=[]	# list of genes of spice 0 that pass the filter
		self.genesSpice1=[]
		self.spices2chrs={}	# { spice : [chr1, chr2, ...] }
		self.spices2maxSize={}
		self.spices2minSize={}
		self.filterUsed=[]	# list of genes that pas the filter
		self.added=[]		# list of added genes
		self.orthGenes=[]
		self.notInLocusDicts=[]	# genes with no locus avalible
		for spice,i in zip(self.Allspices,range(self.nSpices)):
			self.spices2chrs[spice]=[]	# { especie : [chr1,chr2,...] }
			self.spices2maxSize[spice]={}	# { especies : { cromosoma : longitud máxima encontrada }}
			self.spices2minSize[spice]={}	# { especie : {chromosoma : longitud máxima encontrada }}

	def spiceListIdx(self,L):
		# Reorna lista con los índices donde se encuentra la especie self.Allspices[0]
		elements=[]
		for l,i in zip(L,range(len(L))):
			if(l==self.Allspices[0]): elements+=[i]
		return elements

	def initGenesFilter(self):
		#Verifica que el gen está en la lista self.genesFilter.
		# si self.genesFilter==None --> siempre pasa el filtro
		if(self.genesFilter==None): self.filter=lambda gen: True
		else: self.filter=lambda gen:gen in self.genesFilter
