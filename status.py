# Class to administrate the project status

# For the generalization of the class:
# - project class must be passed to class (alredy implemented)
# - nodes kinds must be passed to class
# - depencency graph edges must be passed to class
# - setAtribute method must be passed to class:
#   - method that manipulates the project atributes
#     and the staus with the Status.setNodeStatus
#     or setNodesListStatus methods.
#   - here could be good idea create a new class
#     atributeSetter, instead of setAtribue method.
# - runStep method that run project methods and
#   actualizates the method-nodes of staus

import os

class Status:
	""" 
	Class that administrates the status of a project.

	The status is administered with the help of a
	graph of dependecy, that we construct as follows:
	directed bipartite graph data struct, where a
	node has the atributes:
	- kind (file or method)
	- inEdges (dependency of the node)
	- outEdges (nodes that need the node of this atributes)
	- state (depending if ht node is method of file indicates if the method has been runned or if the files exist)
	"""

	# The dependency graph has 2 kind of nodes: files and methods
	files=["fasta","geneDicts","gff","proteinortho-files-1","proteinortho-files-2",\
				"proteinortho-files-3","graph","conexComponents",\
				"conexComponents-editions","tree-genes","tree-species",\
				"tree-editions","tree-reconciliation","tree-reconciliation-plot",\
				"orthology-synteny","orthology-synteny-plot","orthology-ages"]
	methods=["proteinortho-method-1","proteinortho-method-2","proteinortho-method-3","getGeneDicts"\
				"proteinortho-method-sinteny","proteinortho-2-adjacencyList","conexComponents-method",\
				"modular-decomposition","tree-reconciliation-method","tree-reconciliation-plot-method",\
				"orthology-synteny-plot-method","mincutsupertree"]
	# Now we set the edges, we divide it in 2 kinds: in-edges (represent dependecny) and out-edges (represents products)
	node2outEdges={
				"fasta":["proteinortho-method-1"],\
				"geneDicts":[],\
				"gff":["proteinortho-method-sinteny"],\
				"proteinortho-files-1":["proteinortho-method-2"],\
				"proteinortho-files-2":["proteinortho-method-3"],\
				"proteinortho-files-3":["proteinortho-2-adjacencyList"],\
				"getGeneDicts":["geneDicts"],\
				"graph":["conexComponents-method"],\
				"conexComponents":["modular-decomposition"],\
				"conexComponents-editions":[],\
				"tree-genes":["tree-reconciliation-method"],\
				"tree-species":["tree-reconciliation-method"],\
				"tree-editions":[],\
				"tree-reconciliation":["tree-reconciliation-plot-method"],\
				"tree-reconciliation-plot":[],\
				"orthology-synteny":["orthology-synteny-plot-method"],\
				"orthology-synteny-plot":[],\
				"orthology-ages":[],\
				"proteinortho-method-1":["proteinortho-files-1"],\
				"proteinortho-method-2":["proteinortho-files-2"],\
				"proteinortho-method-3":["proteinortho-files-3"],\
				"proteinortho-method-sinteny":["orthology-synteny"],\
				"proteinortho-2-adjacencyList":["graph"],\
				"conexComponents-method":["conexComponents"],\
				"modular-decomposition":["tree-genes","conexComponents-editions"],\
				"tree-reconciliation-method":["tree-reconciliation","tree-editions","orthology-ages"],\
				"tree-reconciliation-plot-method":["tree-reconciliation-plot"],\
				"orthology-synteny-plot-method":["orthology-synteny-plot"],\
				"mincutsupertree":["tree-species"]}
	node2inEdges={
				"fasta":[],\
				"geneDicts":["getGeneDicts"],\
				"gff":[],\
				"proteinortho-files-1":["proteinortho-method-1"],\
				"proteinortho-files-2":["proteinortho-method-2"],\
				"proteinortho-files-3":["proteinortho-method-3"],\
				"getGeneDicts":["fasta"],\
				"graph":["proteinortho-2-adjacencyList"],\
				"conexComponents":["conexComponents-method"],\
				"conexComponents-editions":["modular-decomposition"],\
				"tree-genes":["modular-decomposition"],\
				"tree-species":[],\
				"tree-editions":["tree-reconciliation-method"],\
				"tree-reconciliation":["tree-reconciliation-method"],\
				"tree-reconciliation-plot":["tree-reconciliation-plot-method"],\
				"orthology-synteny":["proteinortho-method-sinteny"],\
				"orthology-synteny-plot":["orthology-synteny-plot-method"],\
				"orthology-ages":["tree-reconciliation-method"],\
				"proteinortho-method-1":["fasta"],\
				"proteinortho-method-2":["proteinortho-files-1"],\
				"proteinortho-method-3":["proteinortho-files-2"],\
				"proteinortho-method-sinteny":["gff"],\
				"proteinortho-2-adjacencyList":["proteinortho-files-3"],\
				"conexComponents-method":["graph"],\
				"modular-decomposition":["conexComponents"],\
				"tree-reconciliation-method":["tree-genes","tree-species"],\
				"tree-reconciliation-plot-method":["tree-reconciliation"],\
				"orthology-synteny-plot-method":["orthology-synteny-plot"],\
				"mincutsupertree":["tree-genes"]}

	def __init__(self,project):
		"""
		- input: project object.
		"""
		self.project=project
		self.initGraphStatus()

	def setAtributesList(self,atributes,parameters):
		if(len(atributes)==len(parameters)):
			for atribute,parameter in zip(atributes,parameters):
				self.setAtribute(atribute,parameter)
		else: raise AttributeError("atributes list and parameters list must have the same length")

	def setAtribute(self,atribute,parameter):
		if(atribute=="project_name"): raise AttributeError("cant change the project name once it has been created")
		elif(atribute=="project_path"): raise AttributeError("cant change the project path once it has been created")
		elif(atribute=="secuences_path"): self.setPathAtribute(atribute,parameter,"sequencesPath",["fasta"])
		elif(atribute=="fasta_labels"): self.setNotNoneAtribute(atribute,parameter,"fastasLabels",[])
		elif(atribute=="fastas_format"): self.setNotNoneAtribute(atribute,parameter,"fastasFormat",[])
		elif(atribute=="secuences_kind"): self.project.setSecuencesKind(parameter)
		elif(atribute=="identifier_kind"): self.project.setIdentifierKind(parameter)
		elif(atribute=="species_tree"): self.setPathAtribute(atribute,parameter,"speciesTree",["tree-species"])
		elif(atribute=="proteinortho"): self.setNotNoneAtribute(atribute,parameter,"proteinorthoPath",[])
		elif(atribute=="reconciliation_filter"): self.setSimpleAtribute(atribute,parameter,"reconFilterType",[])
		elif(atribute=="recon_filter_object"): self.setSimpleAtribute(atribute,parameter,"reconListFilter",[])
		elif(atribute=="prt_steeps"): self.setNotNoneAtribute(atribute,parameter,"proteinorthoSteeps",[])
		elif(atribute=="gff_path"): self.setPathAtribute(atribute,parameter,"GTFsPath",[])
		elif(atribute=="gff_labels"): self.setPathAtribute(atribute,parameter,"GTFsLabels",[])
		else: raise AttributeError("No atribute '{}'".format(atribute))

	def setSimpleAtribute(self,atribute,parameter,projectAtribute,nodes):
		setattr(self.project,projectAtribute,parameter)
		self.setNodesListStatus(nodes,True)

	def setNotNoneAtribute(self,atribute,parameter,projectAtribute,nodes):
		if(parameter==None): self.setNodesListStatus(nodes,False)
		else:
			setattr(self.project,projectAtribute,parameter)
			self.setNodesListStatus(nodes,True)

	def setPathAtribute(self,atribute,parameter,projectAtribute,nodes):
		if(parameter==None): self.setNodesListStatus(nodes,True)
		else:
			if(os.path.exists(parameter)):
				setattr(self.project,projectAtribute,parameter)
				self.setNodesListStatus(nodes,True)
			else: raise AttributeError("the atribute '{}' cant be asigned to \n{}\ndirecory doesn't exist".format(atribute,parameter))

	def areDependenciesSatisfied(self,node):
		return not False in [self.node2status[dependency] for dependency in self.node2inEdges[node]]

	def checkMethodDependencies(self,node):
		files=self.getUnsetedDependencies(node)
		if(len(files)>0):
			methods=[self.status.getUnsetedDependencies(file) for file in files]
			raise AttributeError("To run {} first generate the files:\n{}\n with the methods\n{}".format(node,files,methods))

	def getUnsetedDependencies(self,node):
		unSet=[]
		for dependency in self.node2inEdges[node]:
			if(not self.node2status[dependency]): unSet+=[dependency]
		return unSet

	def runSteps(self,steps):
		if(steps==[0]): steps=[1,2,3]
		for step in steps: self.runStep(step)

	def runStep(self,step):
		if(step==1): self.project.proteinortho()
		elif(step==2):self.project.getEventTrees()
		elif(step==3): self.project.reconciliate()
		else: raise AttributeError("Step {} not defined".format(step))

	def isNodeSeted(self,node):
		return self.node2status[node]

	def setNodeStatus(self,node,status=True):
		self.node2status[node]=status
		if(node in self.methods and status):
			for file in self.node2outEdges[node]: self.setNodeStatus(file,True)
		elif(node in self.files and status):
			for method in self.node2outEdges[node]: self.setNodeStatus(method,False)

	def setNodesListStatus(self,nodes,status):
		for node in nodes: self.setNodeStatus(node,status)

	def initGraphStatus(self):
		self.node2status={node:False for node in self.files+self.methods}
