# coding=utf-8
import bmgmd as pr
import loadGenesListFilter as lg

project="SEGs"
path="/u/scratch/antonio/Ensemble_proteinas/"
speciesTree="/u/scratch/antonio/Species_trees/8s.speciesTree.awk"
secuencesPath="/u/scratch/antonio/Secuencias_ensemble_pep/"
proteinorthoPath="proteinortho5.pl"
GTFsPath="/u/scratch/antonio/GTF/"
GTFsLabels=pr.getGTFsLabels("/u/scratch/antonio/gtf2label")
overwrite=False
ignoreDotFiles=False
secuencesKind="proteins"

""" 
# Create a new project
p=pr.project(project,\
	path,\
	sequencesPath=secuencesPath,\
	speciesTree=speciesTree,\
	proteinorthoPath=proteinorthoPath,\
	overWrite=overwrite,\
	ignoreDotFiles=ignoreDotFiles,\
	GTFsPath=GTFsPath,\
	GTFsLabels=GTFsLabels,\
	secuencesKind=secuencesKind)
print("project created!")
p.save()
"""

# Load a project
p=pr.load(project,path)
print("project loaded")

""" 
# run proteinortho
p.proteinortho(kind="p")
print("prt done!")
p.save()
"""

""" 
# create dictionarys
p.setDictionarys()
print("Diccionarios creados")
#p.save()
"""

""" 
# imprime lista de ortología de ratón
p.printSpiceOrthologos("MOUSE",includeAge=True)
print("Se imprimieron los ortólgos")
"""

""" 
# create adjacency list
p.proteinortho2adjL()
print("adjacenci list created")
#p.save()
"""

# Obtiene las proteínas correspondientes a las protocaderinas
genesFilter=[]
genesDict=p.loadDict("GenesDicts")
for gen in lg.protocaderinas("/u/scratch/katia/protocadherins_cluster_mmus.txt"):#,group="beta"):
	if(gen in genesDict.gen2label): genesFilter+=[genesDict.label2name[genesDict.gen2label[gen]]]

# Imprime comparación de locus de familias de genes
lc=p.locusComparation("MOUSE","GALUS",genesFilter=genesFilter)
lc.write()
lc.save()
print("p.locusComparation done")

""" 
# Obtein singletones
p.getSingletones()
print("Se obtubieron los singletones\n")
#p.save()
"""

""" 
# Obtain conex components
p.getCC()
#p.save()
print("CC files created")
"""

""" 
# Obtener árboles de eventos
p.getEventTrees(editPrimeModules=True)
p.save()
print("Se descompusieron todos los árboles")
"""


""" 
# Reconciliar árboles de eventos
rmt=p.reconciliate(include_edited=False,genesFilter="MOUSE")
print("Se reconciliaron árboles")
p.save()
"""

""" 
# Guardar árbole mapeado
rmt.T.save(path+project+"/",project)
print("Se guardó árbol de eventos")
"""
