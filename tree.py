# coding=utf-8
#from matplotlib import pyplot as plt
#from matplotlib.colors import rgb2hex
from scipy.special import binom
from itertools import combinations
from copy import deepcopy
from networkx import Graph,connected_components
#from nets import cut
import dicts
from order import Order

import re
import os
import pickle

def LeavsUnion(T):
	"Retorna unión de las hojas de la lista de árboles T"
	S=set()
	for t in T:
		for l in t.leavs:
			S.add(l)
	return S

def starTree(S):
	"Regresa un árbol estrella donde las hojas son los elementos de la lista S"
	if(len(S)>1):
		t="("
		for s in S: t+=str(s)+","
		return Tree(t[:-1]+")")
	else: return Tree(list(S)[0])

def properSubSetGraph(Trees,S):
	'Construir grafo donde la arista (A,B,c) significa que A y B están en el mismo cluster propio de c árboles en Trees'
	St=Graph()
	for s in S: St.add_nodes_from(S)
	#    St=Graph(S)
	for T in Trees:
		T.forBro(action=lambda B:St.add_edges_from([par for par in combinations(B.indE(),2)])) #          .addEdges([tuple(list(par)+[1]) for par in combinations(B.indE(),2)]))
	return St

def get_w_edges(St,w):
	"Retorna aristas del grafo St con peso w"
	Et=[]
	for E in G.edges(data=True):
		if(E[2]['weight']==w): Et+=E[:2]
	return Et

def properSubsets(T):
	"Retorna lista de los subconjuntos propios mas grandes que pueden obtener del árbol T"
	p=T.wp[:]
	n=len(T.root)
	A=[]
	for i in range(1,n):
		T.setWP([i])
		A+=[list(T.indE())]
	T.setWP(p)
	return A

def isProperSubset(T,leavs):
	"Return True if the set leavs is a proper sub set of T"
	return True in [True if set(leavs).intersection(pss)==set(leavs) else False for pss in properSubsets(T)]

def compact_w_edges(St,Trees,S,w):
	"Comprime todas las aristas con peso w. Se borran las aristas paralelas y se remplazan con una arista cullo peso es la suma de los pesos de los árboles que tienen un subconjunto propio que contiene los extremos de almenos una arista en esa clase paralela"
	# Obtener aristas inducidas por todos los árboles
	Et=get_w_edges(St,w)
	# Por cada arista en Et Contraer arsita, Quitar y remplazar aristas paralelas
	for E in Et:
		# Hojas involucradas en E
		leavs=[]
		# Cada elemnto de E puede ser un solo elemento, o una tupla, que es un conjunto de nodos (un nodo comprimido)
		for e in E:
			# Si el elemnto e está en S ---> entonces es un nodo simple
			if(e in S): leavs+=[e]
			# else es una tupla con mas de un nodo
			else: leavs+=e
		# Crear nodo comprimido
		Nc=tuple(sorted(leavs))
		St.add_node(Nc)
		# Poner peso de las aristas paralelas = número de árboles en los que leavs es un subconjunto propio del árbol con las hojas en los extremos de las aristas 
		# Los vecions co los que tendrán aristas paralelas son aquellos que están en la interección de las vecindades de los nodos en los extremos de la arista a contraer
		for vecino in set([n for n in G.neighbors(E[0])]).intersection([n for n in G.neighbors(E[1])]):
		#        for vecino in set(St.adjL[E[0]]).intersection(St.adjL[E[1]]):
			for T in Trees:
				if(isProperSubset(T,leavs+(vecino if len(vecino)>1 else [vecino]))): #St.addEdges([(vecino,Nc,1)])
					if(St.has_edge(vecio,Nc)):
						St[vecino][Nc]['weight']+=1
					else:
						St.add_edge(vecio,Nc,weight=1)
		# Borrar nodos en E y aristas hacia los nodos en E
		St.remove_nodes_from(E)

def minCutSuperTree(Treess):
	"Obtiene el super-arbol de la lista de árboles Trees"
	# Quitar Nones, de árboles que no se pueden inducir
	Trees=[]
	for T in Treess:
		if(T!=None): Trees+=[T]
	S=LeavsUnion(Trees)
	if(len(S)>2):
		# Grafo donde la arista (A,B,c) significa que A y B están en el mismo cluster propio de c árboles en Trees'
		St=properSubSetGraph(Trees,S)
		# Si St es no conexo las nuevas hojas son los elementos de las componentes conexas de St, else son las componentes conexas de St_Et
		# Obtener componentes conexas de St
		#        S=BFS(St)
		S=[list(cc) for cc in list(
		connected_components(St))]
		if(len(S)==1):
			# Crear St_Et
			St_Et=deepcopy(St)
			compact_w_edges(St_Et,Trees,S,len(Trees))
			Ecut,Wcut=cut(St_Et)
			for ecut in Ecut: St_Et.remove_edges_from(ecut)
			#            for ecut in Ecut: St_Et.delEdges(ecut)
			# Obtener componentes conexas de St_Et
			S=[list(cc) for cc in list(connected_components(St))]
		# Para cada conjunto de nodos S_i in S aplica minCutSuperTree a los árboles inducidos por los árboles de entrada y S_i
		T_j=[minCutSuperTree([T.induceSubTree(S_i) for T in Trees]) for S_i in S]
		T=joinTrees(T_j)
		return T
	else:
		return starTree(S)

def invE(B):
	"input: B: lista de genes. Retorna diccionario donde la llave es una especie y el contenido es cuántos genes en B hay de esa especie"
	spices={}
	for node in B:
		if((node[-1]!=')')&(node[-1]!='P')&((node[-1]!='S')|(node=="GALUS"))):
			spice=gen2spice(node)
			if(spice in list(spices)): spices[spice]+=1
			else: spices[spice]=1
	return(spices)

def invG(B):
	"B es una lista de strings. Retorna diccionario donde la llave es un gen en B, y el contenido es cuántas veces aparece"
	# Posible errpr lógico: Para cualquier llave de genes su contenido siempre va a ser 1
	genes={}
	for node in B:
		if((node[-1]!=')')&(node[-1]!='P')&(node[-1]!='S')):
			if(node in list(genes)): genes[node]+=1
			else: genes[node]=1
	return(genes)

def readS(s,i):
	"lee la string s (que es formato awk) a partir del elemento i-ésimo. Retorna lo que fue leído y el índice del último elemento leído"
	l=len(s)
	R=''
	while((s[l-1-i]!=')')&(s[l-1-i]!='(')&(s[l-1-i]!=',')&(i<l)):	# Lee cosa antes de símbolo
		R+=s[l-1-i]
		i+=1
	return([i,R[::-1]])	# Regresa índice del stop y palabra leída

def Name(P):
	"Crea nombre de nodo en base a su ruta desde root"
	if(len(P)==0):
		return('')
	else:
		n=('/'+str(P[0]))
		for p in P[1:]: n+=('/'+str(p))
		return(n)

def gen2spice(G):
	"get the spice of the gen"
	l=re.split('\d+',G)[0]
	return l

def areCongruent(t,T):
	"Es congruente t con T?, es decir, t está contenido en T?"
	outG_T=list(T.triplet)
	outG_t=list(t.triplet)
	# Revisa todos los conjuntos tripletas ( A_t | B_t ) in t.triplets
	i=0
	f=0
	while((i<len(outG_t))&(f==0)):
		A_t=outG_t[i]
		B_t=t.triplet[A_t]
		b_t=set(B_t)
		# Revisa todos los a_t in A_t
		j=0
		while((j<len(A_t))&(f==0)):
			a_t=A_t[j]
			# Revisa todos los conjuntos tripletas ( A_T | B_T ) in T.triplets
			k=0
			while((k<len(outG_T))&(f==0)):
				A_T=outG_T[k]
				# Revisar si  a_t in A_T
				if(a_t in A_T):
					B_T=T.triplet[A_T]
					b_T=set(B_T)
					# Reviar que b_t in b_T
					if(b_t<=b_T): k=len(outG_T)+1
					else:
						t.tripletaMala=(a_t,B_t)
						f=1
				else: k+=1
			if(k==len(outG_T)):
				t.tripletaMala=(a_t,'|')
				f=1
			j+=1
		i+=1
	if(f==0): return True
	else: return False

def loadSingles(file):
	with open(file) as F:
		# leer especies en e header
		header=F.readline()
		spices=header.split()[4:]
		# Crea diccionario { especie : [singletones] }
		sing={spi:[] for spi in spices}
		# Lee el resto del archivo
		for line in F:
			# lee número de especies en el renglón, número de genes y los genes que encontró
			lineSplit=line.split()
			NoSpices=lineSplit[0]
			NoGenes=lineSplit[1]
			genes=lineSplit[3:]
			# Si es singleton
			if(NoGenes=="1"):
				# Identifica el gen y agrega el singleton
				for spice,gen in zip(spices,genes):
					if(gen!='*'): sing[spice]+=[gen]
	return sing

class reconMT:
	'Clase para reconciliar muchos árboles'
	def __init__(self,project,path,include_edited,genesdicts,genesFilter=None,speciesTree=None):
		self.speciesTree=speciesTree
		# Definir consición para reconciliar o no a un grafo
		if(include_edited): condition=lambda f:(project+"_" in f[0])&("nPrime"==f[-2])
		else:
			def condition(f):
				return (project+"_" in f[0])&("nPrime"==f[-2])&(".edited" not in f[1])
#			condition=lambda f:(project+"_" in f[0])&("nPrime"==f[-2])&(".edited" not in f[1])
		# Load dictionary of real genes names
		gen2label=genesdicts.gen2label
		label2gen=genesdicts.label2gen
		self.D=label2gen
		# Definir función para filtrar árboles de genes
		if(genesFilter==None): genesTreeFilter=lambda t: True
		elif(genesFilter=="MOUSE"):
			def genesTreeFilter(t):
				MiLeavs=["MOUSE" in name for name in t.leavs]
				return True in MiLeavs
#			genesTreeFilter=lambda t: True in ["MOUSE" in name for name in t.leavs]
		else: genesTreeFilter=lambda t: len(set([label2gen[leav].split(".")[0] for leav in t.leavs]).intersection(genesFilter))>0
		# Load species tree
		T=Tree(open(self.speciesTree).readline().split(';')[0],weigth=True)
		self.T=T
		# Open output files
		self.F_rec=open(path+"Summary/"+project+".reconciliados",'w')
		self.F_inc=open(path+"Summary/"+project+".incongruentes",'w')
		self.F_age=open(path+"Summary/"+project+".edades",'w')
		# Comienza reconciliación masiva
		with open("/u/scratch/antonio/Ensemble_proteinas/SEGs/Trees/SEGs.modular_decomposition") as f:
			for line in f:
				file,awk=line.split()
				t=Tree(awk.split(";")[0])
				# Revisa que se cumpla la condición para reconciliar árboles de genes
				if(genesTreeFilter(t)):
					self.t=t
					t.cc=file
					t.recon(T)
					self.anotaEdades()
		# Guardar singletones
		sings=loadSingles(path+"Prt/"+project+".proteinortho")
		self.T.singletones=sings
		for spi in sings:
			for sing in sings[spi]:
				self.F_age.write(sing+"	"+str(0)+"	singletone\n")
		# Cerrar archivos
		self.F_rec.close()
		self.F_inc.close()
		self.F_age.close()
		# Guardar árbol
		with open(path+"Summary/"+project+".tree","wb") as F: pickle.dump(T,F)
		# Guardar mapeo de nodos
		F_nodeMap=open(path+"Summary/"+project+".mapeo_n","w")	# Archivo de mapeo de nodos
		for node in list(T.nodesW): F_nodeMap.write(node+'	'+str(T.nodesW[node])+'\n')
		F_nodeMap.close()
		# Guardar mapeo de aristas
		F_gainMap=open(path+"Summary/"+project+".mapeo_e","w")	# Archivo de mapeo de aristas
		for edge in list(T.edgesG): F_gainMap.write(str(edge)+'	+'+str(T.edgesG[edge])+'	-'+str(T.edgesL[edge])+'\n')
		F_gainMap.close()
		# Guardar coorthologos
		with open(path+"Summary/"+project+".coorthologs","w") as F_coor:
			for node in T.coorthologs:
				F_coor.write(">> "+node+"\n")
				for genes in T.coorthologs[node]:
					if(len(genes)>0):
						for gene in genes[:-1]: F_coor.write(self.D[gene]+",")
						F_coor.write(self.D[genes[-1]]+"\n")
		with open(path+"Summary/"+project+".coparalogs","w") as F_copa:
			for edge in T.coparalogs:
				F_copa.write(">>"+str(edge)+"\n")
				for genes in T.coparalogs[edge]:
					if(len(genes)>0):
						for gene in genes[:-1]: F_copa.write(self.D[gene]+',')
						F_copa.write(self.D[genes[-1]]+'\n')

	def anotaEdades(self,specie='MOUSE'):
		'tree,componente-conexa del tree, Archivo de edades, Diccionario, null si el '
		# Anota eddes de los genes en árboles y si son congruentes o nel
		if(self.t.incongreunt_tree):
			self.F_inc.write(self.t.cc+'	'+str(self.t.tripletaMala)+'\n')	# Escribe que no se pudo
			for gene in self.t.leavs:
				if(gen2spice(gene)==specie):
					self.F_age.write(self.D[gene]+"	-"+"	"+self.t.cc+'\n')
		else:
			self.F_rec.write(self.t.cc+'\n')	# Anotar que sí se pudo
			for gene in self.t.edades:
				if(gen2spice(gene)==specie):
					self.F_age.write(self.D[gene]+"	"+str(self.t.edades[gene])+"	"+self.t.cc+"\n")

	def save(self,path,projectName):
		with open(path+"Summary/"+projectName+".rmt","wb") as F: pickle.dump(self,F)

def joinTrees(T_j):
	'Crea un nuevo árbol donde las raíces de los árboles en T_j son hijos de una nueva raíz'
	rootName='('
	for Tj in T_j: rootName+=Tj.root[0]+','
	rootName=rootName[:-1]
	rootName+=')'
	T=Tree(rootName)
	return T

class Tour:
	"Clase que lleva a cabo el recorrido y las acciones sobre un árbol"
	def evaluate(self):
		"Función para evaluar las condiciones y aplicar acciones para el 'nodo act' en el recorrido"
		if(self.back(self.T)):
			self.backTour()
		else:
			self.f=True
			if(self.condition(self.T)):
				for action in self.actions: action(self.T)
			else:
				for action in self.noActions: action(self.T)

	def backTour(self,actions=[lambda N0: None],reactions=[lambda N0: None]):
		"Función para regresar en el recorrido, sin importar el resto de la rama"
		for action in actions: action(self.T)
		self.order.pop()
		if(self.order.size()>0):
			self.T.setWP(self.T.wp[:-1])
			next=self.order.pop()+1
			self.order.push(next)
			self.f=True
			for reaction in reactions: reaction(self.T)
		else:
			self.f=False
			self.f=False

	def go(self):
		"Recorrido del árbol. Lo hace en forma de DFS (con pilas)"
		self.order.push(1)
		self.evaluate()
		while(self.f):
			next=self.order.next()
			if(next<len(self.T.wn)):
				self.T.setWP(self.T.wp+[next])
				self.order.push(1)
				self.evaluate()
			else:
				self.backTour()

	def __init__(self,T,actions=[lambda n0: None],condition=lambda n0: True,noActions=[lambda n0: None],back=lambda n0: False):
		"Inicializa recorrido"
		self.T=T
		self.actions=actions
		self.condition=condition
		self.noActions=noActions
		self.back=back
		self.order=Order()

class Tree:
	'Árbol: Los nodos internos son listas, [N,n_1,n_2,...] donde N es el label del nodo y n_i sus elementos.'
	def nodeAge(self):
		# Return age of the self.wn
		return 2*len(self.wp)

	def edgeAge(self):
		return 2*len(self.wp)-1
		# Return the age of the paren edge's self.wn

	def induceSubTree(self,Ss):
		'Determina subárbol inducido por las hojas S'
		# Descomprimir nodos comprimidos (sacar de las tuplas)
		S=[]
		for Si in Ss:
			if(type(Si)==tuple):S+=Si
			else: S+=[Si]
		S=set(S)
		# Si S no es un subconjunto de las hojas self, entonces no se puede inducir el árbol.
		if(S<=set(self.leavs)):
			# Crear copia del árbol original
			T=deepcopy(self)
			# Quitar hojas que no aparecerán
			quit=[]
			for leav in T.leavs:
				if(leav not in S): quit+=[leav]
			for leav in quit: del T.leavs[leav]
			# Lista de direcciones de ramas que se cortarán
			paths=[]
			# Inicializa recorrido
			rec=Tour(T)
			# La acción es agregar dirección a paths y regresar en el recorrido
			rec.actions=[lambda N0: rec.backTour(actions=[lambda n0: paths.append(n0.wp[:])])]
			# Solo se va a ejecutar la acción cuando el nodo actual N0 no esté en un camino de la raíz a cualquier elemento de S
			rec.condition=lambda N0: len(S.intersection(N0.induceLeavs()))==0
			# Recorrer!
			rec.go()
			# Recortar cada una de las ramas en paths
			n=len(paths)
			for i in range(n): # (recorremos por índice porque cada elemento que borramos cambian los paths, ergo, solo cambiamos n-i paths para 0<=i<n)
				# digamos que la rama a cortar es path=[a,b,c,d]
				# Quitar rama: nos paramos en [a,b,c] y borramos el elemento d
				path=paths[i]
				T.setWP(path[:-1])
				del T.wn[path[-1]]
				# Actualizar las paths que cambian y que se tendrán que cortar
				afectados=[]
				for j in range(i+1,n):
					if(T.isWnInPath(paths[j])):
						afectados+=[j]
						if(paths[j][len(path)-1]>path[-1]): paths[j][len(path)-1]-=1
				# Actualizar paths de hojas
				dd=len(path)-1
				for leav in T.induceLeavs():
					if(T.isWnInPath(T.leavs[leav])):
						if(T.leavs[leav][dd]>path[-1]): T.leavs[leav][dd]-=1
				# Si el nodo actual se quedó con un solo hijo --> contraer
				if(len(T.wn)==2):
					# Actualizar paths de hojas
					for leav in T.induceLeavs():
						del T.leavs[leav][dd]
					# Contraer nodo: Si estamos en el nodo raíz --> la nueva raíz es el hijo de la vieja raíz
					if(len(T.wp)==0):
						T.root=T.root[1]
						dd=0
					else: # else El hijo del nodo a contraer ahora será hijo del padre del nodo a contraer
						hijo=T.wn[1]
						T.setWP(path[:-2])
						T.wn[path[len(path)-2]]=hijo
						dd=len(path)-2
					# actualizar fututos paths afectados
					for j in afectados: del paths[j][dd]
			T.setWP([])
			# Retorna el árbol recortado
			return T
		else: return None

	def save(self,path,projectName):
		with open(path+"Summary/"+projectName+".tree","wb") as F: pickle.dump(self,F)

	def induceLeavs(self):
		"Retorna lista de hojas inducidas por el nodo actual"
		n=len(self.wp)
		L=[]
		for leav in self.leavs:
			f=True
			i=0
			while((i<n)&(f)):
				if(self.wp[i]!=self.leavs[leav][i]): f=False
				i+=1
			if(f): L+=[leav]
		return L

	def forBro(self,action=lambda b: None):
		"Realiza acción por cada hijo del nodo actual"
		path=self.wp[:]
		for i in range(1,len(self.wn)):
			self.setWP(path+[i])
			action(self)
		self.setWP(path)

	def upperEdg(self):
		"Retorna la arista raíz del nodo actual"
		p=self.wp[:]
		if(len(p)>0):
			self.setWP(p[:-1])
			N0=self.wn[0]
			self.setWP(p)
			return tuple([N0,self.wn[0]])
		else:
			return tuple(['-/', '/'])

	def isWnInPath(self,path):
		'True in working node is in some node of the path'
		if(len(self.wp)<=len(path)):
			l=len(self.wp)
			i=0
			while(i<l):
				if(self.wp[i]==path[i]): i+=1
				else: i=l+2
			if(i==l+2): return False
			else: return True
		else: return False

	def isHere(self,B_lca):
		'True if your path is some of the paths in B_lca. Each path is a list.'
		i=0
		l=len(B_lca)
		while(i<l):
			if(self.wp==B_lca[i]): i=l+1
			else: i+=1
		if(i==l+1): return True
		else: return False

	def sumGene(self): self.nodesW[self.wn[0]]+=1

	def loseGene(self):
		self.edgesL[self.upperEdg()]+=1

	def heredar(self,targets,restriction=lambda N0: False):
		heredar=Tour(self)
		# Establecer condiciones del recorrido de segregación
		heredar.actions=[Tree.sumGene]
		heredar.condition=lambda N0: len(set(targets).intersection(N0.indE()))>0
		heredar.noActions=[lambda N0: heredar.backTour(actions=[Tree.loseGene])]
		heredar.back=restriction
		heredar.go()

	def getSpesRestrictors(self,t):
		'Regresa lista de nodos que no pueden ser visitados en self al segregar el mapeo del nodo t.wn'
		restrictors=[]
		t.forBro(action=lambda b: None if len(b.wn)==1 else\
			restrictors.append(self.lLCAn(list(b.indE()))) if len(b.indE())>1\
			else restrictors.append(self.leavs[list(b.indE())[0]]))
		return restrictors

	def soonLeavs(self):
		sl=[]
		self.forBro(action=lambda b: sl.append(b.wn[0]) if len(b.wn)==1 else None)
		return sl

	def mapSpes(self,t):
		# Determinar freno de segregación de genes
		restrictors=self.getSpesRestrictors(t)
		targets=list(t.indE())
		Tup=tuple(sorted(targets))
		self.setWP(self.lLCAn(targets))
		# Anota edades de genes hijos de n0
		age=nodeAge()
		soonLeavs=t.soonLeavs()
		for gen in soonLeavs: t.edades[gen]=age
		# Guarda coorthologos
		self.coorthologs[self.wn[0]]+=[soonLeavs]
		# Cuenta genes en conjunto
		if(Tup in self.nodeSW[self.wn[0]]): self.nodeSW[self.wn[0]][Tup]+=1
		else: self.nodeSW[self.wn[0]][Tup]=1
		# Heredar genes
		self.heredar(targets,restriction=lambda N0: N0.isHere(restrictors))

	def isDecendent(self,P):
		if(len(P)<len(self.wp)):
			if(self.wp[:len(P)]==P): return True
			else: return False
		else: return False

	def mapDupl(self,t):
		# Definir objetivos
		Targets=list(t.indE())
		Tup=tuple(sorted(Targets))
		# Calcular edad de los hijos hojas
		if(len(Targets)>1): self.setWP(self.lLCAn(Targets))
		else: self.setWP(self.leavs[Targets[0]])
		age=edgeAge()
		# Guarda edad de los hijos hojas
		soonLeavs=t.soonLeavs()
		for gen in soonLeavs: t.edades[gen]=age
		# Guardar coparalogos
		self.coparalogs[self.upperEdg()]+=[soonLeavs]
		# Mapear duplicación
		edg=self.lLCAe(list(Targets))
		self.edgesG[edg]+=len(t.wn)-2
		if(Tup in self.edgeSW[edg]): self.edgeSW[edg][Tup]+=len(t.wn)-2
		else: self.edgeSW[edg][Tup]=len(t.wn)-2
		# Programar recorridos
		P=t.wp[:]
		for i in range(1,len(t.wn)):
			t.setWP(P+[i])
			targets=list(t.indE())
			# Si n0 no es hoja
			if(len(t.wn)>1): restrictor=self.lLCAn(targets)
			else:
				restrictor=[]
				# anotar edad de la hoja
				t.edades[t.wn[0]]=age
			self.heredar(targets,restriction=lambda N0: N0.isHere([restrictor]))
		t.setWP(P)

	def mapEvent(self,T):
		nk=self.nodeK()
		if(nk=='S'): T.mapSpes(self)
		elif(nk=='P'): T.mapDupl(self)

	def mapRoot(self,T):
		'Mapea la arista raíz de self a T, así como las respectias pérdidas'
		T.setWP([])	# El recorrido comienza desde la raíz
		target=T.lLCAn(list(self.indE()))	# path(N0) | n0 maps to N0	
		rec=Tour(T)
		rec.condition=lambda N0: N0.isWnInPath(target)
		rec.actions=[lambda N0: N0.sumGene()]
		rec.noActions=[lambda N0: N0.loseGene(),lambda N0: rec.backTour()]
		rec.back=lambda N0: N0.isHere([target])
		rec.go()

	def recon(self,T):
		'reconcilia árbol de genes self con el arbol de especies T'
		if(areCongruent(self,T)):
			# inicializar diccionario de edades de genes
			self.edades={}
			# Mapear pérdidas en especies que no involucran a self
			self.mapRoot(T)
			# Definir recorrido
			rec=Tour(self)
			rec.actions=[lambda n0: rec.backTour() if n0.nodeK()=="G" else n0.mapEvent(T)]
			rec.go()
			self.incongreunt_tree=False
			return True
		else:
			self.incongreunt_tree=True
			return False

	def wasCorrectMap(self):
		"return True if yes,else False"
		f=0
		self.setWP([])
		S=[1]
		while((len(S)>0)&(f==0)):
			if(len(self.wn)>S[-1]):	# Si el siguiente hijo existe --> lo analizamos
				# Arista por donde se bajará
				edg=tuple([self.wn[0],self.wn[S[-1]][0]])
				# Contar cuantos genes pasaron de edg[0] a edg[1]
				sum=0
				if(edg[0] in self.nodesW): sum+=self.nodesW[edg[0]]
				if(edg in self.edgesG): sum+=self.edgesG[edg]
				if(edg in self.edgesL): sum-=self.edgesL[edg]
				# Ver genes actuales en edg[1]
				if(edg[1] in self.nodesW): sum1=self.nodesW[edg[1]]
				else: sum1=0
				# Ver si el flujo fue correcto
				if(sum==sum1):	# Si el flujo es congruente
					self.setWP(self.wp+[S[-1]])	# Ponemos a hijo en la pila
					S+=[1]	# Ponemos a un elemento mas en la stack
				else: f=1	# Error en el mapeo
			else:	# Regresar un nodo y visitar al siguiente hermano
				del S[-1]
				if(len(S)>0):
					self.setWP(self.wp[:-1])
					S[-1]+=1
		if(f==0): return True
		else: return False

	def addE(self,N):
		"Agrega edge de wp to N con peso 0"
		n=Name(self.wp)+'/'
		self.edgesG[n,n+str(len(self.wn)-1)+'/']=0

	def putN(self,N):
		"Agrega N como hijo del nodo actual"
		if(len(N)==0): N=Name(self.wp)+'/'+str(len(self.wn))
		self.wn+=[[N]]
		if(self.weigth):
			self.addE(N)
			self.nodesW[N]=0

	def getLeavs(self):
		"Regresa lista con índex de elementos hoja"
		l=[]
		for i in range(1,len(self.wn)):
			if (len(self.wn[i])==1): l+=[i]
		return(l)

	def addLeaves(self):
		"Crea diccionario self.leavs"
		L=self.getLeavs()	# Saca índice de ojas
		for l in L:
			leaveL=self.wn[l][0]	# leave label
			self.leavs[leaveL]=self.wp[:]+[l]	# Pone las hojas en el diccionario

	def getN(self,path):
		"Get the node in the path"
		t=self.root
		for i in range(len(path)): t=t[path[i]]
		return(t)

	def setWP(self,P):
		"Set working path"
		self.wp=P[:]	# Path de trabajo
		self.wn=self.getN(P)

	def initC(self):
		"Inicializa listas de colapzo para recorrido de abajo hacia arriba"
		self.nLeavs={}	# Nodos colpasados, nodo:{[ruta,[nodos]]} Los primeros son las hojas
		self.nDist={}	# Distancia a nodos colapnsados desde root
		for leav in list(self.leavs):	# Inicializa nodos a colpazar
			P=self.leavs[leav]	# Saca ruta en forma de lista
			lP=len(P)
			if((lP) in list(self.nDist)): self.nDist[lP]+=[leav]	# Agrega hoja con distancia lP
			else: self.nDist[lP]=[leav]	# Crea distancia lP y agrega hoja
			self.nLeavs[leav]=[P,[leav]]	# Crea nodo colpasado de la hoja

	def nodeK(self):
		"retorna Node kind: (G) Gen, (P) Nodo paralelo, (S) nodo serie ó (I) ningun tipo"
		if((self.wn[0][-1]=='P')|(self.wn[0][-1]=='S')):
			if(self.wn[0]=="GALUS"): return "G"
			else: return (self.wn[0][-1])
		elif(self.wn[0][-1]=='*'): return("*")	# prime node
		elif(self.wn[0][-1]==')'): return ('I')	# undefined-event inner node
		else: return('G')	# Es gen

	def lLCAn(self,S):
		"Encuentra last common ancestor node de una lista de hojas"
		P=[self.leavs[s] for s in S]	# Lista de rutas
		if(len(P)>1):	# Si hay mas de una especie...
			ld=max([len(p) for p in P ])	# Límite de produndidad
			i=0	# Profundidad
			f=0	# flag			p=[]	# Ruta común
			p=[]
			while(i<ld):
				j=1
				while((j<len(P))&(f==0)):	# Para comparar las rutas
					if(P[0][i]==P[j][i]): j+=1
					else: f=1
				if(f==0):
					p+=[P[0][i]]
					i+=1
				else: i=ld
		else:
			if(len(P)>0):
				p=P[0][:]	# Pues es la ruta de la especie :v
				if(len(p)>0): del p[len(p)-1]
			else: p=[]
		return(p)

	def getName(self,path):
		N=self.root
		for i in path: N=N[i]
		return(N[0])

	def lLCAe(self,S):
		"Last common ancestor edge de un conjuntode hojas"
		if(len(S)>1):
			p1=self.lLCAn(S)[:]
			if(len(p1)>0):
				p0=p1[:-1]
				return((self.getName(p0),self.getName(p1)))	# LCAe
			else: return(('-',self.root[0]))
		else:
			p1=self.leavs[S[0]]
			p0=p1[:-1]
			return((self.getName(p0),self.getName(p1)))	# LCAe

	def areEqIL(self):
		" Return 'TRUE' if Are equal Induced spices of working path, else 'FALSE'"
		B=[self.wn[i][0] for i in range(1,len(self.wn))]	# Todos los hijos
		S=set(list(invE(self.nLeavs[B[0]][1])))
		i=1
		f=0
		while((i<len(B))&(f==0)):
			if(len(S.symmetric_difference(list(invE(self.nLeavs[B[i]][1]))))==0): i+=1
			else: f=1
		if(f==0): return('TRUE')
		else: return('FALSE')

	def indE(self):
		"Retorna dicciionario de especies inducidas por el nodo actual, donde las llaves son las especies, y el contenido es el número de genes en las hojas de tales especies"
		B=[self.wn[i][0] for i in range(1,len(self.wn))]
		BB=[]
		if(self.nodeK()=='G'):
			BB=invE([self.wn[0],'A)'])
		else:
			for bro in B: BB+=self.nLeavs[bro][1]
			BB=invE(BB)
		return(BB)

	def wnProf(self):
		"returns Profundidad del nodo actual"
		return 2*len(self.wp)+1

	def upEProf(self):
		"Retorna Profundidad de la arista raíz del nodo actual"
		return 2*(len(self.wp))

	def downEProf(self):
		"Retorna profundidad de ñas aristas hijo del nodo actual"
		return 2*(len(self.wp)+1)

	def addNodeTriplets(self):
		"Se crearán los conjuntos de tripletas A|B, donde las tripletas posibles son a_i|{b_j,b_k}, a_i in A, b_j,k in B, j!=k"
		B_ind=[]	# Lista de especies inducidas por cada hijo
		inGroup=[]	# índice de los elementos in-group de la especiación
		path=self.wp	# Path base para sacar tripletas
		# Determina los hijos inducidos de cada nodo y los in-groups
		for i in range(1,len(self.wn)):
			self.setWP(path+[i])
			B_ind+=[list(self.indE())]
			if(self.nodeK()!="G"): inGroup+=[i-1]
		self.setWP(path)
		# Pone a los ingroups a la estructura de dato
		for i in inGroup:
			A=set()
			for j in range(len(B_ind)):
				if(i!=j): A=A.union(B_ind[j])
			A_tup=tuple(sorted(A))
			B_tup=tuple(sorted(B_ind[i]))
			if(A_tup in self.triplet): self.triplet[A_tup]=tuple(sorted(set(B_tup).union(self.triplet[A_tup])))
			else: self.triplet[A_tup]=B_tup
		borr=[]
		for A in self.triplet:
			if(len(self.triplet[A])==1): borr+=[A]
		for b in borr: del self.triplet[b]


	def tripletSets(self,condition=lambda T:True):
		""" 
		Inicializar diccionario de tripletas:

		self.triplet={ A : B }

		A=(a_1,a_2,...)  n > #A >= 1
		B=(b_1,b_2,...)  m > #B > 1

		Todas las tripletas son las tripletas son: (a_i|b_j,b_k)  para j diferente de k
		"""
		self.triplet={}
		# Inicializar recorrido
		self.setWP([])
		rec=Tour(self)
		rec.actions=[lambda N0:N0.addNodeTriplets()]
		rec.condition=condition
		rec.go()

	def drawTree(self,lx=1,ly=1,name="flujo.png",spices="all"):
		"Crea representación grafica de árbol de especies con pérdidas y ganancias de genes"
		# Cargar singletones
		singles=self.singletones	
		for spiS in singles:		
			singles[spiS]=len(singles[spiS])  
		# Inicializar características del dibujo
		dx=lx/(self.diameter+1)
		dy=ly/(len(self.leavs)+1)
		self.xNodes={}
		self.yNodes={}
		y=dy
		# Inicializar bottomUp
		self.initC()
		# Iniciar el recorrido
		while(len(self.nDist)>0):
			# Entrar a hoja profunda
			m=max(self.nDist)	# Debe ser atributo global
			path=self.nLeavs[self.nDist[m][0]][0][:-1]	# Me gustaría que este path sea "atributo" del recorrido
			self.setWP(path)
			# Meter nuevo nodo a la lista de espera!
			lp=len(self.wp)
			uni=[]
			for i in range(1,len(self.wn)): uni+=self.nLeavs[self.wn[i][0]][1]	# Hace lista de genes induced
			self.nLeavs[self.wn[0]]=[self.wp,uni] # Crea nodo compalpsado
			if(lp>0):
				if(lp in list(self.nDist)): self.nDist[lp]+=[self.wn[0]] # Pone a wp en cola
				else: self.nDist[lp]=[self.wn[0]]
			# Sacar a los reconciliados de la cola
			for i in range(1,len(self.wn)): self.nDist[m].remove(self.wn[i][0])
			if(len(self.nDist[m])==0): del self.nDist[m]	# Quita distancia si ya ni tiene elementos
			# Hacer tarea
			#	Calcular pos de hojas
			for i in range(1,len(self.wn)):
				self.setWP(path+[i])
				if(self.nodeK()=='G'):
					self.xNodes[self.wn[0]]=lx
					self.yNodes[self.wn[0]]=y
					plt.text(lx,y+0.2*dy,self.wn[0])
					y+=dy
			self.setWP(path)
			#	Calcular pos de nodos
			indL=self.nLeavs[self.wn[0]][1]
			indY=[self.yNodes[indu] for indu in indL]
			self.xNodes[self.wn[0]]=m*dx
			self.yNodes[self.wn[0]]=(min(indY)+max(indY))/2
			for i in range(1,len(self.wn)):
				X=[self.xNodes[self.wn[0]],self.xNodes[self.wn[0]],self.xNodes[self.wn[i][0]]]
				Y=[self.yNodes[self.wn[0]],self.yNodes[self.wn[i][0]],self.yNodes[self.wn[i][0]]]
				plt.plot(X,Y,color="black")
				plt.text(self.xNodes[self.wn[0]]+0.1*dx,self.yNodes[self.wn[i][0]]+0.1*dy,"+"+str(self.edgesG[(self.wn[0],self.wn[i][0])]),color="green")
				plt.text(self.xNodes[self.wn[0]]+0.1*dx,self.yNodes[self.wn[i][0]]-0.3*dy,"-"+str(self.edgesL[(self.wn[0],self.wn[i][0])]),color="red")
				plt.text(self.xNodes[self.wn[i][0]]+0.1*dx,self.yNodes[self.wn[i][0]],self.nodesW[self.wn[i][0]],color="blue")
				if(len(self.wn[i])==1): plt.text(self.xNodes[self.wn[i][0]]+0.1*dx,self.yNodes[self.wn[i][0]]-0.2*dy,\
					"("+str(singles[self.wn[i][0]])+")",color="blue" )
		plt.plot([dx,0],[ly/2,ly/2],color="black")
		plt.text(dx*(1.1),ly/2,self.nodesW[self.wn[0]],color="b")
		plt.text(dx*0.1,ly/2+dy*0.1,"+"+str(self.edgesG[("-",self.wn[0])]),color="g")
		plt.text(dx*0.1,ly/2-dy*0.3,"-"+str(self.edgesL[("-",self.wn[0])]),color="r")
#		plt.plot([0,1,1,0,0],[0,0,1,1,0],color="gray")
		plt.savefig(name)

	def __init__(self,s,weigth=False,kind=None):	# Ingresa sitring en nwk sin ";"
		"Crea árbol a partir de string con formato awk"
		# Inicializar nodo raíz
		n=0 # Numeración de eventos
		[i,R]=readS(s,0)	# Lee al primer objeto en la string "s" que tiene a un awk
		if(R==''): self.root=[str("/")]	# Crea nodo raíz
		else:		# Estams dando por hecho que lo primero que encuentra es ')'
			self.root=[R]
		self.diameter=0	# Distancia del nodo raíz a la hoja mas profunda
		Tree.setWP(self,[])
		self.leavs={}
		self.weigth=weigth
		if(self.weigth):
			self.edgesG={}	# edges gain
			self.edgesL={}	# edges lose
			self.edgesG[('-/','/')]=0
			self.edgesL[('-/','/')]=0
			self.nodesW={}
			self.nodesW['/']=0
		i+=1
		# Leer el resto del árbol
		l=len(s)
		while(i<l):
			[i,R]=readS(s,i)
			if(s[l-i-1]=='('):	# R puede ser nodo o nada, El contenido del pat actual pueden ser hojas, se regresa un directorio
				if(R!=''): Tree.putN(self,R)
				Tree.addLeaves(self)
				Tree.setWP(self,self.wp[:-1])
			elif(s[l-i-1]==')'):	# Se crea nodo y actualiza wp, wn
				Tree.putN(self,R)
				Tree.setWP(self,self.wp+[len(self.wn)-1])
				if(len(self.wp)>self.diameter):
					self.diameter=len(self.wp)
			elif(s[l-i-1]==','):	# Se crea nodo
				if(R!=''):
					if((R=='P')|(R=='S')):
						R=R+'.'+str(n)
						n+=1
					Tree.putN(self,R)
					if(len(self.wp)+1>self.diameter): self.diameter=len(self.wp)+1
			i+=1
		# Cambiar nombres de nodos y aristas del árbol
		oldName={}
		self.initC()	# Inicializa diccionario distancia-nodos y diccionario nodo-hojas_inducidas
		l=list(self.nDist)	# Lista de distancias disponibles
		# self.nDist es la cola de nodos a colapsar
		while(len(l)>0):	# Mientras existan nodos en nDist
			# Escogemos nodo
			A=self.nDist[max(l)][0]	# Toma la primera hoja mas profunda
			pp=self.nLeavs[A][0][:]	# Ruta de A
			# Vamos al LCA de A
			P=pp[:-1]
			self.setWP(P)	# Estamos en padre de A
			B=[self.wn[i][0] for i in range(1,len(self.wn))]	# Hermanos de A y A
			# Escribimos el nombre del nodo en nwk
			newName='('
			for i in range(len(B)-1): newName+=B[i]+','
			newName+=B[len(B)-1]+')'
			nk=self.nodeK()
			if((nk=='S')|(nk=='P')): newName+=nk
			elif(nk=="*"):
				newName+=nk
				self.prime=self.wp[:]
			oldName[self.wn[0]]=newName
			self.wn[0]=newName
			# Meter nuevo nodo a nDist
			LP=len(self.wp)
			uni=[]
			for i in range(1,len(self.wn)): uni+=self.nLeavs[self.wn[i][0]][1]	# Hace lista de genes involved
			self.nLeavs[self.wn[0]]=[self.wp,uni] # Crea nodo compalpsado
			if(LP>0):
				if(LP in list(self.nDist)): self.nDist[LP]+=[self.wn[0]] # Pone a wp en cola
				else: self.nDist[LP]=[self.wn[0]]
			# Sacar a las hojas colapsadas de la lista de espera
			MAX=self.nDist[max(l)]
			for b in B:
				if(b in MAX): MAX.remove(b)
			if(len(self.nDist[max(l)])==0): del self.nDist[max(l)]	# Quita distancia si ya ni tiene elementos
			# Actualizar lista de distancias
			l=list(self.nDist)
		# Actualizar nombres de listas de pesos t crear lista de co-orthologs y co-paralogs
		if(self.weigth):
			if(kind=='events_tree'): self.tripletSets(condition=lambda T:T.nodeK()=="S")
			else: self.tripletSets()
			oldName[""]=self.root[0]
			oldName['-']='-'
			for leav in list(self.leavs):	# Pone peso en las hojas
				self.nodesW[leav]=0
				oldName[Name(self.leavs[leav])]=leav
				oldName[leav]=leav
			for ew in list(self.edgesG):	# Pesos en las aristas
				self.edgesG[(oldName[ew[0][:-1]],oldName[ew[1][:-1]])]=self.edgesG[ew]
				self.edgesL[(oldName[ew[0][:-1]],oldName[ew[1][:-1]])]=self.edgesG[ew]
				del self.edgesG[ew]
			for nw in list(self.nodesW):	# Pesos en los nodos internos
				self.nodesW[oldName[nw]]=self.nodesW[nw]
				if(nw[0]=='/'): del self.nodesW[nw]
			self.coorthologs={}
			self.coparalogs={}
			self.nodeSW={}
			self.edgeSW={}
			for node in list(self.nodesW):
				self.coorthologs[node]=[]
				self.nodeSW[node]={}
			for edge in list(self.edgesG):
				self.coparalogs[edge]=[]
				self.edgeSW[edge]={}
		else:
			self.tripletSets()
