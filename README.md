# EvoReco

**Ramírez Rafael J.A.**    | jarr.tecn@gmail.com

**Dra. Hernades Rosales M.** | maribel@im.unam.mx

This tool allows the **inference and study of the evolutionary history of large gene families** by approximating an **orthology graph** (where nodes represent genes and there is an edge between two nodes if they are predicted to be orthologous genes).

From this graph we obtain a **gene-tree**, where leaves represent sequences (genes, proteins, transcripts, ... ) and the inner nodes represent evolutionary events (at the moment, we only have speciation and duplication events). This tree is obtained by applying an algorithm for _modular decomposition_ proposed by Christian Capelle et. al. (1994) to the _Proteinortho_ graph, and a _min-cut_ algorithm proposed by Mechthild Stoer and Frank Wagner (1997) that ensures the graph is a cograph by removing or adding orthology relationships. This last condition relies on theory of symbolic ultrametrics aborded by Hellmuth M, Hernandez-Rosales et. al. (2012): *a relation on a set of genes is an orthology relation if and only if it is a cograph*.

Finally the tool will reconcile the gene trees with a specie tree through a mapping of each genes-tree to a species tree (where the leaves are exant species and inner nodes *ancestral species* i.e. species which existed and from which the current species descend). This process is based on the results of Hernandez-Rosales and Hellmuth M. et. al. (2012) when analyzing the process of mapping a gene to species trees: *S is a species tree for the genes tree T if and only if S displays all rooted triples of T that have three distinct species as their leaves and are rooted in a speciation vertex*, and also denotes that the *gene trees convey a large amount of information on underlying species trees, even for a large percentage of gene losses*.

It is possible to infer potential genes families with the tool [Proteinortho](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-12-124#citeas) (developed by Lechner M and Findeiß S.  *et al.* in (2001))  so **you have no need to know a priori the genes families**. Also if you don't know the species tree this tool may **infer the species tree form the genes trees** by using the algorithm *MinCut SuperTree* (Semple and Steel (2000)) that uses an heuristic to determine a species tree that displays the subset of consistent triplets, whose node represent a speciation event, of the event-labeled gene trees,.

# Table of contents

1. [Quick description](#Quick description)
2. [Installation](#Installation)
   1. [Prerequisites](#Prerequisites)
3. [Linux/Unix COMMAND LINE USAGE](#Linux/Unix COMMAND LINE USAGE)
   1. [Synopsis](#Synopsis)
   2. [Description](#Description)
   3. [Options](#options)
4. [Output data](#Output data)
5. [Project function](#Project function)
6. [Hints](#Hints)
7. [Pipe line description](#Pipe line description)
8. [Example](#Example)
9. [References](#References)

# Quick description

**Input**: 

- A set of FASTA files containing DNA or aminoacid sequences (if you want to make the complete analysis), and a species tree with Newick format. There must be one FASTA file for each species in the species tree.

**Output**: 

- Orthology graphs (from _Proteinortho_)
- Event-labeled Gene trees (inner nodes represent duplication or speciation events)
- Reconciliation Tree (where gene losses are inferred and gene duplications are placed along the branches of the species tree)

# Installation

Clone or download [this](https://gitlab.com/jarr.tecn/d2r2) repository.

## Prerequisites

- [proteinortho](https://gitlab.com/paulklemm_PHD/proteinortho) (V >= 5.0)
- [networkx](https://pypi.org/project/networkx/) (V < 2)

# Linux/Unix COMMAND LINE USAGE

**genTimeFlow**: Python script for bash command line implementation of the pipeline.

## Synopsis

In installation you downloaded the **d2r2** directory. Inside this directory, there is a python script name genTimeFlow, you just have to execute it as follows:

```bash
$ cd d2r2
$ ./genTimeFlow -[option 1] [parameter_for_option_1] -[option_2] [parameter_for_option_2] ...
```

## Description

To run an analysis you must use **genTimeFlow** with specific options to tell the tool the task you want to perform, first denoting the option with a dash ("-") and a white space (" ") followed by the parameter of the option. You can set as many options as you want.

**All the options are optional, but you need to set them accurately depending on the steps you want to perform**

## Options

  ```bash
./geneTimeFlow.py -project_name project_name -project_path /path/to/save/the/project/ -secuences_path /path/with/fasta/files/
-fasta_labels /path/to/fasta2label/file -fastas_format ensemble -secuences_kind proteins_or_genes -identifier_kind proteins_or_genes -species_tree /path/to/newick/file -overwrite True_or_False.
  ```

<details>
<summary> Project creation (Click to expand) </summary>
- project_name Name (default: myproject)

  Needed for any step of th analysis, if not provided default will be assigned .

  ```bash
  -project_name [name]
  ```

  - parameter: name
  - Default: "project"

- **project_path**: Path of the project's directory, or where it will be created.

  Needed for any step of th analysis, if don't provided default will be assigned.

  ```bash
  -project_path [path]
  ```

  - Parameter: path
  - Default: "./"

- **overwrite**: Determines what to do when trying to create an allredy existing project.

  Use in case you want to overwrite a project


  - Parameter: "True" or "False"
  - Default: This parameter allways will be "False" unless you specify "True" when executing genTimeFlow (not like others flags that it's value are saved when are specified or the project is created)

</details>

<details>
<summary> Input data (Click to expand) </summary>



  <details>
  <summary style="margin-left: 40px"> Sequences (Click to expand) </summary>
  <p style="margin-left: 60px">

- secuences_path**: Path where the FASTA files (.fa or .fasta) are allocated

  Needed for steep 1.

  We pass these files to proteinortho to approximate the orthology graph of the spices, then gene families are identified and for each family an evolutionary history is obtained (in form of genes tree) by modular decomposition.

  ```bash
  -secuences_path [path]
  ```

  - Parameter: path
  - Default: "./"
  - <u>Attention:</u> See fasta_labels, secuences_kind and identifier_kind

- **fasta_labels**: Label of the corresponding spice for each fasta file

  Complement data for steep 1

  ```bash
  -fasta_labels [path]
  ```

  - Parameter: path to a .tsv file with two columns:
    1. FASTA file name
    2. label (spice) of the FASTA file
  - If not provided the labels will be asigned atumatically.

- **secuences_kind**: Kind of the secunces in the FASTa files

  Needed for steep 1, if don't provided default will be assigned.

  ```bash
  -secuences_kind [kind]
  ```

  - Parameter: "genes" or "proteins"
  - Default: "genes"

  Note: "genes" may be set for analicing nucleotide secuences, and "proteins" for peptide secuences.

- **identifier_kind**: Kind of identifier extracted from FASTA to make the analisys

  Needed for steep 1, if don't provided default will be assigned.

  ```bash
  -identifier_kind [kind]
  ```

  - Parameter:
    - if secuences_kind = "genes" then the only posible parameter is "genes"
    - if secuences_kind = "proteins" the the parameter may be "proteins" or genes
  - Default: the same as secuences_kind

  Note: "genes" may be set for analicing nucleotide secuences, and "proteins" for peptide secuences.

  </p>

</details>

<details>
<summary> Species tree (Click to expand) </summary>


- **species_tree**: Species tree in [newick format](https://en.wikipedia.org/wiki/Newick_format).

  Needed for steep 3, if don't provided one will be inferred with MinCutSuperTree.

  It is  used for the analysis of gene histories through the reconciliation of the gene trees with the species tree, resulting in a estimation of **how much ancient is a gen**, also is used to determine gene lose and duplication.

  ```bash
  -species_tree [path]
  ```

  - Parameter: Path to a file with one line containg the species tree in newick format.
    - <u>Atention</u>: The leaves label of the species tree must mach with the labels of the species of the fasta files.
  - If don't provided the species tree will be infered from the genes tree generated during the pipe line.

</details>

<details>
<summary> GFF files (Click to expand) </summary>


- **gff_path**: Path where gff files of the corresponding spices are saved

  Needed for proteinortho sinteny analysis.

  ```bash
  -gff_path [path]
  ```

  - Parameter: path
  - Default: None
  - <u>Attention</u>: See gff_labels labels

- **gff_labels**: Label of the corresponding spice for each gff files.

  Complement data for steep gff_path. Must be congreunt with fasta_labels.

  These files contain information about the genes in the FASTAs, part of the information contained is about **gene locus** so we can pass these files to proteinortho for the implementation of a **sinteny analysis**, that will **improve the gene families determination** by seeing how much a set of genes are clustered in some loci, increasing probability that these genes are paralogs to each other.

  ```bash
  -gff_labels [path]
  ```

  - Parameter: Path to a .tsv file with two columns:
    1. GFF file
    2. namelabel (spice) of the GFF fil

</details>

</details>

  <details>
  <summary> Analisys (Click to expand) </summary>


- **steeps**: steeps of the process desired to perform:

  Needed for any step of th analysis, if don't provided default will be assigned.

  ```bash
  -steeps [list of integers separated by a comma(",")]
  ```

  - Parameter: list of integers separated by a comma (","). These integers are described in the [pipeline](Pipeline)
  - Default: "0"

  <u>Note</u>: The number zero means "all steeps", and is equivalent to write "1,2,3,4,5,6,7"

</details>

<details>
<summary> Filter gene families (Click to expand) </summary>


- **reconciliation_filter**: Criteria to filter genes families for reconciliation with the recon_filter_object.

  Extra configuration for step 3.

  ```bash
  -reconciliation_filter [criteria]
  ```

  You can determine which gene families are reconcilied throught this flag

  - Parameter:
    - "secences_list": A family is accepted if at least one of it's genes is in recon_filter_object
    - or  "species": A family is accepted if at least one of it's genes is of spice "spice"
    - or "familys_list": A family is accepted if it's label is in recon_filter_object
  - Default: None: all the gene families will be reconcilied.
  - <u>Atention</u>: see recon_filter_object

- **recon_filter_object**: Object for check the criteria determined in reconciliation_filter.

  Needed when reconciliation_filter seted

  ```bash
  -recon_filter_object [path or specie depending of recon_filter_object]
  ```

  - Parameter depends on recon_filter_object:
    - if recon_filter_object = "secuences_list" then recon_filter_object is the path to  file thar have the label of a secuence for each line.
    - if recon_filter_object = "species" then recon_filter_object is a list of species separated by a comma (",")
      - <u>Attention</u>: the spices must mach with the spices labels of the FASTA files
    - if recon_filter_object = "famiys_list" then recon_filter_object is the path to  file thar have the label of a genes family for each line.

</details>

<details>
<summary> Fix proteinortho (Click to expand) </summary>


- **proteinortho**: command for calling proteinortho from bash.

  This is the out-program runned for step 1.

  If you downloaded or clonated the [proteinortho repository](https://gitlab.com/paulklemm_PHD/proteinortho), the parameter you must put is how is runned the executable, as example "bash /path/to/proteinortho5.pl".

  If you installed proteinrtho you can just set the parameter as "proteinortho5.pl".

  ```bash
  -proteinortho [command]
  ```

  - Parameter: command
  - Default: "proteinortho5.pl"

- **prt_steeps**: steeps to run with proteinortho.

  Extra configuration for proteiorhto

  ```bash
  -prt_steeps [list of integers separated by a comma(",")]
  ```

  - Parameter: list of integers separated by a comma (","). The steeps can be seen in the [proteinortho project](https://gitlab.com/paulklemm_PHD/proteinortho).
  - Default: "0"
    - <u>Note</u>: The number zero means "all steeps", and is equivalent to write "1,2,3"

</details>

# Output data

In the project directory you will find sub-directories that contain the information recovered during the analisys, below are listed that directories and inside each direcory are listed with an indentation the files that contains, with name and content format:

- **Summary**: Here are sumarised de results of alll the analysis, the kind of files that contains are:

  - \<project name>.\<specie 1>_\<specie 2>.\<secuences kind>**.edgesAge**

    content format: tsv

    This file contains all the orthology relationships between the \<specie 1> and the \<specie 2>. The colums conten is:

    1. gene family

    2. age

    3. secuences identifiers

    4. This column contains the orthologs of  \<specie 1> with other spice(s) depending on  \<specie 2> value:

       - \<specie 2> is a label of an specie.

         The column contains gene identifiers of  \<specie 2>

       - \<specie 2> is "orthologous"

         In this case there will be a column for each specie of the FASTAs different from  \<specie 1>, each column contains the orthologs to the gen of  \<specie 1> with the respective specie.

  - \<project name>**.editions**

    This file contains the editions applyed to the graphs obtained to enshure that represents orthology relatinships.

    Content format: The file is readed by lines. If the line begins with 

    - "#" then it line indicates a genes family

- "> added" means that below are listed the orthology relatinships added

  - "> removed" means that below are listed the orthology relatinships removed

  - \<project name>.\<secuences kind>--\<distinction>**.reconciliationTree**

    Reconciliation is the process of mapping a gene tree (that corresponds to a gene family) to an species tree. When **n** genes trees are maped to the species tree, the tool makes **n+1** reconciliation trees: the first **n** corresponds to the reconciliarion of a single gene tree with an species tree, and the **+1** corresponds with the sum of the reconciliation of all gene trees in a single species tree.

    So the \<distinction> part of the file name depends on what was mapped to the species tree that the file contains:

    - if \<distinction> == "cc\<int>" then the file contains a species tree where were mapped the genes tree of the family "cc\<int>".

  - if \<distinction> == "sum" then the file contains a species tree with the sum of the reconciliation of al gene trees.

<details>
<summary> others (Click to expand) </summary>


- **Dictionaries**: Here are the relations between genes identifier and proteins identifiers if you provided emsemble FASTA format.

  \<project name>.\<specie>

  file content format: two columns:

  1. protein identifier
  2. gen identifier

- **Prt**: Here are the outpt files of proteinortho

  - \<project name>.blast-graph

    Contains the edges resulting of the blast part of the analisys

  - \<project name>.proteinortho

    Conatins information abot gene families

  - \<project name>.proteinortho-graph

    Contains the list of edges of the aproximated orthology graph.

- **Graphs**: Contains adjacency lists of the orthology graph and each of the genes families separately.

  file anme format: 

  - for the complete orthology graph: \<project name>.\<secuences kind>.adjL
  - for individual genes families: \<project name>.\<secuences kind>-cc\<int>.adjL

- **Trees**: Contains the evolutionary history infered for each gene family.

  file name format: 

  - for the complete orthology graph: \<project name>.\<secuences kind>.newick
  - for individual genes families: \<project name>.\<secuences kind>-cc\<int>.newick

  content format: newick format.

- **Blast**: Here are saved the result of the BLAST analisys runned by proteinortho.

- **Locus**: Here is saved the locus-dict python object that contains the loci information of the genes.

- **Identifiers**: Here is saved the genesDict python object that contains information of the secuences as identifiers, kind and spice.

</details>



# Hints

**Get Genomes**: You can download  genomes of different organism in the [ensmble](https://www.ensembl.org/info/data/ftp/index.html) database.

**Get species tree**: you can get it on the ncbi [taxnonomy tool](https://www.ncbi.nlm.nih.gov/taxonomy).

# "Project" function

The *project* (asociated with a project name and a project path) is meant to keep the project parameters and track the available files to determine which steps dependencies are satisfied. This avoids the user to write multiple times the same parameters if wants to re run an specific step, or if wants to review the parameters with which the data was generated.

# Pipe line description

Below are enumerated the steps and inside each one are listed it's required data and output files. 

2. **proteinortho**

   Here is approximated the orthology graph, and optionally synteny is analyzed.

   Required data:

   - Path to directory containing a FASTA file for each specie, containing the genes for the analysis.
   - (optional) Path to directory containing GFF files.

   Output files:

   - blast-graph
   - proteinortho file
   - proteinortho-graph

3. **getGeneTrees**

   Here are detected gene families and their respective orthology graph, then are obtained it's gene trees.

   Required data:correctNoCographFamilies

   - path to proteinortho files

   Output files:

   - gene trees
   - identifier dictionaries

   Here is implemented an optional step that will be run by default:

   3.1. **correct no cograph families** (optional)

   During the step 3 are identified gene families which it's orthology graph can't be converted to an gene tree (because it is not a cograph). 

   This optional step will add or delete orthology relationships between the gene of the noisy gen family through application of *min-cut algorithm* for the correction of the orthology graph.

4. **reconciliation**

   In this step the gene trees are mapped to species tree.

   Input data:

   - path to directory where the gene histories are saved
   - (optional) path to a file containing species tree.

   Output files:

   - Reconciliation trees
   - Lower bound estimation of gene antiquity
   - visualization of the sum of all reconciliation trees
   - (optional)  visualization of reconciliation tree for individual gene families.

   4.1 **species tree inference** (optional)

   If you don't provide an species tree, it will be generated through the algorithm *min-cut supertree*.

# Example

A single-line bash script is also downloaded with the repository, if you run it, a project will be done with the data in the test_files directory. To run the bash script just type:

```bash
./tutorial.sh
```

If you want to see the configuration you can see the line with:

```bash
cat tutorial.sh
```

# References

- Christian Capelle & Alain Cournier & Michel Habib (1994). Cograph Recognition Algorithm Revisited and Online Induced P 4 Search. 
- Mechthild Stoer & Frank Wagner (1997). A Simple Min-Cut Algorithm. *Lecture Notes in Computer Science*, vol. 855, 1994, pp. 141–147.
- Hellmuth M, Hernandez-Rosales M, Huber KT, Moulton V, Stadler PF & Wieseke N (2012). Orthology relations, symbolic ultrametrics, and cographs. *Journal of Mathematical Biology* **66**, 399–420. DOI 10.1007/s00285-012-0525-x.
- Maribel Hernandez-Rosales, Marc , Nicolas Wieseke, Katharina T Huber, Vincent Moulton,
  and Peter F Stadler (2012). From event-labeled gene trees to species trees. *BMC Bioinformatics*, **13**(Suppl 19):S6. http://www.biomedcentral.com/1471-2105/13/S19/S6
- Lechner, M & Findeiß, S. & Steiner, L. *et al.* Proteinortho: Detection of (Co-)orthologs in large-scale analysis. *BMC Bioinformatics* **12,** 124 (2011). https://doi.org/10.1186/1471-2105-12-124
- Charles Semple & Mike Steel (2000). A supertree method for rooted trees. *Elsevier. Discrete Applied Mathematics* **105**,147–158.
