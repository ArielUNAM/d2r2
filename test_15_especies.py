# coding=utf-8
import bmgmd as pr
import datetime
import loadGenesListFilter as lg

project="SEGs15"
path="/u/scratch/antonio/Ensemble_proteinas/"
#path="../testFiles/"
speciesTree="/u/scratch/antonio/Species_trees/15s.speciesTree.awk"
secuencesPath="/u/scratch/antonio/FASTA/"
proteinorthoPath="proteinortho5.pl"
GTFsPath="/u/scratch/antonio/GTF/"
#GTFsLabels=pr.getGTFsLabels("/u/scratch/antonio/gtf2label")
overwrite=True
ignoreDotFiles=True
identifierKind="proteins"
""" 
# Create a new project
p=pr.project(project,\
	path,\
	sequencesPath=secuencesPath,\
	speciesTree=speciesTree,\
	proteinorthoPath=proteinorthoPath,\
	overWrite=overwrite,\
	ignoreDotFiles=ignoreDotFiles,\
	GTFsPath=GTFsPath,\
	GTFsLabels=GTFsLabels)
print("project created!")
p.save()
"""

# Load a project
p=pr.load(project,path)
print("project loaded")

p.identifierKind="genes"	
p.secuencesKind="proteins"	


""" 
# run proteinortho
p.proteinortho(kind="p")
print("prt done!")
p.save()
"""

# create dictionarys
""" 
print(datetime.datetime.now())	
p.setDictionarys()
print(datetime.datetime.now())	
p.save()
print("Diccionarios creadoes")
"""



""" 
# create adjacency list
graph=p.proteinortho2adjL()
print("adjacenci list created")
#p.save()

# Obtain conex components
p.getCC()
p.save()
print("CC files created")
"""

# Obtiene las proteínas correspondientes a las protocaderinas por grupos alfa, beta, gama
#	y analiza locus
""" 
genesDict=p.loadDict("GenesDicts")
distinctions=["alfa","beta","gamma"]
for distinction in distinctions:
	genesFilter=[]
	protocaderinas=lg.protocaderinas("/u/scratch/katia/protocadherins_cluster_mmus.txt",group=distinction)
	if(p.identifierKind=="proteins"):
		for gen in protocaderinas:
			if(gen in genesDict.gen2label): genesFilter+=[genesDict.label2protein[genesDict.gen2label[gen]]]
	else: genesFilter=protocaderinas
	# Imprime comparación de locus de familias de genes
	lc=p.locusComparation("MOUSE","HUMAN",genesFilter=genesFilter,distinction=distinction)
	lc.write()
	print("p.locusComparation "+distinction+" done")
	print("not in locus dicts "+str(len(lc.notInLocusDicts)))	
	print("filters used "+str(len(lc.filterUsed)))			
	print("filter len "+str(len(genesFilter)))			
	print("faltantes: ")						
	for gen in genesFilter:
		if(gen not in lc.filterUsed): print(gen)		
	print("")
"""

""" 
distinctions=["TF","microproteins"]
filters={"TF":["ENSMUSG00000045515","ENSMUSG00000095139","ENSMUSG00000090125","ENSMUSG00000056854"],\
	"microproteins":["ENSMUSG00000025128","ENSMUSG00000045493","ENSMUSG00000048904"]}
for distinction in distinctions:
	genesFilter=filters[distinction]
	lc=p.locusComparation("MOUSE","GALUS",genesFilter=genesFilter,distinction=distinction)
	print("p.locusComparation "+distinction+" done")
	print("not in locus dicts "+str(len(lc.notInLocusDicts)))	
	print("filters used "+str(len(lc.filterUsed)))			
	print("filter len "+str(len(genesFilter)))			
	print("faltantes: ")						
	for gen in genesFilter:						
		if(gen not in lc.filterUsed): print(gen)		
	print("\nortologos\n")						
	for gen in lc.genesSpice1: print(gen+" "+str(lc.genesOth[gen]))	
	print("")							
"""

""" 
# Obtiene lista de SEGs y analiza locus
genesFilter=lg.loadSegs("../Archivos_katia/Localizacion_SEGs.txt")
lc=p.locusComparation("MOUSE","GALUS",genesFilter=genesFilter,distinction="SEGs")
lc.write()
print("p.locusComparation "+distinction+" done")
print("not in locus dicts "+str(len(lc.notInLocusDicts)))	
print("filters used "+str(len(lc.filterUsed)))			
print("filter len "+str(len(genesFilter)))			
print("faltantes: ")						
for gen in genesFilter:						
	if(gen not in lc.filterUsed): print(gen)		
print("\nortologos\n")						
for gen in lc.genesSpice1: print(gen+" "+str(lc.genesOth[gen]))	
print("")							

# Obtein singletones
p.getSingletones()
print("Se obtubieron los singletones\n")
p.save()
"""

# Obtener árboles de eventos
p.getEventTrees()
p.save()
print("Se descompusieron todos los árboles")

# singletones de katia
#genesFilter=lg.loadSegs("../Archivos_katia/Localizacion_SEGs_uniq.txt")
genesFilter=lg.loadSegs("/home/katia/IGs_Mouse.txt")

# Reconciliar árboles de eventos
rmt=p.reconciliate()

#for spice in rmt.spices:
#	rmt.printAges(spice)
#rmt.printNoOrthologous("MOUSE")

#print("Se reconciliaron árboles")
#p.save()

""" 
# imprime ortólogos y edad
filteredGenes=p.printSpiceOrthologos("MOUSE",genesFilter=genesFilter,printNoOrthologous=True)
print("se imprimieron los ortologos")
"""
