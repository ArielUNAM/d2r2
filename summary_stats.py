import matplotlib.pyplot as plt

def ageDistribution(file,GenesList=None,singletonesPath=None):
	# Definir función de filtro
	if(GenesList==None): filter=lambda gen:True
	else: filter=lambda gen: gen in GenesList
	# Crear lista de edades
	agesL=[]
	with open(file) as F:
		for line in F:
			gen,age,file=line.split()
			gen=gen.split(".")[0]
			if(filter(gen)):
				if(age!="-"): agesL+=[int(age)]
	"""
	# Agrgar singletones
	if(singletonesPath!=None):
		sings=loadSingles(singletonesPath)
		for spi in sings:
			agesL+=[]
	"""
	# Crear histograma
	bins=sorted(set(agesL))
	n,bins,patches=plt.hist(agesL,bins=bins,density=False,rwidth=0.9,log=True)
	dx=bins[1]-bins[0]
	for x,y in zip(bins,n):
		plt.text(x+0.25*dx,y,"{:.0f}".format(y))
	plt.savefig("ages_distribution")
