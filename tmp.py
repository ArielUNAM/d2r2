# coding=utf-8
import dicts as dct
import evolutionaryTree as evo
import nets as nt
import bmgmd as pr
import os
import order

#project="SEGs15"
project="cl_example"
projectPath="/u/scratch/antonio/Ensemble_proteinas/"
path="{}{}/".format(projectPath,project)
#path="../testFiles/"

genesdicts=dct.GeneDicts.load(project,path)
identifierType="proteins"
secuencesType="proteins"


# load project
p=pr.load(project,projectPath)


""" 

speciesTree="/u/scratch/antonio/Species_trees/15s.speciesTree.awk"
T=evo.Tree(newick=open(speciesTree).readline().split(';')[0],type="spices")

genesTree="/u/scratch/antonio/Ensemble_proteinas/SEGs15/Trees/SEGs15.proteins-cc24187.adjL"
with open(genesTree) as F: newick=F.readline().split(";")[0]
t=evo.Tree(newick=newick,type="proteins",label2spice=genesdicts.protein2spice)
evo.Recon(t,T)

print("~~~")
print([leav.type for leav in t.leavs])
print( [leav.weight for leav in T.leavs])
"""

""" 
#graph="24187"
#adjacencyList="{}Graphs/{}.{}-{}.adjL".format(path,project,identifierType,graph)
adjacencyList="/u/scratch/antonio/Ensemble_proteinas/SEGs15/Graphs/SEGs15.proteins-cc24187.adjL"
G=nt.Graph(adjacencyList=adjacencyList,secuencesType=secuencesType,identifierType=identifierType,\
				genesdicts=genesdicts).G
md=nt.modularDecomposition(G,editPrimesModules=True)
print(md.newik)	

# run posorder
T=evo.Tree(open(speciesTree).readline().split(';')[0],type="spices")
print(T.root.labelAsSuperNode())
posOrder=order.Tour(T.root,kind="DFS",DFSOrder="pos",actions=[lambda node:print("{}".format(node.label))])
posOrder.go()

# generar archivos de tutorial
fastaPath="/u/scratch/antonio/FASTA/"
testfastaP="/u/scratch/antonio/FASTA_test/"

with open("/u/scratch/antonio/testGenes") as F: genes=[line[:-1] for line in F]
proteins=[]
for gen in genes: proteins+=genesdicts.gen2proteins[gen]

identifierSpecifications={}
identifierSecuences={}

for fasta in os.listdir(fastaPath):
	if(".fa" in fasta or ".fasta" in fasta):
		with open("{}{}".format(fastaPath,fasta)) as fastaFile: fastaLines=fastaFile.readlines()
		with open("{}{}".format(testfastaP,fasta),"w") as testFile:
			i=0
			l=len(fastaLines)
			while(i<l):
				line=fastaLines[i] #[:-1]
				if(">" in line):
					identifier=line.split()[0][1:]
					idInGenes=identifier in proteins
				if(idInGenes): testFile.write(line)
				i+=1
"""
